# frozen_string_literal: true

class Topic < ApplicationRecord
  DISPLAY_ADDITIONAL_INFO_THRESHOLD = 15
  LONG_TITLE_THRESHOLD = 70

  include PgSearch::Model
  pg_search_scope :search,
    against: {title: "A", studied_text: "B"},
    using: {tsearch: {any_word: true}}

  belongs_to :best_essay, class_name: "Essay", optional: true
  belongs_to :category
  belongs_to :sub_category, optional: true
  has_many :essays, dependent: :destroy
  has_many :quiz_questions, as: :categorisable, class_name: "Quiz::Question"
  has_one_attached :studied_text_audio

  before_save(
    :generate_title_from_details,
    if: -> { category&.group&.commentaire? }
  )

  before_save(
    :generate_title_arabic_numbers,
    if: -> { title }
  )

  after_destroy :destroy_orphan_category

  validates(
    :title,
    presence: true,
    uniqueness: true,
    if: -> { category&.group&.dissertation? }
  )

  validates(
    :studied_text,
    presence: true,
    if: -> { category&.group&.commentaire? }
  )

  validates(
    :part,
    absence: true,
    unless: -> { book.present? }
  )

  validates(
    :edition,
    absence: true,
    unless: -> { book.present? }
  )

  scope :displayable, lambda {
    joins(:essays).where(essays: {status: :published}).distinct
  }

  scope :without_attached_audio, lambda {
    left_joins(:studied_text_audio_attachment).where("active_storage_attachments.id IS NULL")
  }

  def destroy_orphan_category
    category.destroy if category.topics.empty?
  end

  def best_displayable_essay
    essays.published.order(essay_content_type: :desc, hits_nb: :desc).first
  end

  def generate_title_from_details
    self.title = category.name
    self.title += ", #{I18n.t(book, default: book)}" if book.present?
    self.title += additional_details
  end

  def additional_details(concatenated: true)
    additional_details = []
    additional_details << [" - ", part] if part.present? && (!concatenated || category.group.francais?)
    additional_details << [" : ", themes] if themes.present?
    unless concatenated || additional_details.none?
      additional_details.first.shift
    end
    additional_details.join
  end

  def generate_title_arabic_numbers
    roman_numbers_other_than_words = /[IVXLCDM]+(?![a-z']+)/
    title_arabic_numbers = title.gsub(roman_numbers_other_than_words) do |roman_number|
      RomanFiguresHelper.roman_to_arabic(roman_number)
    end
    title_arabic_numbers = title_arabic_numbers.gsub(/Préface|Prologue|Préambule/, "0")
    title_arabic_numbers = title_arabic_numbers.gsub(/\d+/) { |number| number.rjust(5, "0") }
    self.title_arabic_numbers = title_arabic_numbers
  end

  def reference_from_details
    return if category.group.dissertation?

    reference = category.name
    reference += ", #{book}" if book.present?
    reference += " - #{part}" if book.present? && part.present?
    reference += " (#{edition})" if book.present? && edition.present?
    reference
  end

  def keywords
    return unless title

    keywords = title.downcase
    # u2019, u0027, u02BC are unicodes for apostrophes and have to be replaced before I18n transliterate
    keywords = keywords.gsub(/((c|d|j|l|m|n|qu|s|t|y)(\u2019|\u0027|\u02BC))/, "")
    keywords = I18n.transliterate(keywords)
    keywords = keywords.gsub(/-\s|\s-/, " ")
    keywords = keywords.gsub(/[^0-9a-z\- ]/, "")
    keywords = keywords.split(" ")
    keywords = keywords.select { |word| word.length > 2 || word.match(/^\d+$/) || word.match(/^[ivlcm]+$/) }
    keywords.join("-")
  end

  def similar_topics
    Topic.includes(best_essay: :essay_content)
      .displayable
      .search(keywords.split("-").join(" "))
      .with_pg_search_rank
      .where.not(id: id)
      .limit(5)
  end

  def question_display_name
    display_name = ["Question"]
    display_name += ["de #{I18n.t(sub_category.group.subject)}"]
    display_name += ["sur le texte #{additional_details(concatenated: false)}"]
    display_name += ["tiré de l'oeuvre #{sub_category.name}"]
    display_name += ["de l'auteur #{sub_category.category.name}"]
    display_name.to_sentence(words_connector: " ", last_word_connector: ", ")
  end
end
