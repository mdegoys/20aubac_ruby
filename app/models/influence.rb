class Influence < ApplicationRecord
  belongs_to :influencer, class_name: "Category"
  belongs_to :influencee, class_name: "Category"

  validates(:influencer_id, uniqueness: {scope: :influencee_id})
end
