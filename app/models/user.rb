# frozen_string_literal: true

class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
    :recoverable, :rememberable, :validatable

  has_many :essay_contents_internals, class_name: "EssayContents::Internal", dependent: :destroy
  has_many :premium_accesses, dependent: :destroy
  has_many :answers, class_name: "Quiz::Answer", dependent: :destroy

  enum level: {
    eleve: 0,
    professeur: 1,
    admin: 2
  }

  def active_premium?
    premium_accesses.active.any?
  end

  def active_recurring_premium?
    premium_accesses.recurring_paid.active.any?
  end

  def premium_rights?
    professeur? || admin? || active_premium?
  end

  def active_premium_access
    premium_accesses.active.order(expire_at: :desc).first
  end

  def add_premium_access(number_of_days = PremiumAccess::DEFAULT_DURATION, provider_id = nil, provider = :no_provider)
    return if professeur? || admin?

    if active_recurring_premium?
      PremiumAccess.create(user: self, provider: :no_provider)
    else
      activate_at = Time.zone.now
      expire_at = number_of_days.days.from_now
      PremiumAccess.create(user: self, activate_at: activate_at, expire_at: expire_at,
        provider_id: provider_id, provider: provider)
    end
  end
end
