# frozen_string_literal: true

class Quiz::Answer < ApplicationRecord
  self.table_name = "quiz_answers"

  belongs_to :question, class_name: "Quiz::Question"
  belongs_to :proposition, class_name: "Quiz::Proposition"
  belongs_to :user

  scope :correct, -> { joins(:proposition).where(quiz_propositions: {correct: true}) }
end
