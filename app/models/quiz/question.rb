# frozen_string_literal: true

class Quiz::Question < ApplicationRecord
  self.table_name = "quiz_questions"

  PROPOSITIONS_COUNT = 4
  CORRECT_PROPOSITION_COUNT = 1

  has_many :answers, class_name: "Quiz::Answer", dependent: :destroy
  has_many :propositions, class_name: "Quiz::Proposition", dependent: :destroy
  has_one :correct_proposition, -> { where(correct: true) }, class_name: "Quiz::Proposition"
  belongs_to :categorisable, polymorphic: true

  validates :statement, presence: true
  validate :propositions_count
  validate :correct_proposition_count

  def propositions_count
    if propositions.map(&:statement).uniq.count != PROPOSITIONS_COUNT
      errors.add(:base, "Le nombre de propositions uniques doit être de #{PROPOSITIONS_COUNT}")
    end
  end

  def correct_proposition_count
    if propositions.count(&:correct) != CORRECT_PROPOSITION_COUNT
      errors.add(:base, "Le nombre de propositions correctes doit être de #{CORRECT_PROPOSITION_COUNT}")
    end
  end
end
