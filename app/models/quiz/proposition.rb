# frozen_string_literal: true

class Quiz::Proposition < ApplicationRecord
  self.table_name = "quiz_propositions"

  belongs_to :question

  validates :statement, presence: true
end
