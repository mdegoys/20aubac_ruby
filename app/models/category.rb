# frozen_string_literal: true

class Category < ApplicationRecord
  belongs_to :group
  has_many :topics, -> { order(:title_arabic_numbers) }, dependent: :nullify
  has_many :quiz_questions, as: :categorisable, class_name: "Quiz::Question"
  has_many :sub_categories
  has_many :influenced_authors, foreign_key: :influencer_id, class_name: "Influence"
  has_many :influencees, through: :influenced_authors
  has_many :influencing_authors, foreign_key: :influencee_id, class_name: "Influence"
  has_many :influencers, through: :influencing_authors
  has_one_attached :avatar

  validates(:name, presence: true)
  validates(:name, uniqueness: {scope: :group_id})
  validates(:slug, uniqueness: true)

  before_create(:slugify)

  scope :without_attached_avatar, lambda {
    left_joins(:avatar_attachment).where("active_storage_attachments.id IS NULL")
  }

  scope :assigned, lambda {
    joins(:group).where.not(groups: {name: Group::NON_ASSIGNED})
  }

  scope :quiz_displayable, lambda {
    where.not(id: quiz_not_displayable)
  }

  scope :quiz_not_displayable, lambda {
    joins(:group)
      .where(groups: {exercice_type: :commentaire})
      .where.missing(:sub_categories)
  }

  def self.create_author_other(author_other, subject)
    unassigned_group = Group.find_or_create_by(
      {
        name: Group::NON_ASSIGNED,
        subject: subject,
        exercice_type: :commentaire
      }
    )
    find_or_create_by(
      {
        name: author_other,
        group: unassigned_group
      }
    )
  end

  def to_param
    slug
  end

  def slugify
    return unless name && !slug

    slug = name.downcase.strip.tr(" ", "-").gsub(/[^\w-]/, "")
    slug += "-bis" while self.class.find_by(slug: slug)

    self.slug = slug
  end

  def question_display_name
    display_name = ["Question"]
    display_name += ["de #{I18n.t(group.subject)}"]
    display_name += if group.exercice_type == "dissertation"
      ["sur la notion #{name}"]
    else
      ["sur l'auteur #{name}"]
    end
    display_name.to_sentence(words_connector: " ", last_word_connector: ", ")
  end

  def influencers_or_influencees
    self.class
      .where(id: influencee_ids)
      .or(self.class.where(id: influencer_ids))
  end

  class << self
    def top_by_hits
      joins(topics: :essays)
        .select("categories.*, sum(essays.hits_nb) as hits_sum")
        .group(:id)
        .order("hits_sum DESC")
        .limit(5)
    end
  end
end
