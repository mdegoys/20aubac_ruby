# frozen_string_literal: true

class PremiumAccess < ApplicationRecord
  DEFAULT_DURATION = 30

  belongs_to :user

  enum provider: {
    no_provider: 0,
    stripe: 1,
    mobiyo: 2
  }

  scope :active, lambda {
    current_date = Time.zone.now
    where("activate_at < '#{current_date}' AND activate_at notnull AND '#{current_date}' < expire_at AND expire_at notnull")
  }

  scope :inactive, lambda {
    current_date = Time.zone.now
    where("activate_at > '#{current_date}' OR activate_at isnull OR '#{current_date}' > expire_at OR expire_at isnull")
  }

  scope :recurring_paid, lambda {
    where(provider: :stripe)
  }

  def activable?
    return false if provider_id.present?

    activate_at.blank? && expire_at.blank? && user.premium_accesses.active.empty?
  end

  def active?
    return false if expire_at.blank? || activate_at.blank?

    current_date = Time.zone.now
    activate_at < current_date && current_date < expire_at
  end

  def cancelable?
    return false if provider_id.blank? || !stripe?

    status = StripeHelper.status_for(provider_id)
    ["active", "past_due"].include?(status)
  end

  def cancel
    return unless provider_id

    StripeHelper.cancel_subscription(provider_id)
  end

  def activate
    activate_at = Time.zone.now
    expire_at = PremiumAccess::DEFAULT_DURATION.days.from_now
    update(activate_at: activate_at, expire_at: expire_at)
  end
end
