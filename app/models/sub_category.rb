# frozen_string_literal: true

class SubCategory < ApplicationRecord
  belongs_to :category
  has_one :group, through: :category
  has_many :quiz_questions, as: :categorisable, class_name: "Quiz::Question"

  validates(:name, presence: true)
  validates(:name, uniqueness: {scope: :category_id})
  validates(:slug, uniqueness: true)
  validates(:studied_texts_autonomous, inclusion: [false, true])

  before_create(:slugify)

  def to_param
    slug
  end

  def slugify
    return unless name && !slug

    slug = name.downcase.strip.tr(" ", "-").gsub(/[^\w-]/, "")
    slug += "-bis" while self.class.find_by(slug: slug)

    self.slug = slug
  end

  def question_display_name
    display_name = ["Question"]
    display_name += ["de #{I18n.t(group.subject)}"]
    display_name += ["sur l'oeuvre #{name}"]
    display_name += ["de l'auteur #{category.name}"]
    display_name.to_sentence(words_connector: " ", last_word_connector: ", ")
  end
end
