# frozen_string_literal: true

module EssayContents
  def self.table_name_prefix
    "essay_contents_"
  end
end
