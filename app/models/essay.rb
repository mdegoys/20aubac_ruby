class Essay < ApplicationRecord
  belongs_to :topic
  belongs_to :essay_content, polymorphic: true, dependent: :destroy, touch: true

  after_destroy :destroy_orphan_topic
  after_commit :update_topic_best_essay

  enum status: {
    draft: 0,
    validation_pending: 1,
    published: 2
  }

  scope :not_published, -> { where.not(status: :published) }

  class << self
    def top_by(order, limit = 5, subject = nil)
      top = preload(%i[topic essay_content])
      if subject
        top = top.joins(topic: [category: :group])
          .where(group: {subject: Group.subjects[subject]})
      end
      top.published
        .order(order => :desc)
        .limit(limit)
    end
  end

  def destroy_orphan_topic
    topic.destroy if topic.essays.empty?
  end

  def update_topic_best_essay
    topic.update(best_essay: topic.best_displayable_essay) unless topic.destroyed?
  end

  def other_essays
    Essay.includes(essay_content: :user)
      .published
      .where(topic: topic)
      .where.not(id: id)
  end

  def topic_locked?
    published? || !topic.essays.empty? && self != topic.essays.first
  end

  def author_name
    essay_content.is_a?(EssayContents::Internal) ? essay_content.user.username : essay_content.author_name
  end

  def author_level
    essay_content.is_a?(EssayContents::Internal) ? essay_content.user.level : essay_content.author_level
  end
end
