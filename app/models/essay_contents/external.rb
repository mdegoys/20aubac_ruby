# frozen_string_literal: true

module EssayContents
  class External < ApplicationRecord
    has_one :essay, as: :essay_content, dependent: :destroy, touch: true
    has_one :topic, through: :essay
    has_one_attached :pdf

    validates(:url, presence: :value)

    scope :published, -> { joins(:essay).where(essay: {status: :published}) }
    scope :not_published, -> { joins(:essay).where.not(essay: {status: :published}) }

    enum author_level: {
      eleve: 1,
      professeur: 2,
      admin: 3
    }

    def to_param
      "#{id}-#{keywords}"
    end

    def url_domain
      URI.parse(url).host
    end
  end
end
