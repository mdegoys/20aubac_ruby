# frozen_string_literal: true

module EssayContents
  class Internal < ApplicationRecord
    belongs_to :user
    has_one :essay, as: :essay_content, dependent: :destroy, touch: true
    has_one :topic, through: :essay

    accepts_nested_attributes_for :topic, :essay

    validates(:description, presence: :value)
    validates(:content, presence: :value)

    scope :published, -> { joins(:essay).where(essay: {status: :published}) }
    scope :not_published, -> { joins(:essay).where.not(essay: {status: :published}) }

    def to_param
      if keywords
        "#{id}-#{keywords}"
      else
        id.to_s
      end
    end

    def topic_attributes=(topic_attrs)
      topic = Topic.find_or_initialize_by(id: topic_attrs.delete(:id))
      self.topic = topic
      return if self&.essay&.topic_locked?

      self.topic.attributes = topic_attrs
    end

    def excerpt
      content[0..2500]
    end
  end
end
