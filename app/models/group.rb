# frozen_string_literal: true

class Group < ApplicationRecord
  has_many :categories, dependent: :nullify
  has_many :topics, through: :categories

  validates(:name, presence: true)
  validates(:name, uniqueness: {scope: :subject}, unless: -> { name == Group::NON_ASSIGNED })

  enum exercice_type: {
    dissertation: 0,
    commentaire: 1
  }

  enum subject: {
    philosophie: 0,
    francais: 1
  }

  NON_ASSIGNED = "Non assigné"
end
