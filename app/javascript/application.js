// Configure your import map in config/importmap.rb. Read more: https://github.com/rails/importmap-rails
import "@hotwired/turbo-rails"
import "controllers"

window.addEventListener('turbo:load', function(event) {
  let shareLabel = document.getElementById('share-label');
  if (shareLabel) {
    let labels = {
      default: {
        name: shareLabel.innerText
      },
      actionable: [
        {
          id: "link",
          name: null,
          color: "#FF851B",
          element: null,
          actionOnClick: function(url) { copyToClipboard(url) }
        },
        {
          id: "email",
          name: null,
          color: "#3D9970",
          element: null
        },
        {
          id: "mastodon",
          name: null,
          color: "#3088D4",
          element: null,
          actionOnClick: function(url) { openPopup(url) }
        },
        {
          id: "diaspora",
          name: null,
          color: "#222222",
          element: null,
          actionOnClick: function(url) { openPopup(url) }
        },
        {
          id: "twitter",
          name: null,
          color: "#1DA1F2",
          element: null,
          actionOnClick: function(url) { openPopup(url) }
        },
        {
          id: "facebook",
          name: null,
          color: "#4267B2",
          element: null,
          actionOnClick: function(url) { openPopup(url) }
        }
      ]
    }

    labels.actionable.forEach((label) => {
      label.element = document.getElementById(label.id);
      label.name = document.getElementById(label.id).dataset.name;
      label.element.addEventListener('mouseover', function() {
        changeLabel('actionable', label);
        changeIconColor(label, true);
      });
      label.element.addEventListener('mouseout', function() {
        changeLabel('default', labels.default);
        changeIconColor(label, false);
      });
      label.element.addEventListener('click', function(e) {
        if (label.actionOnClick) {
          e.preventDefault();
          label.actionOnClick(e.currentTarget.href)
        }
      });
    })
  }

  let audio_icon = document.getElementById('audio_icon');
  let audio_player = document.getElementById('audio_player');
  if (audio_icon && audio_player) {
    audio_icon.addEventListener('click', function(event) {
      if (audio_player.paused) {
        audio_player.play();
        audio_icon.classList.add("text-green-700");
        audio_icon.classList.remove("text-gray-700");
        audio_icon.classList.remove("text-orange-700");
      } else {
        audio_player.pause();
        audio_icon.classList.add("text-orange-700");
        audio_icon.classList.remove("text-gray-700");
        audio_icon.classList.remove("text-green-700");
      }
    })
  }

  window.addEventListener('scroll', function() {
    const progressBar = document.getElementById('progress-bar');
    const header = document.getElementById('header');
    const scrollY = window.scrollY;
    if (scrollY == 0) {
      header.classList.remove('banner-shadow')
      progressBar.classList.add('hidden');
    } else {
      makeScrolledTitleLinkBold();
      header.classList.add('banner-shadow')
      const internalContentEnd = document.getElementById('internal-content-end');
      if (internalContentEnd) {
        const windowHeight = window.innerHeight;
        const internalContentEndHeight = internalContentEnd.offsetTop;
        const scrollableContentHeight = internalContentEndHeight - windowHeight;
        const percentage = Math.max(Math.floor(scrollY / scrollableContentHeight * 100), 0)
        progressBar.classList.remove('hidden');
        progressBar.style.width = percentage + '%';
      }
    }
  })
});

function makeScrolledTitleLinkBold() {
  let paragraphTitles = getParagraphTitles();
  if (paragraphTitles.length > 0) {
    let title = paragraphTitles.find(function(x, i, a) {
      return x.scrollHeight < scrollY && (a.length == i+1 || a.length > i+1 && scrollY < a[i+1].scrollHeight)
    })
    if (title) {
      addFontBoldToTitleLink(title);
      paragraphTitles.filter(x => x.title != title.title).forEach(removeFontBoldToTitleLink);
    } else {
      paragraphTitles.forEach(removeFontBoldToTitleLink);
    }
  }
}

function getParagraphTitles() {
  const articles = document.getElementsByTagName('article');
  let paragraphTitles = articles.length > 0 ? Array.from(articles[0].getElementsByTagName('h2')) : [];
  let scrollHeightMargin = 90;
  return paragraphTitles.map(x => ({ title: x, scrollHeight: x.offsetTop - scrollHeightMargin }));
}

function getTitleLink(title) {
  return document.querySelectorAll(`a[href='#${title.title.id}']`)[0];
}

function removeFontBoldToTitleLink(title) {
  let titleLink = getTitleLink(title)
  titleLink.classList.remove('title-link-bold');
}

function addFontBoldToTitleLink(title) {
  let titleLink = getTitleLink(title)
  titleLink.classList.add('title-link-bold');
}

function changeLabel(labelType, label) {
  let labelsDiv = document.getElementById('share-label');
  if (labelType != 'default') {
    labelsDiv.innerHTML = `<span style="color:${label.color}">${label.name}</span>`
  } else {
    labelsDiv.innerHTML = label.name;
  }
}

function changeIconColor(label, isOn) {
  let divSpan = document.querySelector(`.icon-${label.id}`);
  divSpan.style.cssText = isOn ? `color:${label.color};`: '';
}

function copyToClipboard(url) {
  if (navigator.clipboard) {
    navigator.clipboard.writeText(url);
    changeLabel('actions', { name: 'Lien copié !', color: '#009400' })
  }
}

function openPopup(url) {
  let { innerWidth, innerHeight } = window
  let height = 320;
  let width = 600;
  let topValue = Math.round((innerHeight - height)/2)
  let leftValue = Math.round((innerWidth - width)/2)
  let windowFeatures = `menubar=yes, location=no, resizable=yes, scrollbars=yes, status=yes, height=${height}, width=${width}, top=${topValue}, left=${leftValue}`;
  let newWindow = window.open(url, '_blank', windowFeatures);
  if (window.focus && newWindow) { newWindow.focus() }
  return false;
}

