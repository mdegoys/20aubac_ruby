import { Controller } from "@hotwired/stimulus"

// Connects to data-controller="quiz-answer"
export default class extends Controller {
  static targets = ['type', 'category', 'subCategory', 'topicId'];

  submitType() {
    this.typeTarget.requestSubmit();
  }

  submitCategory() {
    this.categoryTarget.requestSubmit();
  }

  submitSubCategory() {
    this.subCategoryTarget.requestSubmit();
  }

  submitTopicId() {
    this.topicIdTarget.requestSubmit();
  }
}
