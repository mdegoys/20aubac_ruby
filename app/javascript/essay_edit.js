function convertToHtml(bbcodeTag, contentFieldId) {
  let contentField = document.getElementById("essay_contents_internal_content");

  let contentFieldValue = contentField.value;

  let selectedContentBefore = contentFieldValue.substring(0, contentField.selectionStart);
  let selectedContentAfter = contentFieldValue.substring(contentField.selectionEnd, contentField.textLength);
  let selectedContent = contentFieldValue.substring(contentField.selectionStart, contentField.selectionEnd);

  let selectedContentChanged = `[${bbcodeTag}]${selectedContent}[/${bbcodeTag}]`

  contentField.value = `${selectedContentBefore}${selectedContentChanged}${selectedContentAfter}`;
  contentField.focus();

  let cursorNewPosition = `${selectedContentBefore}${selectedContentChanged}`.length;
  contentField.setSelectionRange(cursorNewPosition, cursorNewPosition);
}

function toggleDisplay(id) {
  let element = document.getElementById(id);
  element.classList.toggle('hidden');
}

window.convertToHtml = convertToHtml;
window.toggleDisplay = toggleDisplay;
