const LOCAL_DOMAINS = ["localhost", "127.0.0.1", ''];
let stripe_pk;
if (LOCAL_DOMAINS.includes(window.location.hostname)) {
  stripe_pk = 'pk_test_51IvJP7AMYGWaBiwJaDWUG1g2n5SMcwDyz36BADrsPqqoEEWFKxjLiEJMuR9RNQWZFg4s5SPuGr5yOX67ojvHsIE600RtNGhVvT';
} else {
  stripe_pk = 'pk_live_51IvJP7AMYGWaBiwJPjXdqFrgAKnOnZY7iMJ8x81dohUgX270bs7eiDVDgUf4yKazh5PooNT8dx3ZxwZEupmvxZpH00WI7ZoPxF';
}

window.addEventListener('DOMContentLoaded', function(event) {
  let stripe = Stripe(stripe_pk);
  let elements = stripe.elements();
  let cardElement = elements.create('card');
  cardElement.mount('#card-element');

  cardElement.on('change', function(event) {
    if (event.error) {
      displayResult(event.error.message, 'error');
    } else {
      displayResult('');
    }
  });

  const btn = document.querySelector('#submit-payment-btn');

  btn.addEventListener('click', async (e) => {
    e.preventDefault();
    disableButton(btn);

    const customerId = document.getElementById('customer-id').value;
    const nameInput = document.getElementById('name').value || 'n/a';
    fetch('acces-premium/abonnement-stripe', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        customerId: customerId
      }),
    })
    .then(checkResponse)
    .then(data => {
      const clientSecret = data.clientSecret;
      const subscriptionId = data.subscriptionId;
      stripe.confirmCardPayment(clientSecret, {
        payment_method: {
          card: cardElement,
          billing_details: {
            name: nameInput
          },
        }
      }).then((result) => {
        if(!result.error) {
          displayResult('Le paiement a été réalisé avec succès.', 'success');
        }
        else {
          enableButton(btn);
        }
      });
    })
    .catch((error) => {
      let errorMessage = 'Erreur lors du paiement';
      if (error.message) {
        errorMessage += ` (message d'erreur: "${error.message}")`;
      }
      displayResult(errorMessage, 'error');
      enableButton(btn);
    });
  });
});

function checkResponse(response) {
  if (response.status >= 200 && response.status <= 299) {
    return response.json();
  } else {
    throw Error(response.statusText);
  }
}


function displayResult(resultMessage, resultType) {
  let messageTypes = ['error', 'success'];
  messageTypes.forEach(messageType => {
    let messageTypeDiv = document.getElementById(`flash-${messageType}`);
    messageTypeDiv.textContent = '';
    messageTypeDiv.classList.remove('py-2');
  })

  if (messageTypes.includes(resultType)) {
    let messageTypeDiv = document.getElementById(`flash-${resultType}`);
    messageTypeDiv.textContent = resultMessage;
    messageTypeDiv.classList.add('py-2');
  }
}

function disableButton(button) {
  button.classList.add("button-disabled");
  button.disabled = true;
};

function enableButton(button) {
  button.classList.remove("button-disabled");
  button.disabled = false;
};
