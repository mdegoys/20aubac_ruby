# frozen_string_literal: true

class AdminMailer < ApplicationMailer
  ADMIN_EMAIL = Rails.application.credentials.dig(:email, :admin)

  def essay_submitted(essay)
    return unless essay

    @user = essay.essay_content.user
    @topic = essay.topic
    mail(to: ADMIN_EMAIL, subject: "Un corrigé a été proposé")
  end

  def hits_reset
    mail(to: ADMIN_EMAIL, subject: "Nombre de hits réintialisé")
  end
end
