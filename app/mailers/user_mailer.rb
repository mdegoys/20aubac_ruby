# frozen_string_literal: true

class UserMailer < ApplicationMailer
  def essay_validated(essay, first_validation: false)
    return unless essay

    @user = essay.essay_content.user
    @essay = essay
    @first_validation = first_validation
    mail(to: @user.email, subject: "Votre corrigé a été accepté")
  end

  def essay_rejected(essay)
    return unless essay

    @user = essay.essay_content.user
    @essay = essay
    mail(to: @user.email, subject: "Votre corrigé a été refusé")
  end
end
