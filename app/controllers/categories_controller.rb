# frozen_string_literal: true

class CategoriesController < ApplicationController
  before_action :authenticate_user!, :require_admin, only: %i[edit update destroy]
  before_action :category, only: %i[edit update destroy]

  def index
    @matiere = if Group.subjects.key?(params[:matiere])
      params[:matiere].to_sym
    else
      :philosophie
    end
    @type = if @matiere == :francais
      :commentaire
    elsif Group.exercice_types.key?(params[:type])
      params[:type].to_sym
    else
      :dissertation
    end
    @groups = Group.where(exercice_type: @type, subject: @matiere).where.not(name: Group::NON_ASSIGNED)

    @categories = Category.joins([topics: :essays])
      .where(essays: {status: :published})
      .group("categories.id")
      .having("COUNT(essays.id) > 0")
      .order(:sorting_index)
  end

  def edit
  end

  def update
    flash[:success] = "La catégorie a bien été mise à jour." if @category.update(category_params)
    redirect_to(category_topics_path(@category))
  end

  def destroy
    if @category.topics.empty?
      @category.destroy
      flash[:success] = "La catégorie a bien été supprimé"
      redirect_to(admin_categories_orphelines_path)
    else
      flash.now[:alert] = "La catégorie ne peut être détruite car il y a des sujets associés"
    end
  end

  private

  def category_params
    params
      .require(:category)
      .permit(:name, :slug, :sorting_index, :group_id, :avatar)
  end

  def category
    @category = Category.find_by(slug: params[:id])
    redirect_to "/404" unless @category
  end
end
