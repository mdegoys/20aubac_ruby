# frozen_string_literal: true

class RegistrationsController < Devise::RegistrationsController
  before_action :store_location
  before_action :check_published_contents, only: %i[destroy]
  before_action :check_active_paid_access, only: %i[destroy]

  private

  def store_location
    case params[:suite]
    when "premium"
      store_location_for(:user, membres_premiumabonnement_path)
    when "corrige"
      store_location_for(:user, new_essay_contents_internal_path)
    end
  end

  def check_published_contents
    return unless current_user.eleve? && current_user.essay_contents_internals.published.count.positive?

    flash[:alert] = "Afin d'éviter les abus, vous ne pouvez pas effectuer cette action. Merci de nous contacter."
    redirect_to membres_accueil_path
  end

  def check_active_paid_access
    return unless current_user.active_recurring_premium?

    active_recurring_premium_access = current_user
      .premium_accesses
      .active
      .where(provider: :stripe)
      .first
    # if no active recurring premium access found, last one to expire may still be payable ('past_due' status)
    active_recurring_premium_access ||= current_user
      .premium_accesses
      .where(provider: :stripe)
      .order(expire_at: :asc)
      .last
    return unless active_recurring_premium_access.cancelable?

    flash[:alert] = "Vous avez un abonnement actif, vous devez le résilier avant de supprimer votre compte."
    redirect_to membres_accueil_path
  end
end
