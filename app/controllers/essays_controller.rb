# frozen_string_literal: true

class EssaysController < ApplicationController
  def most_popular
    @most_popular_essays_philosophie = Essay.top_by(:hits_nb_last30d, 15, :philosophie)
    @most_popular_essays_francais = Essay.top_by(:hits_nb_last30d, 15, :francais)
  end

  def recently_updated
    @recently_updated_philosophie = Essay.top_by(:updated_at, 15, :philosophie)
    @recently_updated_francais = Essay.top_by(:updated_at, 15, :francais)
  end
end
