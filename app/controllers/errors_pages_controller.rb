# frozen_string_literal: true

class ErrorsPagesController < ApplicationController
  def not_found
    render :not_found, status: :not_found, formats: [:html]
  end

  def internal_server_error
    render :internal_server_error, status: :internal_server_error, formats: [:html]
  end
end
