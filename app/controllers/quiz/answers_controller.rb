# frozen_string_literal: true

class Quiz::AnswersController < ApplicationController
  before_action :authenticate_user!
  before_action :update_selected_questions, only: %i[new]
  before_action :set_selected_questions, only: %i[show new]

  layout "quiz_answer"

  def show
    @answer = current_user.answers.last
    redirect_to(new_quiz_answers_path) unless @answer
  end

  def new
    questions = Quiz::Question
    questions = questions.where(categorisable: categorisables) if categorisables.any?
    question = questions.order("RANDOM()").first
    @answer = question.answers.build

    respond_to do |format|
      format.html { render(:new) }
      format.turbo_stream
    end
  end

  def create
    @answer = Quiz::Answer.new(answer_attributes)
    @answer.user = current_user
    @answer.save

    respond_to do |format|
      format.html { redirect_to(quiz_answers_path) }
      format.turbo_stream
    end
  end

  private

  def answer_attributes
    params.require(:quiz_answer).permit(:proposition_id, :question_id)
  end

  def selected_questions_params
    params.permit(:type, :category, :sub_category, :topic_id)
  end

  def update_selected_questions
    return if selected_questions_params.empty?

    current_user.update!(quiz_selected_questions: selected_questions_from_params)
  end

  def selected_questions_from_params
    if selected_questions_params[:type]
      if selected_questions_params[:type].present?
        "type_#{selected_questions_params[:type]}"
      end
    elsif selected_questions_params[:category]
      if selected_questions_params[:category].present?
        "category_#{selected_questions_params[:category]}"
      elsif previous_group
        "type_#{previous_group.subject}-#{previous_group.exercice_type}"
      end
    elsif selected_questions_params[:sub_category]
      if selected_questions_params[:sub_category].present?
        "sub_category_#{selected_questions_params[:sub_category]}"
      elsif previous_category
        "category_#{previous_category.slug}"
      end
    elsif selected_questions_params[:topic_id]
      if selected_questions_params[:topic_id].present?
        "topic_id_#{selected_questions_params[:topic_id]}"
      elsif previous_sub_category
        "sub_category_#{previous_sub_category.slug}"
      end
    end
  end

  def previous_group
    if previous_category_match
      Category.find_by(slug: previous_category_match)&.group
    elsif previous_sub_category_match
      SubCategory.find_by(slug: previous_sub_category_match)&.category&.group
    elsif previous_topic_id_match
      Topic.find_by(id: previous_topic_id_match)&.sub_category&.category&.group
    end
  end

  def previous_category
    if previous_sub_category_match
      SubCategory.find_by(slug: previous_sub_category_match)&.category
    elsif previous_topic_id_match
      Topic.find_by(id: previous_topic_id_match)&.sub_category&.category
    end
  end

  def previous_sub_category
    if previous_topic_id_match
      Topic.find_by(id: previous_topic_id_match)&.sub_category
    end
  end

  def previous_category_match
    current_user.quiz_selected_questions&.match(/^category_([a-z-]+)/)&.[](1)
  end

  def previous_sub_category_match
    current_user.quiz_selected_questions&.match(/^sub_category_([a-z-]+)/)&.[](1)
  end

  def previous_topic_id_match
    current_user.quiz_selected_questions&.match(/^topic_id_([0-9]+)/)&.[](1)
  end

  def set_selected_questions
    @selected_questions = {}
    return unless current_user.quiz_selected_questions

    selected_questions_text = current_user.quiz_selected_questions
    selected_type, selected_value = selected_questions_text.rpartition("_") - ["_"]
    @selected_questions.merge!({selected_type.to_sym => selected_value})
  end

  def categorisables
    if @selected_questions[:type]
      subject, exercice_type = @selected_questions[:type].split("-")
      subject_groups = Group.where(subject: subject, exercice_type: exercice_type)
      subject_categories = Category.where(group: subject_groups)
      subject_sub_categories = SubCategory.where(category: subject_categories)
      subject_sub_categories.any? ? categorisables_from(subject_sub_categories) : subject_categories
    elsif @selected_questions[:category]
      categories = Category.where(slug: @selected_questions[:category])
      sub_categories = SubCategory.where(category: categories)
      sub_categories.any? ? categorisables_from(sub_categories) : categories
    elsif @selected_questions[:sub_category]
      categorisables_from(SubCategory.where(slug: @selected_questions[:sub_category]))
    elsif @selected_questions[:topic_id]
      Topic.where(id: @selected_questions[:topic_id])
    else
      []
    end
  end

  def categorisables_from(sub_categories)
    [
      sub_categories.where(studied_texts_autonomous: false),
      Topic.displayable.where(sub_category: sub_categories.where(studied_texts_autonomous: true))
    ].select(&:any?)
  end
end
