# frozen_string_literal: true

class EssayContents::ExternalsController < ApplicationController
  before_action :authenticate_user!, except: %i[show archive_html]
  before_action :require_premium_rights, only: %i[archive_pdf]
  before_action :require_admin, except: %i[show archive_html archive_pdf]
  before_action :essay_content

  def show
    @essay = @essay_content.essay

    unless @essay.published? || current_user&.admin?
      return redirect_to category_topics_path(@essay_content.topic.category)
    end

    # rubocop:disable Rails/SkipsModelValidations
    current_user&.admin? ||
      @essay.update_columns(hits_nb: @essay.hits_nb + 1) &&
        @essay.update_columns(hits_nb_last30d: @essay.hits_nb_last30d + 1)
    # rubocop:enable Rails/SkipsModelValidations

    @other_essays = @essay.other_essays
    @similar_topics = @essay.topic.similar_topics

    @premium_display = current_user&.premium_rights?
  end

  def archive_html
    # rubocop:disable Rails/SkipsModelValidations
    current_user&.admin? || @essay_content.url_unavailable? ||
      @essay_content.update_columns(archives_access_count: @essay_content.archives_access_count + 1)
    # rubocop:enable Rails/SkipsModelValidations
    archive_link = "http://web.archive.org/web/#{@essay_content.created_at.strftime("%Y%m%d")}/#{@essay_content.url}"
    redirect_to(archive_link, allow_other_host: true)
  end

  def archive_pdf
    # rubocop:disable Rails/SkipsModelValidations
    current_user&.admin? || @essay_content.url_unavailable? ||
      @essay_content.update_columns(archives_access_count: @essay_content.archives_access_count + 1)
    # rubocop:enable Rails/SkipsModelValidations
    redirect_to(@essay_content.pdf)
  end

  def destroy
    @essay_content.essay.destroy
    flash[:success] = "Le corrigé externe a été supprimé."
    redirect_to categories_path
  end

  def archive
    @essay_content.update(url_unavailable: true, archives_access_count: 0)

    flash[:success] = "Le corrigé externe a été archivé."
    redirect_to(@essay_content)
  end

  private

  def essay_content
    @essay_content = EssayContents::External.find_by(id: params[:id])
    redirect_to "/404" unless @essay_content
  end
end
