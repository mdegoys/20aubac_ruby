# frozen_string_literal: true

class EssayContents::InternalsController < ApplicationController
  before_action :authenticate_user!, except: %i[show]
  before_action :essay_content, only: %i[edit update destroy publish validate premiumize update_topic]
  before_action :check_essay_validation_disabled_var, only: %i[publish]
  before_action :check_rights_on_essay_content, only: %i[edit update destroy publish]
  before_action :check_rights_on_shared_content, only: %i[edit update destroy publish]
  before_action :require_admin, only: %i[validate premiumize update_topic]
  before_action :topic_from_id, only: %i[new create]
  before_action :topic_from_essay_content, only: %i[edit update]
  before_action :groups, only: %i[new create edit update]

  def show
    @essay_content = EssayContents::Internal.find_by(id: params[:id])
    unless @essay_content
      # TO BE REMOVED: check if essay content exists with external id, as legacy urls mixed both externals and internals
      essay_content = EssayContents::External.find_by(id: params[:id])
      return redirect_to categories_path unless essay_content

      return redirect_to essay_content
    end

    unless @essay_content.essay.published? || current_user == @essay_content.user || current_user&.admin?
      return redirect_to category_topics_path(@essay_content.topic.category)
    end

    # rubocop:disable Rails/SkipsModelValidations
    current_user == @essay_content.user || current_user&.admin? ||
      @essay_content.essay.update_columns(hits_nb: @essay_content.essay.hits_nb + 1) &&
        @essay_content.essay.update_columns(hits_nb_last30d: @essay_content.essay.hits_nb_last30d + 1)
    # rubocop:enable Rails/SkipsModelValidations

    @other_essays = @essay_content.essay.other_essays
    @similar_topics = @essay_content.topic.similar_topics

    @premium_display = current_user&.premium_rights? || current_user == @essay_content.user

    option_url = essay_contents_internal_url(@essay_content)
    option_text = ERB::Util.u(@essay_content.topic.title)
    @options = [
      {
        id: "link",
        name: "Copier le lien",
        link: option_url
      },
      {
        id: "email",
        name: "Envoyer par email",
        link: "mailto:quelqun@quelquepart.fr&subject=#{option_text}&body=#{option_url}"
      },
      {
        id: "mastodon",
        name: "Partager sur mastodon",
        link: "https://toot.karamoff.dev/?text=#{option_text}%20#{option_url}"
      },
      {
        id: "diaspora",
        name: "Partager sur diaspora",
        link: "https://share.diasporafoundation.org/?title=#{option_text}&url=#{option_url}"
      },
      {
        id: "twitter",
        name: "Partager sur twitter",
        link: "https://twitter.com/intent/tweet/?text=#{option_text}&url=#{option_url}"
      },
      {
        id: "facebook",
        name: "Partager sur facebook",
        link: "https://www.facebook.com/share.php?u=#{option_url}"
      }
    ]
  end

  def new
    @essay_content = EssayContents::Internal.new
    @essay_content.topic = @topic
  end

  def create
    apply_author_other!
    @essay_content = EssayContents::Internal.new(essay_content_params)
    @essay_content.user = current_user
    if @essay_content.save
      flash[:success] = "Le corrigé a bien été créé."
      redirect_to @essay_content
    else
      render "new", status: 422
    end
  end

  def edit
  end

  def update
    apply_author_other!
    if @essay_content.update(essay_content_params)
      flash[:success] = "Le corrigé a bien été mis à jour."
      redirect_to @essay_content
    else
      render "edit", status: 422
    end
  end

  def destroy
    @essay_content.essay.destroy
    flash[:success] = "Le corrigé a bien été supprimé."
    redirect_to categories_path
  end

  def publish
    essay = @essay_content.essay
    if essay.draft?
      essay.update(status: :validation_pending)
      AdminMailer.essay_submitted(@essay_content.essay).deliver_later
      flash[:success] = "Votre corrigé est désormais en attente de validation."
    elsif essay.published?
      essay.update(status: :draft)
      flash[:success] = "Votre corrigé est désormais un brouillon."
    end

    redirect_to(@essay_content)
  end

  def validate
    essay = @essay_content.essay
    if essay.validation_pending? && params[:validated] == "true"
      @essay_content.update(keywords: essay.topic.keywords)
      essay.update(status: :published)
      if essay.validated_at.blank?
        first_validation = true
        essay.update(validated_at: Time.zone.now)
        @essay_content.user.add_premium_access if @essay_content.user.eleve?
      end

      UserMailer.essay_validated(essay, first_validation: first_validation).deliver_later
      flash[:success] = "Le corrigé a été accepté."
    elsif essay.validation_pending? && params[:validated] != "true"
      essay.update(status: :draft)
      UserMailer.essay_rejected(@essay_content.essay).deliver_later
      flash[:success] = "Le corrigé a été refusé."
    end

    redirect_to(@essay_content)
  end

  def premiumize
    if @essay_content.premium?
      @essay_content.update(premium: false)
      flash[:success] = "Le corrigé n'est désormais plus premium."
    else
      @essay_content.update(premium: true)
      flash[:success] = "Le corrigé est désormais premium."
    end

    redirect_to(@essay_content)
  end

  def update_topic
    new_topic_id = params[:new_topic_id]
    new_topic = Topic.find_by(id: new_topic_id)

    if new_topic
      old_topic = @essay_content.essay.topic
      @essay_content.essay.update(topic: new_topic)
      old_topic.destroy if old_topic.essays.empty?

      flash[:success] = "Le corrigé a désormais un nouveau topic"
    else
      flash[:alert] = "L'id du topic indiqué n'est pas valide"
    end

    redirect_to(@essay_content)
  end

  private

  def essay_content_params
    permitted_content_attributes = %i[content description]
    permitted_content_attributes << :keywords if current_user.admin? && @essay_content&.essay&.published?
    permitted_topic_attributes = %i[id title category_id studied_text book part themes edition]
    permitted_topic_attributes << :past_exam if current_user.admin?

    params
      .require(:essay_contents_internal)
      .permit(permitted_content_attributes, topic_attributes: permitted_topic_attributes)
  end

  def essay_content
    @essay_content = EssayContents::Internal.find_by(id: params[:id])
    redirect_to "/404" unless @essay_content
  end

  def check_essay_validation_disabled_var
    return unless !current_user.admin? && ENV["ESSAY_VALIDATION_DISABLED"] == "true"

    flash[:alert] = "Il n'est pas possible de soumettre à validation de nouveaux corrigés pour le moment. Merci de votre compréhension."
    redirect_to essay_contents_internal_path(@essay_content)
  end

  def check_rights_on_essay_content
    return if current_user.admin? || @essay_content.user == current_user

    flash[:alert] = "Vous n'avez pas les droits nécessaires pour effectuer cette action."
    redirect_to root_path
  end

  def check_rights_on_shared_content
    return unless current_user.eleve? && !@essay_content.essay.draft?

    flash[:alert] = "Afin d'éviter les abus, vous ne pouvez pas effectuer cette action. Merci de nous contacter."
    redirect_to essay_contents_internal_path(@essay_content)
  end

  def groups
    if @topic.new_record?
      @matiere = if Group.subjects.key?(params[:matiere])
        params[:matiere].to_sym
      else
        :philosophie
      end
      @type = if @matiere == :francais
        :commentaire
      elsif Group.exercice_types.key?(params[:type])
        params[:type].to_sym
      else
        :dissertation
      end
    else
      @matiere = @topic.category.group.subject.to_sym
      @type = @topic.category.group.exercice_type.to_sym
    end
    @groups = Group.where(exercice_type: @type, subject: @matiere)
  end

  def topic_from_id
    @topic_id = params[:topic_id]
    @topic = Topic.find_by(id: @topic_id) || Topic.new
  end

  def topic_from_essay_content
    @essay = @essay_content.essay
    @topic = @essay.topic
  end

  def apply_author_other!
    return if @type == :dissertation

    author_other = params.dig(:author, :other)
    return if author_other.blank?

    author_other_category = Category.create_author_other(author_other, @matiere)
    params[:essay_contents_internal][:topic_attributes][:category_id] = author_other_category.id.to_s
  end
end
