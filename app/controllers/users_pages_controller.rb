# frozen_string_literal: true

class UsersPagesController < ApplicationController
  before_action :authenticate_user!
  before_action :require_eleve, only: %i[premium_accesses premium_recurring]
  before_action :check_existing_premium_access, only: %i[premium_recurring]

  def home
    @number_of_pending_essays = Essay.includes(%i[topic essay_content])
      .validation_pending
      .count

    @number_of_essays_without_proper_plan = EssayContents::Internal.includes(:topic, :essay)
      .where(essay: {status: :published})
      .where("content NOT LIKE '%[tp]II.%'")
      .count

    @number_of_topics_with_long_titles = Topic.joins(:essays)
      .where(essays: {status: :published})
      .where("(length(topics.title) > #{Topic::LONG_TITLE_THRESHOLD}
              and length(topics.title_short) = 0)
              or length(topics.title_short) > #{Topic::LONG_TITLE_THRESHOLD}
             ")
      .distinct
      .count

    @number_of_topics_without_audios = Topic.displayable
      .joins(:essays)
      .where(essays: {essay_content_type: "EssayContents::Internal"})
      .where("studied_text IS NOT NULL and studied_text != ''")
      .without_attached_audio
      .count

    @number_of_orphan_topics = Topic.where.missing(:essays).count

    number_of_unassigned_categories = Category
      .joins([:group, {topics: :essays}])
      .where(essays: {status: :published})
      .where(group: {name: Group::NON_ASSIGNED})
      .count

    number_of_categories_without_avatars = Category
      .joins([:group])
      .where.not(group: {name: Group::NON_ASSIGNED})
      .without_attached_avatar
      .count

    number_of_categories_without_sorting_index = Category
      .joins([:group])
      .where.not(group: {name: Group::NON_ASSIGNED})
      .where(sorting_index: nil)
      .count

    @number_of_categories_missing_info = number_of_unassigned_categories +
      number_of_categories_without_avatars +
      number_of_categories_without_sorting_index

    @number_of_categories_needing_links = Category
      .select('categories.id,
         COUNT(DISTINCT influences.id),
         COUNT(DISTINCT influencing_authors_categories_join.id),
         topics.count
      ')
      .joins(topics: :essays)
      .left_joins(:influencees, :influencers)
      .where(essays: {status: Essay.statuses[:published]})
      .group("categories.id")
      .having("
          COUNT(DISTINCT influences.id) = 0 AND
          COUNT(DISTINCT influencing_authors_categories_join.id) = 0 AND
          topics.count < #{Topic::DISPLAY_ADDITIONAL_INFO_THRESHOLD}
      ")
      .to_a
      .size

    @number_of_orphan_categories = Category
      .joins(:group)
      .without_attached_avatar
      .where.not(group: {name: Group::NON_ASSIGNED})
      .where.missing(:topics)
      .count
  end

  def essays
    @not_published_essays = current_user
      .essay_contents_internals
      .includes([topic: {category: :group}])
      .not_published
    @published_essays = current_user
      .essay_contents_internals
      .includes([topic: {category: :group}])
      .published
  end

  def premium_accesses
    @active_premium_access = current_user.active_premium_access
    @other_premium_accesses = if @active_premium_access
      current_user.premium_accesses
        .where.not(id: @active_premium_access.id)
        .order(expire_at: :desc)
    else
      current_user.premium_accesses
        .order(expire_at: :desc)
    end
  end

  def premium_recurring
    if current_user.stripe_id.blank?
      begin
        stripe_customer = StripeHelper.create_customer(current_user)
        @customer_id = stripe_customer.id
        current_user.update(stripe_id: @customer_id)
      rescue Stripe::InvalidRequestError => e
        RorVsWild.record_error(e)
        alert_message = "Stripe n'a pas pu créer votre identifiant. Veuillez vérifier la \
                         validité de votre adresse email, puis réessayez."
        flash[:alert] = alert_message
        redirect_to membres_accueil_path
      end
    else
      @customer_id = current_user.stripe_id
    end
  end

  private

  def check_existing_premium_access
    return unless current_user.active_premium?

    flash[:alert] = "Vous avez déjà un accès premium actif."
    redirect_to membres_accespremium_path
  end
end
