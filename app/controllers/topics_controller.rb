# frozen_string_literal: true

class TopicsController < ApplicationController
  before_action :authenticate_user!, :require_admin, only: %i[new create edit update destroy]
  before_action :new_topic, only: %i[new create]
  before_action :topic_from_id, only: %i[edit update destroy]
  before_action :essays, only: %i[edit update]
  before_action :groups, only: %i[new create edit update]

  def index
    @category = Category.find_by(slug: params[:category_id])

    if @category
      @topics = Topic.includes(best_essay: :essay_content)
        .displayable
        .where(category: @category)
        .order(:title_arabic_numbers)
        .page(params[:page])
        .per(15)
    else
      return redirect_to(categories_path)
    end

    @display_additional_authors = @topics.count < Topic::DISPLAY_ADDITIONAL_INFO_THRESHOLD && params[:page].blank?
    if @display_additional_authors
      @influencers_or_influencees = @category.influencers_or_influencees
        .order(:sorting_index)
    end
  end

  def search
    @topics = Topic.includes(best_essay: :essay_content)
      .displayable
      .search(params[:q])
      .with_pg_search_rank
      .page(params[:page])
      .per(10)
  end

  def index_all
    @matiere = if Group.subjects.key?(params[:matiere])
      params[:matiere].to_sym
    else
      :philosophie
    end
    @type = if @matiere == :francais
      :commentaire
    elsif Group.exercice_types.key?(params[:type])
      params[:type].to_sym
    else
      :dissertation
    end
    @groups = Group.where(exercice_type: @type, subject: @matiere).where.not(name: Group::NON_ASSIGNED)

    @groups_topics_count = Topic.joins(category: :group)
      .where(group: {exercice_type: Group.exercice_types[@type],
                     subject: Group.subjects[@matiere]})
      .where.not(group: {name: Group::NON_ASSIGNED})
      .where.not(best_essay_id: nil).count

    @categories = Category.preload([topics: [best_essay: :essay_content]])
      .joins([topics: :essays])
      .where(essays: {status: :published})
      .group("categories.id")
      .having("COUNT(essays.id) > 0")
      .order(:sorting_index)
  end

  def new
  end

  def create
    # force the params with the created category to take precedence over the selected category in the form
    author_other = params.dig(:author, :other) if @type == :commentaire
    if author_other.present?
      author_other_category = Category.create_author_other(author_other, @matiere)
      params[:topic][:category_id] = author_other_category.id.to_s
    end
    @topic.assign_attributes(topic_params)
    if @topic.save
      flash[:success] = "Le sujet bien été créé."
      redirect_to(edit_topic_path(@topic))
    else
      render "new", status: 422
    end
  end

  def edit
  end

  def update
    # force the params with the created category to take precedence over the selected category in the form
    author_other = params.dig(:author, :other) if @type == :commentaire
    if author_other.present?
      author_other_category = Category.create_author_other(author_other, @matiere)
      params[:topic][:category_id] = author_other_category.id.to_s
    end
    flash.now[:success] = "Le sujet a bien été mis à jour." if @topic.update(topic_params)
    render "edit", status: 422
  end

  def destroy
    if @topic.essays.empty?
      @topic.destroy
      flash[:success] = "Le sujet a bien été supprimé"
      redirect_to(admin_sujets_path)
    else
      flash.now[:alert] = "Le sujet ne peut être détruit car il a des corrigés associés"
    end
  end

  private

  def topic_params
    params
      .require(:topic)
      .permit(:title, :title_short, :category_id, :studied_text, :book, :part, :themes, :edition, :past_exam, :studied_text_audio)
  end

  def new_topic
    @topic = Topic.new
  end

  def topic_from_id
    @topic = Topic.includes([essays: [essay_content: :user]]).find_by(id: params[:id])
    redirect_to "/404" unless @topic
  end

  def essays
    @essays = @topic.essays
  end

  def groups
    if @topic.new_record?
      @matiere = if Group.subjects.key?(params[:matiere])
        params[:matiere].to_sym
      else
        :philosophie
      end
      @type = if @matiere == :francais
        :commentaire
      elsif Group.exercice_types.key?(params[:type])
        params[:type].to_sym
      else
        :dissertation
      end
    else
      @matiere = @topic.category.group.subject.to_sym
      @type = @topic.category.group.exercice_type.to_sym
    end
    @groups = Group.where(exercice_type: @type, subject: @matiere)
  end
end
