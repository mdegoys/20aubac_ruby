# frozen_string_literal: true

class ApplicationController < ActionController::Base
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :tops_five
  after_action :record_page_view

  # For devise: user_root_path used in after_sign_up_path_for, after_sign_in_path_for, after_update_path_for
  alias_method :user_root_path, :membres_accueil_path

  def tops_five
    @top_five_essays = Essay.top_by(:hits_nb)
    @top_five_categories = Category.top_by_hits
  end

  def record_page_view
    if response.content_type&.start_with?("text/html")
      if Rails.env.production? && !request.is_crawler? && request.domain == "www.20aubac.fr"
        ActiveAnalytics.record_request(request)
      end
    end
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:username])
    devise_parameter_sanitizer.permit(:account_update, keys: [:username])
  end

  def require_admin
    return if current_user&.admin?

    flash[:alert] = "Vous devez être administrateur pour effectuer cette action."
    redirect_to membres_accueil_path
  end

  def require_eleve
    return if current_user&.eleve?

    flash[:alert] = "Vous devez être un élève pour effectuer cette action."
    redirect_to membres_accueil_path
  end

  def require_premium_rights
    return if current_user&.premium_rights?

    flash[:alert] = "Vous devez avoir un accès premium pour accéder à ce contenu."
    redirect_to membres_premiumabonnement_path
  end
end
