# frozen_string_literal: true

class AdminPagesController < ApplicationController
  before_action :authenticate_user!, :require_admin
  around_action :skip_bullet, if: -> { defined?(Bullet) }

  def skip_bullet
    previous_value = Bullet.enable?
    Bullet.enable = false
    yield
  ensure
    Bullet.enable = previous_value
  end

  def essays
    @pending_essays = Essay.includes(%i[topic essay_content])
      .order(created_at: :desc)
      .validation_pending
    @drafts_count = Essay.draft.count
  end

  def external_essays
    select = 'essay_contents_externals.*,
              essay_contents_externals.archives_access_count as archives_access_count,
              essays.hits_nb_last30d as hits_nb_last30d,
              (100.0*essay_contents_externals.archives_access_count/essays.hits_nb_last30d) as archives_access_rate
            '
    @external_essays_by_alerts = EssayContents::External.select(select)
      .includes(essay: :topic)
      .where.not(essays: {hits_nb_last30d: 0})
      .joins([:essay])
      .order("archives_access_rate DESC")
      .limit(15)
  end

  def plans
    @essays_without_proper_plan = EssayContents::Internal.includes(:topic, :essay)
      .where(essay: {status: :published})
      .where("content NOT LIKE '%[tp]II.%'")
  end

  def titles
    @topics_with_long_titles = Topic.joins(:essays)
      .select('topics.*,
               length(topics.title) as title_length,
               length(topics.title_short) as title_short_length
            ')
      .where(essays: {status: :published})
      .where("(length(topics.title) > #{Topic::LONG_TITLE_THRESHOLD}
              and length(topics.title_short) = 0)
              or length(topics.title_short) > #{Topic::LONG_TITLE_THRESHOLD}
             ")
      .distinct
      .order("length(topics.title) DESC")
  end

  def audios
    @topics_without_audios = Topic.displayable
      .joins(:essays)
      .where(essays: {essay_content_type: "EssayContents::Internal"})
      .where("studied_text IS NOT NULL and studied_text != ''")
      .without_attached_audio
  end

  def topics
    @orphan_topics = Topic.where.missing(:essays).order(created_at: :desc)
  end

  def influences
    @categories_needing_links = Category
      .select('
         categories.id,
         categories.name,
         categories.slug,
         COUNT(DISTINCT influences.id) AS influencers_count,
         COUNT(DISTINCT influencing_authors_categories_join.id) AS influencees_count,
         topics.count as topics_count
      ')
      .joins(topics: :essays)
      .left_joins(:influencees, :influencers)
      .where(essays: {status: Essay.statuses[:published]})
      .group("categories.id")
      .having("
         COUNT(DISTINCT influences.id) = 0 AND
         COUNT(DISTINCT influencing_authors_categories_join.id) = 0 AND
         topics.count < #{Topic::DISPLAY_ADDITIONAL_INFO_THRESHOLD}
      ")
  end

  def categories_missing_info
    @unassigned_categories = Category.includes(:group)
      .joins(topics: :essays)
      .where(essays: {status: :published})
      .where(group: {name: Group::NON_ASSIGNED})

    @categories_without_avatar = Category
      .includes(:group)
      .where.not(group: {name: Group::NON_ASSIGNED})
      .without_attached_avatar

    @categories_without_sorting_index = Category
      .includes(:group)
      .where.not(group: {name: Group::NON_ASSIGNED})
      .where(sorting_index: nil)
  end

  def orphan_categories
    @orphan_categories = Category
      .includes(:group)
      .without_attached_avatar
      .where.not(group: {name: Group::NON_ASSIGNED})
      .where.missing(:topics)
  end

  def professors
    @professors = User.professeur
  end

  def statistics
    @grouped_by_year_essays = Essay.published.group(:essay_content_type).group_by_year(:created_at, format: "%Y")
    @grouped_by_year_users = User.group_by_year(:created_at, format: "%Y", last: 10)
    @number_of_internal_published_essays = EssayContents::Internal.published.count
    @number_of_paid_published_essays = EssayContents::Internal.where(premium: true).published.count
    top_10_essays = EssayContents::Internal
      .joins(:essay)
      .where(essay: {status: :published})
      .order("essay.hits_nb DESC")
      .limit(10)
    @top_10_essays = top_10_essays.includes(%i[topic])
    @total_hits_top10_essays = top_10_essays.pluck(:hits_nb).inject(:+)
    top_10_paid_essays = EssayContents::Internal
      .joins(:essay)
      .where(premium: true, essay: {status: :published})
      .order("essay.hits_nb DESC")
      .limit(10)
    @top_10_paid_essays = top_10_paid_essays.includes(%i[topic])
    top_10_external_essays = EssayContents::External
      .joins(:essay)
      .where(essay: {status: :published})
      .order("essay.hits_nb DESC")
      .limit(10)
    @top_10_external_essays = top_10_external_essays.includes(%i[topic])
    @total_hits_essays = Essay.published.sum(:hits_nb)
    @percentage_top10_hits = (@total_hits_top10_essays.fdiv(@total_hits_essays) * 100).round(2)

    bottom_10_essays = Essay.published.order(hits_nb: :asc).limit(10)
    @bottom_10_essays = bottom_10_essays.includes(%i[topic essay_content])
    @total_hits_bottom10_essays = bottom_10_essays.pluck(:hits_nb).inject(:+)
    @percentage_bottom10_hits = (@total_hits_bottom10_essays.fdiv(@total_hits_essays) * 100).round(2)
  end
end
