# frozen_string_literal: true

class PremiumAccessesController < ApplicationController
  skip_before_action :verify_authenticity_token

  def create_stripe_subscription
    customer_id = params[:customerId]

    if customer_id.present?
      price_id = Rails.application.credentials.dig(:stripe, :price_id)

      subscription = StripeHelper.create_subscription(customer_id, price_id)
      render json: {
        subscriptionId: subscription.id,
        clientSecret: subscription.latest_invoice.payment_intent.client_secret
      }.to_json
    else
      render json: {
        error: {
          message: "Le paramètre customerId est manquant."
        }
      }.to_json
    end
  end

  def cancel
    premium_access = PremiumAccess.find_by(id: params[:id])

    if premium_access&.cancelable? && premium_access.user == current_user
      premium_access.cancel
      flash[:success] = "L'accès premium est désormais résilié et ne restera actif que jusqu'à la date butoir."
    else
      flash[:alert] = "Vous ne pouvez pas résilier cet accès premium."
    end

    redirect_back(fallback_location: membres_accespremium_path)
  end

  def activate
    premium_access = PremiumAccess.find_by(id: params[:id])

    if premium_access&.activable? && premium_access.user == current_user
      premium_access.activate
      flash[:success] = "L'accès premium est désormais activé."
    else
      flash[:alert] = "Vous ne pouvez pas activer cet accès premium."
    end

    redirect_back(fallback_location: membres_accespremium_path)
  end

  def webhook_stripe
    webhook_secret = Rails.application.credentials.dig(:stripe, :endpoint_secret)
    payload = request.body.read
    sig_header = request.env["HTTP_STRIPE_SIGNATURE"]
    event = nil

    begin
      event = Stripe::Webhook.construct_event(
        payload, sig_header, webhook_secret
      )
    rescue JSON::ParserError
      status 400
      return
    rescue Stripe::SignatureVerificationError
      status 400
      return
    end
    event_type = event["type"]
    data_object = event["data"]["object"]

    if event_type == "invoice.payment_succeeded"
      subscription_id = data_object["subscription"]
      customer_id = data_object["customer"]
      user = User.find_by(stripe_id: customer_id)
      activate_at = Time.zone.now
      current_period_end = Stripe::Subscription.retrieve(subscription_id).current_period_end
      expire_at = Time.zone.at(current_period_end).to_datetime

      if data_object["billing_reason"] == "subscription_create"
        payment_intent_id = data_object["payment_intent"]
        payment_intent = Stripe::PaymentIntent.retrieve(payment_intent_id)
        Stripe::Subscription.update(subscription_id, default_payment_method: payment_intent.payment_method)

        unless user.active_premium?
          PremiumAccess.create(user: user, activate_at: activate_at, expire_at: expire_at,
            provider_id: subscription_id, provider: :stripe)
        end
      else
        premium_access = PremiumAccess.where.not(provider_id: nil).find_by(provider_id: subscription_id)
        premium_access.update(expire_at: expire_at)
      end
    end

    render json: {status: "success"}.to_json
  end
end
