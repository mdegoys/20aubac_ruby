# frozen_string_literal: true

class StaticPagesController < ApplicationController
  def home
    @published_essays_count = Essay.published.count
    @last_modified_essays = Essay.top_by(:updated_at)
    @most_popular_essays = Essay.top_by(:hits_nb_last30d)
  end

  def about_whatwedo
  end

  def about_berewarded
  end

  def about_premiumaccess
  end

  def about_legalstatement
  end

  def about_partners
  end

  def site_plan
  end
end
