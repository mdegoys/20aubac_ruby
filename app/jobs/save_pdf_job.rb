# frozen_string_literal: true

class SavePdfJob < ApplicationJob
  queue_as :default

  def perform(essay_content_id)
    essay_content = EssayContents::External.find_by(id: essay_content_id)
    access_key = Rails.application.credentials.dig(:pdflayer, :api_key)
    url = "http://api.pdflayer.com/api/convert?access_key=#{access_key}&document_url=#{essay_content.url}"
    response = HTTP.get(url)
    pdf = response.body
    save_path = Rails.root.join("tmp/#{essay_content_id}.pdf")
    File.open(save_path, "wb") do |file|
      file << pdf
    end
    essay_content.pdf.attach(io: File.open(save_path), filename: "#{essay_content_id}.pdf")
  end
end
