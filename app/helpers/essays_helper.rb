# frozen_string_literal: true

module EssaysHelper
  def bbcode_to_html(string)
    new_string = string.gsub(/\[b\](.+?)\[\/b\]/i, '<span class="font-bold">\1</span>')
    new_string = new_string.gsub(/\[i\](.+?)\[\/i\]/i, '<span class="italic">\1</span>')
    new_string = new_string.gsub(/\[u\](.+?)\[\/u\]/i, '<span class="italic bg-gray-200">\1</span>')
    new_string.gsub(/\[tp\](\s*(\p{L}+).*)\[\/tp\]/i, '<h2 id="\2">\1</h2>')
  end

  def remove_superflous_p_tags(string)
    string.gsub(/<p>\s*<h2(.*)<\/h2>\s*<\/p>/, '<h2 class="pt-6 pb-4 scroll-mt-20"\1</h2>')
  end

  def extract_plan(string)
    matches = string.scan(/\[tp\](.+?)\[\/tp\]/)
    matches.map do |match|
      "<a href='##{match[0].match(/\w+/)}'>#{match[0]}</a>"
    end
  end
end
