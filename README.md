# README

Rails version of https://www.20aubac.fr

To run the app, clone the repository, then execute the following commands:

## Install the dependencies

`bundle install`

## Create the database and load the schema and seeds

`rails db:create`\
`rails db:schema:load`\
`rails db:seed`

## Run the app

`rails s`
