# frozen_string_literal: true

namespace :essays do
  task align_dates: :environment do
    essays = Essay.all
    puts "Going to update #{essays.count} essays"

    ActiveRecord::Base.transaction do
      essays.includes(:essay_content).find_each do |essay|
        print "essay id: #{essay.id}\n"
        essay.update(created_at: essay.essay_content.created_at, updated_at: essay.essay_content.updated_at)
      end
    end

    puts "All done now!"
  end
end
