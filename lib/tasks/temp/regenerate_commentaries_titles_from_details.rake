# frozen_string_literal: true

namespace :topics do
  task regenerate_commentaries_titles_from_details: :environment do
    topics = Topic.joins([category: :group]).where(group: {exercice_type: Group.exercice_types[:commentaire]})
    puts "Going to update #{topics.count} topics"

    ActiveRecord::Base.transaction do
      topics.find_each do |topic|
        print "topic id: #{topic.id}\n"
        topic_title_before = topic.title
        topic.save
        print "Nouveau titre: #{topic.title}\n" unless topic.title == topic_title_before
      end
    end

    puts "All done now!"
  end
end
