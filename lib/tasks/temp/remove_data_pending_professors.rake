# frozen_string_literal: true

namespace :users do
  task remove_data_pending_professors: :environment do
    pending_professors = User.professeur.where(validation_status: 0)
    puts "Going to update #{pending_professors.count} pending professors"

    ActiveRecord::Base.transaction do
      pending_professors.find_each do |professor|
        print "professor id: #{professor.id}\n"
        professor.update(phone_number: nil, resume: nil, level: :eleve)
      end
    end

    puts "All done now!"
  end
end
