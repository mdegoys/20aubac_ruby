# frozen_string_literal: true

namespace :subscriptions do
  task populate_provider_field: :environment do
    subscriptions = Subscription.all
    puts "Going to update #{subscriptions.count} subscriptions"

    ActiveRecord::Base.transaction do
      subscriptions.each do |subscription|
        print "subscription id: #{subscription.id}\n"

        provider = if subscription.provider_id.blank?
          :no_provider
        elsif /^sub/.match?(subscription.provider_id)
          :stripe
        else
          :mobiyo
        end
        if subscription.provider.blank?
          subscription.update(provider: provider)
        elsif subscription.provider != provider.to_s
          puts "Provider is wrong here ?"
        end
      end
    end

    puts "All done now!"
  end
end
