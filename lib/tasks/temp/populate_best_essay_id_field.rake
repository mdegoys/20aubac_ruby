# frozen_string_literal: true

namespace :topics do
  task populate_best_essay_ids: :environment do
    displayable_topics = Topic.displayable
    puts "Going to update #{displayable_topics.count} topics"

    ActiveRecord::Base.transaction do
      displayable_topics.each do |topic|
        print "topic id: #{topic.id}\n"
        topic.update_attribute(:best_essay_id, topic.best_displayable_essay.id)
      end
    end

    puts "All done now!"
  end
end
