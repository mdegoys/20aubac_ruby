# frozen_string_literal: true

task reassign_enums_indexes: :environment do
  ActiveRecord::Base.transaction do
    query = <<-SQL
        UPDATE essays
        SET status =  status - 1
    SQL

    ActiveRecord::Base.connection.execute(query)

    query = <<-SQL
        UPDATE groups
        SET exercice_type = exercice_type - 1, subject = subject - 1
    SQL

    ActiveRecord::Base.connection.execute(query)

    query = <<-SQL
        UPDATE users
        SET level = level - 1
    SQL

    ActiveRecord::Base.connection.execute(query)
  end

  puts "All done now!"
end
