# frozen_string_literal: true

namespace :subscriptions do
  task populate_activate_at_field: :environment do
    expirable_subscriptions = Subscription.where.not(expire_at: nil)
    puts "Going to update #{expirable_subscriptions.count} subscriptions"

    ActiveRecord::Base.transaction do
      expirable_subscriptions.each do |subscription|
        print "subscription id: #{subscription.id}\n"
        subscription.update(activate_at: subscription.created_at)
      end
    end

    puts "All done now!"
  end
end
