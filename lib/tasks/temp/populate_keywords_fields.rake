# frozen_string_literal: true

namespace :essay_contents do
  task populate_keywords: :environment do
    essay_contents_internals = EssayContents::Internal.all
    puts "Going to update #{essay_contents_internals.count} internal essays"

    ActiveRecord::Base.transaction do
      essay_contents_internals.each do |essay_content|
        print "essay content id : #{essay_content.id}\n"
        essay_content.update(keywords: essay_content.topic.keywords)
      end
    end

    essay_contents_externals = EssayContents::External.all
    puts "Going to update #{essay_contents_externals.count} external essays"

    ActiveRecord::Base.transaction do
      essay_contents_externals.each do |essay_content|
        print "essay content id : #{essay_content.id}\n"
        essay_content.update(keywords: essay_content.topic.keywords)
      end
    end

    puts "All done now!"
  end
end
