# frozen_string_literal: true

namespace :essays do
  task populate_validated_at: :environment do
    essays = Essay.published
    puts "Going to update #{essays.count} essays"

    ActiveRecord::Base.transaction do
      essays.each do |essay|
        print "essay id: #{essay.id}\n"
        essay.update_columns(validated_at: essay.updated_at)
      end
    end

    puts "All done now!"
  end
end
