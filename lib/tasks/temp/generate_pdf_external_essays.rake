# frozen_string_literal: true

namespace :essays do
  task generate_pdf: :environment do
    essay_contents = EssayContents::External
    puts "Going to update #{essay_contents.count} essays contents"

    ActiveRecord::Base.transaction do
      essay_contents.find_each do |essay_content|
        print "essay_content id: #{essay_content.id}\n"
        SavePdfJob.perform_now(essay_content.id)
        sleep 30
      end
    end
    sleep 300

    puts "All done now!"
  end
end
