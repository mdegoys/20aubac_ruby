# frozen_string_literal: true

task check_serp_position: :environment do
  access_key = Rails.application.credentials.dig(:serpstack, :api_key)
  essay_contents = EssayContents::Internal.published
  current_date = Time.zone.now
  current_year = current_date.year
  current_month = current_date.month
  first_day_of_month = Date.new(current_year, current_month, 1)
  essay_contents
    .where(premium_updated_at: nil)
    .or(essay_contents.where("premium_updated_at < ?", first_day_of_month))
    .find_each do |essay_content|
    params = {
      access_key: access_key,
      query: essay_content.essay.topic.title,
      engine: "google",
      type: "web",
      device: "desktop",
      google_domain: "google.fr",
      gl: "fr",
      hl: "fr"
    }
    uri = URI("https://api.serpstack.com/search")
    uri.query = URI.encode_www_form(params)
    json = HTTP.get(uri)
    api_response = JSON.parse(json)

    next if api_response["organic_results"].empty?

    first_domain = api_response["organic_results"][0]["domain"]
    premium = first_domain == "www.20aubac.fr"
    # rubocop:disable Rails/SkipsModelValidations
    essay_content.update_columns(premium: premium, premium_updated_at: current_date)
    # rubocop:enable Rails/SkipsModelValidations
    print "*"
  end
end
