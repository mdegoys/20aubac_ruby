# frozen_string_literal: true

task reset_stats_last30d: :environment do
  # rubocop:disable Rails/SkipsModelValidations
  Essay.all.each { |essay| essay.update_columns(hits_nb_last30d: 0) }
  EssayContents::External.all.each { |essay_content| essay_content.update_columns(archives_access_count: 0) }
  # rubocop:enable Rails/SkipsModelValidations
  AdminMailer.hits_reset.deliver_later
end
