# frozen_string_literal: true

Stripe.api_key = Rails.application.credentials.dig(:stripe, :api_key)

class StripeHelper
  class << self
    def create_customer(user)
      return unless user
      return unless user.eleve?
      return if user.stripe_id.present?

      Stripe::Customer.create(email: user.email)
    end

    def status_for(subscription_id)
      Stripe::Subscription.retrieve(subscription_id).status
    end

    def create_subscription(customer_id, price_id)
      Stripe::Subscription.create(
        customer: customer_id,
        items: [{
          price: price_id
        }],
        payment_behavior: "default_incomplete",
        expand: ["latest_invoice.payment_intent"]
      )
    end

    def cancel_subscription(subscription_id)
      Stripe::Subscription.delete(subscription_id)
    end
  end
end
