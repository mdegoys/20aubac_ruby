# frozen_string_literal: true

class RomanFiguresHelper
  class << self
    ROMAN_TO_ARABIC_MAP = {
      M: 1000,
      CM: 900,
      D: 500,
      CD: 400,
      C: 100,
      XC: 90,
      L: 50,
      XL: 40,
      X: 10,
      IX: 9,
      V: 5,
      IV: 4,
      I: 1
    }

    def roman_to_arabic(roman_number)
      result = 0
      left_to_check = roman_number

      until left_to_check.empty?
        roman_to_remove = ROMAN_TO_ARABIC_MAP.keys.find { |roman| left_to_check.match(/^#{roman}/) }
        left_to_check = left_to_check.sub(roman_to_remove.to_s, "")
        result += ROMAN_TO_ARABIC_MAP[roman_to_remove.to_sym]
      end

      result
    end
  end
end
