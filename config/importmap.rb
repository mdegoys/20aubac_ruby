# Pin npm packages by running ./bin/importmap

pin "application", preload: true
pin "statistics"
pin "@hotwired/turbo-rails", to: "turbo.min.js", preload: true
pin "@hotwired/stimulus", to: "stimulus.min.js", preload: true
pin "@hotwired/stimulus-loading", to: "stimulus-loading.js", preload: true
pin "chartkick", to: "chartkick.js"
pin "Chart.bundle", to: "Chart.bundle.js"
pin "essay_edit"
pin "stripe_payment"
pin_all_from "app/javascript/controllers", under: "controllers"
