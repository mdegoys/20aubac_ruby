# frozen_string_literal: true

Rails.application.routes.draw do
  match "/404", as: "not_found", to: "errors_pages#not_found", via: :all
  match "/422", as: "unprocessable_entity", to: "errors_pages#internal_server_error", via: :all
  match "/500", as: "internal_error", to: "errors_pages#internal_server_error", via: :all
  get "/sitemap.xml", to: redirect("https://20aubac.s3.eu-west-3.amazonaws.com/sitemap.xml")

  devise_for :users,
    path: "compte",
    skip: [:passwords],
    path_names: {
      sign_in: "acceder",
      sign_out: "deconnecter",
      sign_up: "creer",
      edit: "editer"
    },
    controllers: {registrations: "registrations"}

  authenticate :user, ->(u) { u.admin? } do
    mount ActiveAnalytics::Engine, at: "analytics" # http://localhost:3000/analytics
  end

  devise_scope :user do
    match "/compte/recuperer" => "devise/passwords#create", :as => :user_password, :via => [:post]
    match "/compte/recuperer" => "devise/passwords#update", :via => %i[put patch]
    get "compte/recuperer", to: "devise/passwords#new", as: :new_user_password
    get "mdp/editer", to: "devise/passwords#edit", as: :edit_user_password
  end

  post "acces-premium/abonnement-stripe", to: "premium_accesses#create_stripe_subscription"
  post "acces-premium/notification-stripe", to: "premium_accesses#webhook_stripe"
  put "acces-premium/:id/annuler", to: "premium_accesses#cancel"
  put "acces-premium/:id/activer", to: "premium_accesses#activate"

  root "static_pages#home"
  get "apropos-presentation", to: "static_pages#about_whatwedo"
  get "apropos-publication", to: "static_pages#about_berewarded"
  get "apropos-premium", to: "static_pages#about_premiumaccess"
  get "apropos-mentions", to: "static_pages#about_legalstatement"
  get "apropos-partenaires", to: "static_pages#about_partners"
  get "plan-site", to: "static_pages#site_plan"

  get "membres-accueil", to: "users_pages#home"
  get "membres-corriges", to: "users_pages#essays"
  get "membres-accespremium", to: "users_pages#premium_accesses"
  get "membres-premiumabonnement", to: "users_pages#premium_recurring"

  get "admin-corriges", to: "admin_pages#essays"
  get "admin-corriges-externes", to: "admin_pages#external_essays"
  get "admin-plans", to: "admin_pages#plans"
  get "admin-audios", to: "admin_pages#audios"
  get "admin-titres", to: "admin_pages#titles"
  get "admin-sujets", to: "admin_pages#topics"
  get "admin-categories-incompletes", to: "admin_pages#categories_missing_info"
  get "admin-influences", to: "admin_pages#influences"
  get "admin-categories-orphelines", to: "admin_pages#orphan_categories"
  get "admin-professeurs", to: "admin_pages#professors"
  get "admin-statistiques", to: "admin_pages#statistics"

  get "corriges-populaires", to: "essays#most_popular"
  get "corriges-recents", to: "essays#recently_updated"

  get "recherche", to: "topics#search"

  namespace :essay_contents, path: "" do
    resources :internals, path: "corriges", except: %i[index] do
      put "publish", on: :member
      put "validate", on: :member
      put "premiumize", on: :member
      put "update_topic", on: :member
    end

    resources :externals, path: "corriges-externes", only: %i[show destroy] do
      put "archive", on: :member
      get "archive-html", on: :member
      get "archive-pdf", on: :member
    end
  end

  namespace :quiz do
    resource :answers, path: "", only: %i[show new create]
  end

  get "categories/karl-popper/sujets", to: redirect("/categories/popper/sujets")
  get "categories/pierre-duhem/sujets", to: redirect("/categories/duhem/sujets")
  get "categories/alain-badiou/sujets", to: redirect("/categories/badiou/sujets")
  get "categories/rousseau/sujets", to: redirect("/categories/rousseau-philo/sujets")
  get "categories/jean-jacques-rousseau/sujets", to: redirect("/categories/rousseau-francais/sujets")
  get "categories/jean-paul-sartre/sujets", to: redirect("/categories/sartre-philo/sujets")
  get "categories/sartre/sujets", to: redirect("/categories/sartre-francais/sujets")
  get "categories/diderot/sujets", to: redirect("/categories/diderot-philo/sujets")
  get "categories/denis-diderot/sujets", to: redirect("/categories/diderot-francais/sujets")
  get "categories/montesquieu/sujets", to: redirect("/categories/montesquieu-philo/sujets")
  get "categories/charles-louis-montesquieu/sujets", to: redirect("/categories/montesquieu-francais/sujets")
  get "categories/russel/sujets", to: redirect("/categories/russell/sujets")

  get "categories/devoir-respect/sujets", to: redirect("/categories/devoir/sujets")
  get "categories/histoire/sujets", to: redirect("/categories/temps/sujets")
  get "categories/justice-droit/sujets", to: redirect("/categories/justice/sujets")
  get "categories/existence-temps/sujets", to: redirect("/categories/temps/sujets")
  get "categories/travail-technique/sujets", to: redirect("/categories/travail/sujets")
  get "categories/societe/sujets", to: redirect("/categories/etat/sujets")
  get "categories/desir/sujets", to: redirect("/categories/bonheur/sujets")
  get "categories/perception/sujets", to: redirect("/categories/conscience/sujets")
  get "categories/autrui/sujets", to: redirect("/categories/conscience/sujets")
  get "categories/theorie-experience/sujets", to: redirect("/categories/science/sujets")
  get "categories/demonstration-logique/sujets", to: redirect("/categories/raison/sujets")
  get "categories/matiere-esprit/sujets", to: redirect("/categories/conscience/sujets")
  get "categories/interpretation/sujets", to: redirect("/categories/conscience/sujets")
  get "categories/philosophie/sujets", to: redirect("/categories/verite/sujets")

  resources :categories, only: %i[index edit update destroy] do
    get "/:matiere-:type", action: :index, on: :collection, as: "params"
    resources :topics, path: "sujets", only: %i[index] do
      get "/:page", action: :index, on: :collection, as: "params"
    end
  end

  resources :topics, path: "sujets", except: %i[index show] do
    get "/:matiere-:type", action: :index_all, on: :collection, as: "params"
  end

  get "francais/commentaires-composes.html", to: redirect("/categories/francais-commentaire")
  get "philo/corriges-dissertation-:slug.html", to: redirect("/categories/%{slug}/sujets")
  get "philo/corriges-commentaire-:slug.html", to: redirect("/categories/%{slug}/sujets")
  get "francais/commentaires-composes-:slug.html", to: redirect("/categories/%{slug}/sujets")
  get "philo/corriges-:type.html", to: redirect("/categories/philosophie-%{type}")
  get "plan-site.html", to: redirect("/plan-site")
  get "apropos-presentation.html", to: redirect("/apropos-presentation")
  get "apropos-contribuez.html", to: redirect("/apropos-publication")
  get "apropos-mentions.html", to: redirect("/apropos-mentions")
  get "recherche.html", to: redirect("/recherche")
  get "compte-creer.html", to: redirect("/compte/creer")
  get "compte-acceder.html", to: redirect("/compte/acceder")
  get "compte-recuperer.html", to: redirect("/compte/recuperer")
  get "aide-cgu.html", to: redirect("/aide-cgu")
  get "francais/commentaires-composes.html", to: redirect("/categories/francais-commentaire")
  get "philo/corriges-dissertation-:slug.html", to: redirect("/categories/%{slug}/sujets")
  get "philo/corriges-commentaire-:slug.html", to: redirect("/categories/%{slug}/sujets")
  get "francais/commentaires-composes-:slug.html", to: redirect("/categories/%{slug}/sujets")
  get "philo/corriges-:type.html", to: redirect("/categories/philosophie-%{type}")
  # TO BE REMOVED: force redirection for legacy urls containing  "-r" in keywords
  get "philo/dissertation-15-dire-autrui-semblable-dire-ressemble-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-16-rapports-autres-necessairement-ordre-conflit-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-53-avoir-raison-contre-autres-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-58-religion-liberte-excluent-elles-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-73-preferer-revolte-resignation-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-79-epictete-entretiens-role-philosophie-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-85-recherche-bonheur-necessairement-immorale-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-88-raison-oppose-toutes-formes-croyance-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-98-usage-raison-garantie-contre-illusion-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-106-accomplir-tous-desirs-bonne-regle-vie-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-124-descartes-lettre-a-elisabeth-rapport-entre-bonheur-chance-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-137-platon-gorgias-457d-458a-il-y-a-plus-grand-avantage-a-etre-refute-que-refuter-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-140-nietzsche-par-dela-bien-mal-role-philosophe-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-148-kant-critique-raison-pratique-loi-morale-liberte-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-157-freud-repression-desirs-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-194-que-veut-dire-se-rendre-maitre-ses-passions-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-204-est-il-raisonnable-aimer-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-208-obeir-est-ce-renoncer-liberte-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-228-religion-nourrit-elle-amour-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-265-travailler-rend-il-libre-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-273-rousseau-contrat-social-passage-etat-nature-etat-civil-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-290-est-ce-recours-experience-garantit-caractere-scientifique-theorie-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-299-bergson-energie-spirituelle-raison-reel-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-300-societe-peut-elle-passer-religion-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-314-passe-revivre-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-340-existe-t-il-homme-questions-sans-reponse-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-354-suffit-il-avoir-raison-convaincre-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-362-etre-libre-renconter-aucun-obstacle-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-374-prendre-risque-penser-par-soi-meme-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-394-platon-republique-allegorie-caverne-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-398-rousseau-julie-ou-nouvelle-heloise-malheur-a-qui-a-plus-rien-a-desirer-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-406-comment-savons-nous-que-nous-ne-revons-pas-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-412-peut-on-se-fier-a-raison-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-430-malebranche-recherche-verite-raison-universelle-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-431-rester-fidele-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-444-raison-peut-nous-servir-guide-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-449-liberte-passe-par-refus-inconscient-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-470-rousseau-contrat-social-systeme-social-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-480-peut-il-y-avoir-mauvais-usage-raison-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-484-est-ce-regard-spectateur-fait-oeuvre-art-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-485-philosopher-compliquer-vie-rien-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-488-faut-il-renoncer-a-faire-travail-valeur-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-492-humanite-concevoir-sans-religion-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-513-est-ce-meme-chose-faire-respecter-droit-par-force-fonder-droit-sur-force-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-519-kant-critique-raison-pratique-conscience-coupable-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-520-rousseau-illusion-verite-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-524-pour-bien-penser-rien-aimer-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-578-existe-t-il-droit-revolte-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-582-morale-role-a-jouer-sciences-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-604-faut-il-redouter-machines-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-609-combattre-injustice-n-est-ce-pas-respecter-droit-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-610-probleme-moral-peut-il-recevoir-solution-certaine-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-619-descartes-meditations-metaphysiques-dieu-trompeur-verite-rationnelle-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-633-art-obeit-il-aucune-regle-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-679-malebranche-entretiens-sur-metaphysique-religion-passions-bonheur-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-681-ce-que-homme-accompli-par-travail-retourner-contre-lui-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-683-toute-verite-necessairement-rationnelle-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-687-etre-raisonnable-est-ce-renoncer-desirs-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-691-responsable-ce-dont-ai-pas-conscience-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-694-peut-on-avoir-raison-contre-faits-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-700-bergson-rire-mots-etiquettes-realite-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-704-homme-libre-doit-il-refuser-toute-censure-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-710-platon-republique-571b-572d-plaisirs-qui-contredisent-aux-lois-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-712-kant-critique-raison-pratique-liberte-nature-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-719-chercher-verite-prendre-risque-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-753-doit-on-considerer-religion-comme-ennemie-raison-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-792-a-quoi-sert-la-religion-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-801-verite-releve-ce-qui-est-demontrable-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-807-foi-religieuse-exclut-elle-tout-recours-a-raison-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-822-peut-on-combattre-croyance-par-raisonnement-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-833-doit-on-respecter-nature-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-837-schopenhauer-monde-comme-volonte-comme-representation-quatrieme-livre-paragraphe-58-bonheur-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-845-desir-peut-il-satisfaire-realite-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-856-doute-est-il-echec-raison-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-872-est-il-raisonnable-critiquer-progres-technique-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-881-recherche-bonheur-illusion-r:id.html", to: redirect("/corriges/%{id}")
  get "francais/commentaire-6318-apollinaire-alcools-nuit-rhenane-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-893-langage-rapproche-ou-separe-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-895-religion-peut-elle-etre-seulement-affaire-privee-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-905-liberte-peut-elle-definir-comme-obeissance-raison-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-913-obeier-renoncer-liberte-penser-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-926-freud-avenir-illusion-idees-religieuses-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-930-faut-il-renoncer-idee-justice-universelle-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-942-epictete-manuel-double-role-philosophie-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-946-toutes-opinions-respectables-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-952-leibniz-discours-metaphysique-miracle-regularite-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-960-conviction-avoir-raison-obstacle-dialogue-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-963-pascal-pensees-rien-ne-nous-plait-que-combat-mais-non-pas-victoire-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-982-pourquoi-refuse-t-on-conscience-animal-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-985-politique-repond-elle-a-nos-besoins-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-990-russel-problemes-philosophie-valeur-philosophie-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-993-vie-bien-remplie-vie-bien-vecue-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-1112-a-reconnait-on-fausse-science-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-6441-bergson-rire-mots-etiquettes-realite-extrait-4-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-1477-doit-on-respect-au-vivant-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-1620-croire-est-ce-renoncer-a-savoir-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-1712-peut-on-affirmer-que-ne-croire-en-rien-est-progres-pour-homme-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-1902-faire-table-rase-passe-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-2259-culture-nous-rend-elle-plus-humains-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-2335-liberation-passe-t-elle-par-refus-inconscient-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-2497-passion-nous-eloigne-t-elle-realite-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-2543-reflexion-philosophique-nous-detache-t-elle-monde-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-2585-l-usage-raison-fournit-il-seule-garantie-possible-notre-bonheur-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-2778-toute-verite-est-elle-relative-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-2872-pulsion-instinctive-est-elle-plus-naturelle-que-comportement-rationnel-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-3509-valeurs-morales-relatives-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-3648-peut-on-reconnaitre-a-homme-place-particuliere-dans-nature-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-3718-l-homme-peut-il-se-passer-religion-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-3865-usage-notre-raison-rend-il-libre-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-4054-peut-il-etre-raisonnable-desobeir-loi-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-4311-puis-je-invoquer-inconscient-sans-ruiner-morale-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-4442-rire-tout-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-4544-toute-expression-religieuse-doit-elle-etre-rejetee-vie-publique-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-4799-puis-je-au-nom-conscience-refuser-soumettre-aux-lois-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-4909-que-recherche-t-on-reclamant-plus-liberte-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-4911-que-reproche-t-on-a-celui-qu-on-traite-inconscient-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-4945-sommeil-raison-engendre-t-il-monstres-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-5218-qu-est-il-raisonnable-croire-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-5250-reconnait-on-artiste-a-son-savoir-faire-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-5359-responsables-nos-desirs-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-5632-vouloir-retourner-a-vie-naturelle-a-t-il-sens-pour-homme-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-5744-questions-aucune-sciences-repond-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-5747-art-peut-passer-regles-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-5751-bergson-deux-sources-morale-religion-raison-religion-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-5752-rousseau-amour-propre-pitie-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-5753-art-transforme-conscience-reel-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-5755-schopenhauer-monde-volonte-representation-role-etat-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-5759-kant-logique-connaissances-empiriques-rationnelles-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-5763-schopenhauer-monde-comme-volonte-comme-representation-bonheur-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-5766-descartes-regles-pour-direction-esprit-regle-ii-mathematiques-montrent-droit-chemin-verite-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-5770-realiser-tous-desirs-regle-vie-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-5774-role-historien-juger-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-5776-recherche-verite-etre-desinteressee-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-6274-hume-enquete-sur-entendement-humain-raisonnements-experiences-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-5782-renoncer-desirs-heureux-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-5786-recourir-au-langage-renoncer-violence-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-5798-la-question-suis-admet-reponse-exacte-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-5809-amitie-relation-ideale-autrui-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-5813-pourquoi-avons-nous-mal-a-reconnaitre-verite-r:id.html", to: redirect("/corriges/%{id}")
  get "francais/commentaire-5826-rabelais-gargantua-chapitre-xxiii-r:id.html", to: redirect("/corriges/%{id}")
  get "francais/commentaire-5830-rousseau-nouvelle-heloise-lettre-x-r:id.html", to: redirect("/corriges/%{id}")
  get "francais/commentaire-5831-racine-andromaque-acte-iii-scene-8-r:id.html", to: redirect("/corriges/%{id}")
  get "francais/commentaire-5832-victor-hugo-ruy-blas-r:id.html", to: redirect("/corriges/%{id}")
  get "francais/commentaire-5846-fontaine-rat-huitre-r:id.html", to: redirect("/corriges/%{id}")
  get "francais/commentaire-5848-rabelais-gargantua-vie-dans-abbaye-theleme-chapitre-lvii-r:id.html", to: redirect("/corriges/%{id}")
  get "francais/commentaire-5849-henri-michaux-jeteeparue-dans-nuit-remue-r:id.html", to: redirect("/corriges/%{id}")
  get "francais/commentaire-5858-du-bellay-regrets-sonnet-xxxii-r:id.html", to: redirect("/corriges/%{id}")
  get "francais/commentaire-5859-du-bellay-regrets-sonnet-xxxi-heureux-ulysse-r:id.html", to: redirect("/corriges/%{id}")
  get "francais/commentaire-5860-du-bellay-antiquites-rome-sonnet-xxv-r:id.html", to: redirect("/corriges/%{id}")
  get "francais/commentaire-5861-ronsard-sonnets-helene-soir-qu-amour-fit-salle-descendre-r:id.html", to: redirect("/corriges/%{id}")
  get "francais/commentaire-5864-zola-germinal-partie-v-chapitre-5-revolte-ouvriers-r:id.html", to: redirect("/corriges/%{id}")
  get "francais/commentaire-5866-rimbaud-lettre-voyant-r:id.html", to: redirect("/corriges/%{id}")
  get "francais/commentaire-5867-rimbaud-poesies-bateau-ivre-r:id.html", to: redirect("/corriges/%{id}")
  get "francais/commentaire-5868-rimbaud-illuminations-ponts-r:id.html", to: redirect("/corriges/%{id}")
  get "francais/commentaire-5870-eluard-au-rendez-vous-allemand-r:id.html", to: redirect("/corriges/%{id}")
  get "francais/commentaire-5885-rousseau-confessions-livre-i-peigne-casse-extrait-2-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-5890-est-on-autant-plus-libre-raisons-agir-fait-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-5893-raison-peut-elle-rendre-raison-tout-r:id.html", to: redirect("/corriges/%{id}")
  get "francais/commentaire-5901-diderot-neveu-rameau-fin-oeuvre-r:id.html", to: redirect("/corriges/%{id}")
  get "francais/commentaire-5910-fontaine-fables-rat-etait-retire-monde-r:id.html", to: redirect("/corriges/%{id}")
  get "francais/commentaire-5917-rousseau-confessions-livre-ii-ruban-vole-r:id.html", to: redirect("/corriges/%{id}")
  get "francais/commentaire-5918-verlaine-poemes-saturniens-reve-familier-r:id.html", to: redirect("/corriges/%{id}")
  get "francais/commentaire-6321-balzac-peau-chagrin-chapitre-1-deux-amis-assirent-en-riant-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-6025-rousseau-emile-ou-education-on-faconne-plantes-par-culture-hommes-par-education-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-6050-rousseau-lettres-ecrites-montagne-viii-il-y-a-point-liberte-sans-lois-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-6051-rousseau-contrat-social-livre-i-chap-3-droit-plus-fort-r:id.html", to: redirect("/corriges/%{id}")
  get "francais/commentaire-6057-fontaine-fables-chene-roseau-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-6068-malebranche-recherche-verite-sentiment-interieur-notre-liberte-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-6074-rousseau-julie-ou-nouvelle-heloise-plaisir-desirer-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-6075-rousseau-contrat-social-etat-vraiment-libre-r:id.html", to: redirect("/corriges/%{id}")
  get "francais/commentaire-6081-rousseau-confessions-livre-i-peigne-casse-r:id.html", to: redirect("/corriges/%{id}")
  get "francais/commentaire-6085-stendhal-rouge-noir-partie-i-chapitre-x-ascension-julien-en-montagne-r:id.html", to: redirect("/corriges/%{id}")
  get "francais/commentaire-6086-stendhal-rouge-noir-partie-ii-chapitre-41-discours-julien-aux-jures-r:id.html", to: redirect("/corriges/%{id}")
  get "francais/commentaire-6087-stendhal-rouge-noir-livre-i-chapitre-19-dilemme-r:id.html", to: redirect("/corriges/%{id}")
  get "francais/commentaire-6088-stendhal-rouge-noir-livre-ii-chapitre-40-lettre-mme-renal-aux-jures-r:id.html", to: redirect("/corriges/%{id}")
  get "francais/commentaire-6089-stendhal-rouge-noir-livre-i-chapitre-7-perception-julien-par-mme-renal-r:id.html", to: redirect("/corriges/%{id}")
  get "francais/commentaire-6090-stendhal-rouge-noir-livre-i-chapitre-6-premiere-rencontre-r:id.html", to: redirect("/corriges/%{id}")
  get "francais/commentaire-6091-stendhal-rouge-noir-livre-i-chapitre-18-rencontre-eveque-agde-r:id.html", to: redirect("/corriges/%{id}")
  get "francais/commentaire-6092-stendhal-rouge-noir-livre-i-chapitre-1-incipit-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-6098-bonheur-est-ce-renoncer-a-ses-desirs-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-6099-raison-conduit-elle-toujours-au-bonheur-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-6100-recherche-bonheur-oppose-t-elle-a-liberte-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-6102-vivre-instant-present-est-ce-regle-vie-satisfaisante-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-6106-kant-critique-raison-pratique-morale-bonheur-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-6108-rousseau-emile-education-conscience-conscience-instinct-divin-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-6110-kant-critique-raison-pratique-conscience-coupable-2-r:id.html", to: redirect("/corriges/%{id}")
  get "francais/commentaire-6111-racine-bajazet-acte-v-scene-11-r:id.html", to: redirect("/corriges/%{id}")
  get "francais/commentaire-6112-racine-bajazet-acte-iii-scene-8-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-6127-langue-universelle-est-elle-realisable-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-6133-rationnel-irrationnel-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-6135-theories-scientifiques-decrivent-elles-realite-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-6147-l-usage-raison-permet-il-eviter-violence-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-6208-raison-doit-elle-combattre-croyances-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-6152-sommes-nous-responsables-nos-croyances-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-6207-en-quoi-homme-est-il-animal-raisonnable-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-6160-idee-retour-a-nature-a-t-elle-sens-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-6166-doit-on-tout-soumettre-a-raison-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-6171-langage-sert-il-a-exprimer-realite-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-6174-bergson-rire-mots-etiquettes-realite-extrait-2-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-6175-rousseau-emile-ou-education-iv-amour-soi-amour-propre-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-6177-religion-est-elle-qu-consolation-infantilisante-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-6196-comment-notions-mathematiques-dependant-esprit-peuvent-elles-expliquer-reel-qui-en-depend-pas-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-6201-tout-pouvoir-est-il-necessairement-repressif-r:id.html", to: redirect("/corriges/%{id}")
  get "francais/commentaire-6206-victor-hugo-ruy-blas-acte-iii-scene-5-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-6211-nos-rapports-sociaux-sont-ils-naturels-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-6212-ce-qui-divise-hommes-peut-il-etre-en-meme-temps-ce-qui-rapproche-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-6213-l-artiste-doit-il-chercher-a-rendre-compte-realite-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-6218-sens-ce-que-on-dit-se-reduit-il-a-ce-que-on-veut-dire-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-6224-est-ce-devoir-rechercher-bonheur-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-6225-admettre-hypothese-inconscient-psychique-est-ce-denier-a-homme-toute-responsabilite-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-6233-science-est-elle-incompatible-avec-religion-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-6243-peut-on-considerer-religion-comme-alienation-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-6245-bergson-deux-sources-morale-religion-chapitre-iv-quand-on-fait-proces-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-6246-rousseau-emile-ou-education-meme-instinct-anime-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-6248-bergson-pensee-mouvant-realite-coule-nous-coulons-avec-elle-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-6249-bachelard-poetique-reverie-reve-cogito-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-6256-rousseau-discours-sur-origine-inegalite-parmi-hommes-bonheur-quantite-maux-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-6261-conscience-individu-est-elle-que-reflet-societe-a-laquelle-il-appartient-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-6262-hobbes-leviathan-regles-liberte-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-6275-spinoza-traite-theologico-politique-caracteristiques-essentielles-regime-democratique-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-6278-respecter-tout-etre-vivant-est-ce-devoir-moral-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-6304-toute-croyance-est-elle-contraire-a-raison-r:id.html", to: redirect("/corriges/%{id}")
  get "francais/commentaire-6324-balzac-pere-goriot-chapitre-1-maxime-regardait-alternativement-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-6343-bergson-rire-mots-etiquettes-realite-extrait-3-r:id.html", to: redirect("/corriges/%{id}")
  get "francais/commentaire-6341-aragon-rose-reseda-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-6350-obeissance-passive-relativite-jugements-moraux-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-6355-quels-sont-criteres-reel-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-6356-qu-est-ce-qu-vie-ratee-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-6368-de-quoi-suis-je-responsable-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-6370-sagesse-folie-sont-elles-reellement-incompatibles-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-6376-descartes-recherche-verite-honnete-homme-est-pas-oblige-avoir-vu-tous-livres-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-6377-descartes-meditations-metaphysiques-i-reve-conscience-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-6387-platon-republique-iii-401-e-402-a-valeur-educative-musique-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-6388-rousseau-discours-sur-sciences-arts-derniere-reponse-luxe-est-source-mal-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-6389-rousseau-essai-sur-origine-langues-chapitre-ix-naturel-emotions-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-6390-rousseau-essai-sur-origine-langues-chapitre-ix-homme-est-naturellement-paresseux-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-6392-seneque-lettre-a-lucilius-93-1-4-vie-est-longue-si-elle-est-remplie-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-6395-peut-on-repondre-philosophiquement-a-question-en-donnant-son-opinion-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-6397-est-il-souhaitable-realiser-tout-ce-qui-est-techniquement-possible-r:id.html", to: redirect("/corriges/%{id}")
  get "francais/commentaire-6403-jean-giono-regain-ruisseau-gaudissart-r:id.html", to: redirect("/corriges/%{id}")
  get "francais/commentaire-6416-abbe-prevost-manon-lescaut-rencontre-r:id.html", to: redirect("/corriges/%{id}")
  get "francais/commentaire-6417-rimbaud-effares-r:id.html", to: redirect("/corriges/%{id}")
  get "francais/commentaire-6418-ronsard-franciade-iv-r:id.html", to: redirect("/corriges/%{id}")
  get "francais/commentaire-6419-rousseau-confessions-livre-iv-j-aime-a-marcher-a-mon-aise-arreter-quand-il-me-plait-r:id.html", to: redirect("/corriges/%{id}")
  get "francais/commentaire-6420-rousseau-confessions-livre-i-chez-graveur-ducommun-r:id.html", to: redirect("/corriges/%{id}")
  get "francais/commentaire-6421-rousseau-confessions-livre-ii-madame-warens-r:id.html", to: redirect("/corriges/%{id}")
  get "francais/commentaire-6422-rousseau-confessions-preambule-r:id.html", to: redirect("/corriges/%{id}")
  get "francais/commentaire-6423-zola-germinal-partie-v-chapitre-5-revolte-ouvriers-extrait-2-r:id.html", to: redirect("/corriges/%{id}")
  get "francais/commentaire-6424-zola-assommoir-chapitre-2-rencontre-coupeau-gervaise-r:id.html", to: redirect("/corriges/%{id}")
  get "francais/commentaire-6427-zola-curee-chapitre-i-fin-repas-bourgeois-r:id.html", to: redirect("/corriges/%{id}")
  get "francais/commentaire-6428-zola-curee-chapitre-iv-premier-inceste-entre-renee-maxime-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-6435-russel-problemes-philosophie-chapitre-xv-valeur-philosophie-traduction-2-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-6448-russel-problemes-philosophie-valeur-philosophie-traduction-3-r:id.html", to: redirect("/corriges/%{id}")
  get "francais/commentaire-6451-stendhal-rouge-noir-livre-i-chapitre-10-ascension-julien-en-montagne-extrait-2-r:id.html", to: redirect("/corriges/%{id}")
  get "francais/commentaire-6452-racine-britannicus-acte-ii-scene-2-r:id.html", to: redirect("/corriges/%{id}")
  get "francais/commentaire-6453-robert-desnos-destinee-arbitraire-ce-coeur-qui-haissait-guerre-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-6476-durkheim-formes-elementaires-vie-religieuse-autorite-obeissance-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-6477-peut-on-renoncer-a-verite-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-6478-schopenhauer-monde-comme-volonte-comme-representation-conscience-desirs-peurs-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/dissertation-6507-reconnaitre-ses-devoirs-est-ce-renoncer-a-sa-liberte-r:id.html", to: redirect("/corriges/%{id}")
  get "philo/commentaire-6509-leibniz-remarques-sur-descartes-libre-arbitre-conscience-avons-libre-arbitre-r:id.html", to: redirect("/corriges/%{id}")
  get "*subject/*keywords-r:id.html", to: redirect("/corriges/%{id}")
end
