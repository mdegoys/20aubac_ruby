# frozen_string_literal: true

require "aws-sdk-s3"
SitemapGenerator::Sitemap.default_host = "https://www.20aubac.fr"
SitemapGenerator::Sitemap.compress = false

if Rails.env.production?
  SitemapGenerator::Sitemap.adapter = SitemapGenerator::AwsSdkAdapter.new(
    "20aubac",
    aws_access_key_id: Rails.application.credentials.dig(:aws, :access_key_id),
    aws_secret_access_key: Rails.application.credentials.dig(:aws, :secret_access_key),
    aws_region: "eu-west-3"
  )
end

SitemapGenerator::Sitemap.create do
  # Put links creation logic here.
  #
  # The root path '/' and sitemap index file are added automatically for you.
  # Links are added to the Sitemap in the order they are specified.
  #
  # Usage: add(path, options={})
  #        (default options are used if you don't specify)
  #
  # Defaults: :priority => 0.5, :changefreq => 'weekly',
  #           :lastmod => Time.now, :host => default_host
  #
  # Examples:
  #
  # Add '/articles'
  #
  #   add articles_path, :priority => 0.7, :changefreq => 'daily'
  #
  # Add all articles:
  #
  #   Article.find_each do |article|
  #     add article_path(article), :lastmod => article.updated_at
  #   end
  # Static pages
  add apropos_presentation_path, changefreq: "yearly"
  add apropos_publication_path, changefreq: "yearly"
  add apropos_mentions_path, changefreq: "yearly"
  add apropos_premium_path, changefreq: "yearly"
  add apropos_partenaires_path, changefreq: "yearly"
  add plan_site_path, changefreq: "yearly"
  add recherche_path, changefreq: "yearly"

  # User pages
  add new_user_registration_path, changefreq: "yearly"
  add new_user_session_path, changefreq: "yearly"
  add new_user_password_path, changefreq: "yearly"

  # Categories page
  add params_categories_path(matiere: "philosophie", type: "dissertation")
  add params_categories_path(matiere: "philosophie", type: "commentaire")
  add params_categories_path(matiere: "francais", type: "commentaire")

  # Top essays pages
  add corriges_recents_path, changefreq: "weekly"
  add corriges_populaires_path, changefreq: "weekly"

  # Topics pages
  add params_topics_path(matiere: "philosophie", type: "dissertation"), changefreq: "weekly"
  add params_topics_path(matiere: "philosophie", type: "commentaire"), changefreq: "weekly"
  add params_topics_path(matiere: "francais", type: "commentaire"), changefreq: "weekly"

  Group.where.not(name: Group::NON_ASSIGNED).each do |group|
    group.categories
      .joins(topics: :essays)
      .where(essays: {status: :published})
      .distinct
      .each do |category|
      add category_topics_path(category)
      category_page_count = Topic.displayable
        .where(category: category)
        .page(1)
        .total_pages
      next unless category_page_count > 1

      (2..category_page_count).each do |page_number|
        add params_category_topics_path(category, page: page_number)
      end
    end
  end

  # Essays
  Essay.published.find_each do |essay|
    essay_content = essay.essay_content
    if essay_content.is_a?(EssayContents::Internal)
      add essay_contents_internal_path(essay_content), changefreq: "monthly", lastmod: essay_content.updated_at
    else
      add essay_contents_external_path(essay_content), changefreq: "monthly", lastmod: essay_content.updated_at
    end
  end
end
