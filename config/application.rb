# frozen_string_literal: true

require_relative "boot"

require "rails/all"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module App20aubac
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.paths.add "lib", eager_load: true
    config.load_defaults 7.0
    config.exceptions_app = routes

    config.i18n.load_path += Dir[Rails.root.join("my", "locales", "*.{rb,yml}")]
    config.i18n.default_locale = :fr

    config.middleware.insert_before(Rack::Runtime, Rack::Rewrite) do
      r301 %r{^/(.*)/$}, "/$1"
    end

    config.middleware.use Rack::CrawlerDetect

    # Configuration for the application, engines, and railties goes here.
    #
    # These settings can be overridden in specific environments using the files
    # in config/environments, which are processed later.
    #
    # config.time_zone = "Central Time (US & Canada)"
    # config.eager_load_paths << Rails.root.join("extras")
    config.action_view.sanitized_allowed_tags = []
    config.action_view.sanitized_allowed_attributes = []
  end
end
