# frozen_string_literal: true

source "https://rubygems.org"
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby File.read(".ruby-version").strip

gem "active_analytics"
gem "aws-sdk-s3", "~> 1.94"
gem "bcrypt", "~> 3.1.7"
gem "bootsnap", ">= 1.1.0", require: false
gem "chartkick"
gem "crawler_detect"
gem "devise"
gem "devise-i18n"
gem "groupdate"
gem "http"
gem "importmap-rails", "~> 1.1"
gem "jbuilder", "~> 2.5"
gem "kaminari"
gem "mailjet"
gem "pg", ">= 0.18", "< 2.0"
gem "pg_search"
gem "puma", "~> 3.11"
gem "rack-rewrite", "~> 1.5.0"
gem "rails", "~> 7.0.0"
gem "rails-i18n", "~> 7.0.0"
gem "redis", "~> 4.0"
gem "rorvswild"
gem "sitemap_generator"
gem "sprockets-rails"
gem "stimulus-rails"
gem "stripe", "6.5.0"
gem "tailwindcss-rails"
gem "terser"
gem "turbo-rails"

group :development, :test do
  gem "byebug", platforms: %i[mri mingw x64_mingw]
  gem "factory_bot_rails"
  gem "pry", "~> 0.14.1"
  gem "pry-rails"
  gem "rspec-rails", "~> 6.0.0rc1"
  gem "shoulda-matchers"
end

group :development do
  gem "bullet"
  gem "rubocop", require: false
  gem "rubocop-rspec", require: false
  gem "standard", require: false
  gem "standard-rails", require: false
  # Speed up commands on slow machines / big apps [https://github.com/rails/spring]
  # gem 'spring'
end

group :test do
  gem "capybara"
  gem "rails-controller-testing"
  gem "selenium-webdriver"
  gem "simplecov", require: false
  gem "webdrivers", "~> 5.0", require: false
end
