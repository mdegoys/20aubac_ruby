# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# Groupes philosophie dissertation
Group.find_or_create_by(id: 1, name: "L'existence humaine et la culture", subject: :philosophie, exercice_type: :dissertation)
Group.find_or_create_by(id: 3, name: "La connaissance", subject: :philosophie, exercice_type: :dissertation)
Group.find_or_create_by(id: 4, name: "La morale et la politique", subject: :philosophie, exercice_type: :dissertation)

# Groupes philosophie commentaire
Group.find_or_create_by(id: 6, name: "Antiquité", subject: :philosophie, exercice_type: :commentaire)
Group.find_or_create_by(id: 7, name: "Philosophie médiévale", subject: :philosophie, exercice_type: :commentaire)
Group.find_or_create_by(id: 8, name: "Renaissance", subject: :philosophie, exercice_type: :commentaire)
Group.find_or_create_by(id: 9, name: "17ème siècle", subject: :philosophie, exercice_type: :commentaire)
Group.find_or_create_by(id: 10, name: "18ème siècle", subject: :philosophie, exercice_type: :commentaire)
Group.find_or_create_by(id: 11, name: "19ème siècle", subject: :philosophie, exercice_type: :commentaire)
Group.find_or_create_by(id: 12, name: "20ème siècle", subject: :philosophie, exercice_type: :commentaire)

# Groupes francais
Group.find_or_create_by(id: 20, name: "Antiquité", subject: :francais, exercice_type: :commentaire)
Group.find_or_create_by(id: 13, name: "XVIème siècle", subject: :francais, exercice_type: :commentaire)
Group.find_or_create_by(id: 14, name: "XVIIème siècle", subject: :francais, exercice_type: :commentaire)
Group.find_or_create_by(id: 15, name: "XVIIIème siècle", subject: :francais, exercice_type: :commentaire)
Group.find_or_create_by(id: 16, name: "XIXème siècle", subject: :francais, exercice_type: :commentaire)
Group.find_or_create_by(id: 17, name: "XXème siècle", subject: :francais, exercice_type: :commentaire)

# Groupes non assignes
Group.find_or_create_by(id: 18, name: Group::NON_ASSIGNED, subject: :philosophie, exercice_type: :commentaire)
Group.find_or_create_by(id: 19, name: Group::NON_ASSIGNED, subject: :francais, exercice_type: :commentaire)
