# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2025_02_22_085427) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "action_text_rich_texts", force: :cascade do |t|
    t.string "name", null: false
    t.text "body"
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["record_type", "record_id", "name"], name: "index_action_text_rich_texts_uniqueness", unique: true
  end

  create_table "active_analytics_views_per_days", force: :cascade do |t|
    t.string "site", null: false
    t.string "page", null: false
    t.date "date", null: false
    t.bigint "total", default: 1, null: false
    t.string "referrer_host"
    t.string "referrer_path"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.index ["date"], name: "index_active_analytics_views_per_days_on_date"
    t.index ["referrer_host", "referrer_path", "date"], name: "index_active_analytics_views_per_days_on_referrer_and_date"
    t.index ["site", "page", "date"], name: "index_active_analytics_views_per_days_on_site_and_date"
  end

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", precision: nil, null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum"
    t.datetime "created_at", precision: nil, null: false
    t.string "service_name", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "active_storage_variant_records", force: :cascade do |t|
    t.bigint "blob_id", null: false
    t.string "variation_digest", null: false
    t.index ["blob_id", "variation_digest"], name: "index_active_storage_variant_records_uniqueness", unique: true
  end

  create_table "categories", force: :cascade do |t|
    t.string "name"
    t.integer "sorting_index"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.bigint "group_id"
    t.string "slug"
    t.index ["name", "group_id"], name: "index_categories_on_name_and_group_id", unique: true
    t.index ["slug"], name: "index_categories_on_slug", unique: true
  end

  create_table "essay_contents_externals", force: :cascade do |t|
    t.string "url"
    t.string "author_name"
    t.integer "author_level"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "keywords"
    t.boolean "url_unavailable", default: false
    t.integer "archives_access_count", default: 0
  end

  create_table "essay_contents_internals", force: :cascade do |t|
    t.text "content"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.bigint "user_id"
    t.text "description"
    t.string "refusal_reason"
    t.string "keywords"
    t.boolean "premium", default: false
    t.datetime "premium_updated_at"
    t.index ["user_id"], name: "index_essay_contents_internals_on_user_id"
  end

  create_table "essays", force: :cascade do |t|
    t.bigint "topic_id", null: false
    t.string "essay_content_type"
    t.integer "essay_content_id"
    t.string "refusal_reason"
    t.integer "hits_nb", default: 0
    t.integer "hits_nb_last30d", default: 0
    t.integer "status", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "validated_at"
    t.index ["essay_content_id"], name: "index_essays_on_essay_content_id"
    t.index ["topic_id"], name: "index_essays_on_topic_id"
  end

  create_table "groups", force: :cascade do |t|
    t.string "name"
    t.integer "subject"
    t.integer "exercice_type"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.index ["name"], name: "index_groups_on_name"
  end

  create_table "influences", force: :cascade do |t|
    t.integer "influencer_id"
    t.integer "influencee_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["influencee_id", "influencer_id"], name: "index_influences_on_influencee_id_and_influencer_id"
    t.index ["influencer_id", "influencee_id"], name: "index_influences_on_influencer_id_and_influencee_id"
  end

  create_table "premium_accesses", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.datetime "expire_at", precision: nil
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "provider_id"
    t.integer "provider"
    t.datetime "activate_at", precision: nil
    t.index ["provider_id"], name: "index_premium_accesses_on_provider_id"
    t.index ["user_id"], name: "index_premium_accesses_on_user_id"
  end

  create_table "quiz_answers", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "question_id"
    t.bigint "proposition_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["proposition_id"], name: "index_quiz_answers_on_proposition_id"
    t.index ["question_id"], name: "index_quiz_answers_on_question_id"
    t.index ["user_id"], name: "index_quiz_answers_on_user_id"
  end

  create_table "quiz_propositions", force: :cascade do |t|
    t.string "statement"
    t.bigint "question_id"
    t.boolean "correct", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["question_id"], name: "index_quiz_propositions_on_question_id"
  end

  create_table "quiz_questions", force: :cascade do |t|
    t.string "statement"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "categorisable_type", null: false
    t.bigint "categorisable_id", null: false
    t.index ["categorisable_type", "categorisable_id"], name: "index_quiz_questions_on_categorisable"
  end

  create_table "sub_categories", force: :cascade do |t|
    t.string "name"
    t.string "slug"
    t.integer "sorting_index"
    t.bigint "category_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "studied_texts_autonomous", default: false, null: false
    t.index ["category_id"], name: "index_sub_categories_on_category_id"
  end

  create_table "topics", force: :cascade do |t|
    t.string "title"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.integer "category_id"
    t.string "past_exam"
    t.text "studied_text"
    t.string "book"
    t.string "part"
    t.string "themes"
    t.string "edition"
    t.integer "best_essay_id"
    t.string "title_short", default: ""
    t.string "title_arabic_numbers"
    t.bigint "sub_category_id"
    t.index ["best_essay_id"], name: "index_topics_on_best_essay_id"
    t.index ["category_id"], name: "index_topics_on_category_id"
    t.index ["sub_category_id"], name: "index_topics_on_sub_category_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at", precision: nil
    t.datetime "remember_created_at", precision: nil
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.string "username"
    t.integer "level", default: 0
    t.string "last_name"
    t.string "first_name"
    t.string "phone_number"
    t.text "resume"
    t.string "stripe_id"
    t.string "quiz_selected_questions"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["stripe_id"], name: "index_users_on_stripe_id"
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "active_storage_variant_records", "active_storage_blobs", column: "blob_id"
  add_foreign_key "categories", "groups"
  add_foreign_key "essays", "topics"
  add_foreign_key "premium_accesses", "users"
  add_foreign_key "sub_categories", "categories"
  add_foreign_key "topics", "categories"
  add_foreign_key "topics", "sub_categories"
end
