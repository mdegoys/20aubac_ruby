# frozen_string_literal: true

class AddTitleArabicNumbersToTopics < ActiveRecord::Migration[7.0]
  def change
    add_column :topics, :title_arabic_numbers, :string

    reversible do |dir|
      Topic.all.find_each do |topic|
        dir.up do
          topic.title = topic.title.squeeze(" ").strip
          p topic.title unless topic.save
        end
      end
    end
  end
end
