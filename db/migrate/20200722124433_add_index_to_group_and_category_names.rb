class AddIndexToGroupAndCategoryNames < ActiveRecord::Migration[5.2]
  def change
    add_index :groups, :name
    add_index :categories, :name
  end
end
