class AddPremiumUpdatedAtToEssayContentsInternal < ActiveRecord::Migration[7.0]
  def change
    add_column :essay_contents_internals, :premium_updated_at, :datetime
  end
end
