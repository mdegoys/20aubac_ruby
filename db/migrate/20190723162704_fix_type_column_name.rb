class FixTypeColumnName < ActiveRecord::Migration[5.2]
  def change
    rename_column :topics, :type, :exercice_type
  end
end
