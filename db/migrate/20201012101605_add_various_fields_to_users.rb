class AddVariousFieldsToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :last_name, :string
    add_column :users, :first_name, :string
    add_column :users, :phone_number, :string
    add_column :users, :resume, :text
    add_column :users, :validation_status, :int
  end
end
