class AddIndexToKeywordsToEssays < ActiveRecord::Migration[6.0]
  def change
    add_index :topics, :keywords, unique: true
  end
end
