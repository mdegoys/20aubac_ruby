class AddDefaultValuesEssaysTable < ActiveRecord::Migration[5.2]
  def change
    change_column :essays, :hits_nb, :integer, default: 0
    change_column :essays, :hits_nb_last30d, :integer, default: 0
    change_column :essays, :grades_nb, :integer, default: 0
    change_column :essays, :grades_total, :integer, default: 0
    change_column :essays, :grade_admin, :integer, default: 0
    change_column :essays, :status, :varchar, default: "DRAFT"
  end
end
