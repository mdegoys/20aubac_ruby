class RemoveValidationStatusFromUsers < ActiveRecord::Migration[6.1]
  def change
    remove_column :users, :validation_status, :integer
  end
end
