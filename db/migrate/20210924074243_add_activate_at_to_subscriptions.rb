class AddActivateAtToSubscriptions < ActiveRecord::Migration[6.1]
  def change
    add_column :subscriptions, :activate_at, :datetime
  end
end
