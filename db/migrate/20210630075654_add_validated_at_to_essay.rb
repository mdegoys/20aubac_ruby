class AddValidatedAtToEssay < ActiveRecord::Migration[6.1]
  def change
    add_column :essays, :validated_at, :datetime, precision: 6
  end
end
