class AddKeywordsToTopics < ActiveRecord::Migration[6.0]
  def change
    add_column :topics, :keywords, :string
  end
end
