class RenameEssaysTable < ActiveRecord::Migration[6.1]
  def change
    rename_table :essays, :essays_internals
  end
end
