class CreateEssays < ActiveRecord::Migration[5.2]
  def change
    create_table :essays do |t|
      t.text :content

      t.timestamps
    end
  end
end
