class CreateQuizPropositions < ActiveRecord::Migration[7.0]
  def change
    create_table :quiz_propositions do |t|
      t.string :statement
      t.references :question
      t.boolean :correct

      t.timestamps
    end
  end
end
