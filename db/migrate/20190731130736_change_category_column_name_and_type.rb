class ChangeCategoryColumnNameAndType < ActiveRecord::Migration[5.2]
  def change
    rename_column :topics, :category, :category_id
    change_column :topics, :category_id, "integer USING CAST(category_id AS integer)"
  end
end
