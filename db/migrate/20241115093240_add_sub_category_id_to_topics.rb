# frozen_string_literal: true

class AddSubCategoryIdToTopics < ActiveRecord::Migration[7.0]
  def change
    add_reference :topics, :sub_category, foreign_key: true
    add_index :topics, :category_id
    add_foreign_key :topics, :categories
  end
end
