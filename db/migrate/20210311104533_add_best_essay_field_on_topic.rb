class AddBestEssayFieldOnTopic < ActiveRecord::Migration[6.1]
  def up
    add_column :topics, :best_essay_id, :integer
  end

  def down
    remove_column :topics, :best_essay_id, :integer
  end
end
