class AddSlugToCategories < ActiveRecord::Migration[6.0]
  def up
    add_column :categories, :slug, :string
    add_index :categories, :slug, unique: true
  end

  def down
    remove_column :categories, :slug
  end
end
