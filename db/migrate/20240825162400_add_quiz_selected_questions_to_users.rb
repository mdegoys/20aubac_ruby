# frozen_string_literal: true

class AddQuizSelectedQuestionsToUsers < ActiveRecord::Migration[7.0]
  def change
    add_column :users, :quiz_selected_questions, :string
  end
end
