class CreateEssaysExternals < ActiveRecord::Migration[6.0]
  def change
    create_table :essays_externals do |t|
      t.string :url
      t.string :author_name
      t.integer :author_level
      t.integer :grade_admin
      t.integer :status
      t.references :topic, null: false, foreign_key: true

      t.timestamps
    end
  end
end
