class CompleteEssayTable < ActiveRecord::Migration[5.2]
  def change
    add_reference :essays, :user, foreign_key: true
    add_reference :essays, :topic, foreign_key: true
    add_column :essays, :url, :string
    add_column :essays, :description, :text
    add_column :essays, :status, :string
    add_column :essays, :refusal_reason, :string
    add_column :essays, :grade_admin, :integer
    add_column :essays, :grades_total, :integer
    add_column :essays, :grades_nb, :integer
    add_column :essays, :hits_nb, :integer
    add_column :essays, :hits_nb_last30d, :integer
    add_column :essays, :validated_at, :timestamp
  end
end
