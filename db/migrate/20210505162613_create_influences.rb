class CreateInfluences < ActiveRecord::Migration[6.1]
  def change
    create_table :influences do |t|
      t.integer :influencer_id
      t.integer :influencee_id

      t.timestamps
    end
  end
end
