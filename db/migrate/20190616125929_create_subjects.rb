class CreateSubjects < ActiveRecord::Migration[5.2]
  def change
    create_table :subjects do |t|
      t.string :title
      t.string :type
      t.string :keywords

      t.timestamps
    end
  end
end
