class CreateCategories < ActiveRecord::Migration[5.2]
  def change
    create_table :categories do |t|
      t.string :subject
      t.string :exercice_type
      t.string :internal_name
      t.string :displayed_name
      t.string :group
      t.integer :birth_year

      t.timestamps
    end
  end
end
