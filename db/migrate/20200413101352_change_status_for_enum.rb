class ChangeStatusForEnum < ActiveRecord::Migration[5.2]
  def up
    remove_column :essays, :status
    add_column :essays, :status, :integer, default: 1
    add_index :essays, :status
  end

  def down
    remove_column :essays, :status
    add_column :essays, :status, :string, default: "DRAFT"
    remove_index :essays, :status
  end
end
