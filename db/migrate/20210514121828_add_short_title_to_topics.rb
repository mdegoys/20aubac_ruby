class AddShortTitleToTopics < ActiveRecord::Migration[6.1]
  def change
    add_column :topics, :title_short, :string, default: ""
  end
end
