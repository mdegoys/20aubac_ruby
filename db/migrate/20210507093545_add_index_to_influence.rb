class AddIndexToInfluence < ActiveRecord::Migration[6.1]
  def change
    add_index(:influences, [:influencer_id, :influencee_id])
    add_index(:influences, [:influencee_id, :influencer_id])
  end
end
