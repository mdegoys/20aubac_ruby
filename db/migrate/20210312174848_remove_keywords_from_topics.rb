class RemoveKeywordsFromTopics < ActiveRecord::Migration[6.1]
  def change
    remove_column :topics, :keywords, :string
  end
end
