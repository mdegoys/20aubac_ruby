class RemoveInternalNameFromCategories < ActiveRecord::Migration[5.2]
  def change
    remove_column :categories, :internal_name, :string
  end
end
