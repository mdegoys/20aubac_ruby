class MoveCorrectPropositionToPropositions < ActiveRecord::Migration[7.0]
  def up
    unless column_exists? :quiz_propositions, :correct
      add_column :quiz_propositions, :correct, :boolean
    end
    change_column :quiz_propositions, :correct, :boolean, default: false

    Quiz::Question.all.each do |question|
      correct_proposition = Quiz::Proposition.find(question.correct_proposition_id)
      correct_proposition.update!(correct: true)
    end
    Quiz::Proposition.where(correct: nil).update_all(correct: false)

    remove_index :quiz_questions, :correct_proposition_id
    remove_column :quiz_questions, :correct_proposition_id
  end

  def down
    add_column :quiz_questions, :correct_proposition_id, :bigint
    add_index :quiz_questions, :correct_proposition_id

    Quiz::Proposition.where(correct: true).each do |proposition|
      question = Quiz::Question.find(proposition.question_id)
      question.update!(correct_proposition_id: proposition.id)
    end

    remove_column :quiz_propositions, :correct
  end
end
