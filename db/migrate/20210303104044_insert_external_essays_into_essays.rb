class InsertExternalEssaysIntoEssays < ActiveRecord::Migration[6.1]
  def up
    rename_table :essays_externals, :essay_contents_externals

    remove_column :essay_contents_externals, :status
    remove_column :essay_contents_externals, :grade_admin
    remove_column :essay_contents_externals, :topic_id
    remove_column :essay_contents_externals, :hits_nb
    remove_column :essay_contents_externals, :hits_nb_last30d
  end
end
