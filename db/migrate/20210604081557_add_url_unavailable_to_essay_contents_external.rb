class AddUrlUnavailableToEssayContentsExternal < ActiveRecord::Migration[6.1]
  def change
    add_column :essay_contents_externals, :url_unavailable, :boolean, default: false
  end
end
