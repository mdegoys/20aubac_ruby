class RemoveUnusedFieldsFromEssays < ActiveRecord::Migration[6.0]
  def change
    remove_column :essays, :url
    remove_column :essays, :grades_total
    remove_column :essays, :grades_nb
    remove_column :essays, :validated_at
  end
end
