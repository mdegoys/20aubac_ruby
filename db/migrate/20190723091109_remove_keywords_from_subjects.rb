class RemoveKeywordsFromSubjects < ActiveRecord::Migration[5.2]
  def change
    remove_column :subjects, :keywords, :String
  end
end
