class AddHitsNbToEssaysExternal < ActiveRecord::Migration[6.0]
  def change
    add_column :essays_externals, :hits_nb, :integer, default: 0
    add_column :essays_externals, :hits_nb_last30d, :integer, default: 0
  end
end
