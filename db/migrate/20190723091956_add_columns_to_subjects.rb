class AddColumnsToSubjects < ActiveRecord::Migration[5.2]
  def change
    add_column :subjects, :subject, :string
    add_column :subjects, :category, :string
    add_column :subjects, :past_exam, :string
    add_column :subjects, :studied_text, :text
    add_column :subjects, :book, :string
    add_column :subjects, :part, :string
    add_column :subjects, :themes, :string
    add_column :subjects, :edition, :string
  end
end
