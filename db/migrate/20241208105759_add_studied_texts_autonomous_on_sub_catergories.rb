# frozen_string_literal: true

class AddStudiedTextsAutonomousOnSubCatergories < ActiveRecord::Migration[7.0]
  def change
    add_column :sub_categories, :studied_texts_autonomous, :boolean, default: false, null: false
  end
end
