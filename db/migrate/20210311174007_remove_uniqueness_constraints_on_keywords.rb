class RemoveUniquenessConstraintsOnKeywords < ActiveRecord::Migration[6.1]
  def up
    remove_index :topics, :keywords
  end

  def down
    add_index :topics, :keywords, unique: true
  end
end
