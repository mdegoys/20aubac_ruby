# frozen_string_literal: true

class AddMissingIndexes < ActiveRecord::Migration[7.0]
  disable_ddl_transaction!

  def change
    add_index :essays, :essay_content_id, algorithm: :concurrently
    add_index :premium_accesses, :provider_id, algorithm: :concurrently
    add_index :users, :stripe_id, algorithm: :concurrently
    add_index :topics, :best_essay_id, algorithm: :concurrently
  end
end
