class AddCategorisableToQuizQuestion < ActiveRecord::Migration[7.0]
  def change
    add_reference :quiz_questions, :categorisable, polymorphic: true
  end
end
