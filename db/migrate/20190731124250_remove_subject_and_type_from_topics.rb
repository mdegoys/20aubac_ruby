class RemoveSubjectAndTypeFromTopics < ActiveRecord::Migration[5.2]
  def change
    remove_column :topics, :subject, :string
    remove_column :topics, :exercice_type, :string
  end
end
