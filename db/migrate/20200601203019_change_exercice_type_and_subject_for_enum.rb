class ChangeExerciceTypeAndSubjectForEnum < ActiveRecord::Migration[5.2]
  def up
    remove_column :categories, :exercice_type
    remove_column :categories, :subject
    add_column :categories, :exercice_type, :integer, default: 1
    add_column :categories, :subject, :integer, default: 1
    add_index :categories, :exercice_type
    add_index :categories, :subject
  end

  def down
    remove_column :categories, :exercice_type
    remove_column :categories, :subject
    add_column :categories, :exercice_type, :string, default: "dissertation"
    add_column :categories, :subject, :string, default: "philosophie"
    remove_index :essays, :exercice_type
    remove_index :essays, :subject
  end
end
