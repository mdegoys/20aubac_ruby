class CleanUpCategories < ActiveRecord::Migration[5.2]
  def change
    rename_column :categories, :displayed_name, :name
    rename_column :categories, :birth_year, :sorting_index
    remove_column :categories, :subject, :string
    remove_column :categories, :exercice_type, :string
    remove_column :categories, :group, :string
    add_reference :categories, :group, foreign_key: true
  end
end
