class AddIsPremiumToEssayContentsInternal < ActiveRecord::Migration[6.1]
  def change
    add_column :essay_contents_internals, :is_premium, :boolean, default: false
  end
end
