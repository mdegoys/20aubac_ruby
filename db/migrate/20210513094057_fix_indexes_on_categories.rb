class FixIndexesOnCategories < ActiveRecord::Migration[6.1]
  def change
    remove_index(:categories, :group_id)
    remove_index(:categories, :name)
    add_index(:categories, [:name, :group_id], unique: true)
    remove_index(:categories, :slug)
    add_index(:categories, :slug, unique: true)
  end
end
