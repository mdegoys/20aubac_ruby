class InsertInternalEssaysIntoEssays < ActiveRecord::Migration[6.1]
  def up
    rename_table :essays_internals, :essay_contents_internals

    remove_column :essay_contents_internals, :status
    remove_column :essay_contents_internals, :grade_admin
    remove_column :essay_contents_internals, :topic_id
    remove_column :essay_contents_internals, :hits_nb
    remove_column :essay_contents_internals, :hits_nb_last30d
  end
end
