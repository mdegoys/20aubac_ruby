class RenameSubjectTable < ActiveRecord::Migration[5.2]
  def self.up
    rename_table :subjects, :topics
  end

  def self.down
    rename_table :topics, :subjects
  end
end
