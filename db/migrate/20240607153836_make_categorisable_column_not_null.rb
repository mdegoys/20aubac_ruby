class MakeCategorisableColumnNotNull < ActiveRecord::Migration[7.0]
  def change
    change_column_null :quiz_questions, :categorisable_id, false
    change_column_null :quiz_questions, :categorisable_type, false
  end
end
