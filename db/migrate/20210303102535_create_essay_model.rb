class CreateEssayModel < ActiveRecord::Migration[6.1]
  def change
    create_table :essays do |t|
      t.references :topic, null: false, foreign_key: true
      t.string :essay_content_type
      t.integer :essay_content_id
      t.integer :grade_admin, default: 0
      t.string :refusal_reason
      t.integer :hits_nb, default: 0
      t.integer :hits_nb_last30d, default: 0
      t.integer :status, default: 1

      t.timestamps
    end
  end
end
