class RenameSubscriptionIntoPremiumAccess < ActiveRecord::Migration[6.1]
  def change
    rename_table :subscriptions, :premium_accesses
  end
end
