class CreateGroups < ActiveRecord::Migration[5.2]
  def change
    create_table :groups do |t|
      t.string :name
      t.integer :subject
      t.integer :exercice_type

      t.timestamps
    end
  end
end
