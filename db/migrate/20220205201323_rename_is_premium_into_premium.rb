class RenameIsPremiumIntoPremium < ActiveRecord::Migration[6.1]
  def change
    rename_column :essay_contents_internals, :is_premium, :premium
  end
end
