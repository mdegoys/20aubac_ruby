class AddKeywordsFieldsForEssayContentTables < ActiveRecord::Migration[6.1]
  def change
    add_column :essay_contents_internals, :keywords, :string
    add_column :essay_contents_externals, :keywords, :string
  end
end
