class AddIndexToInternalNameInCategories < ActiveRecord::Migration[5.2]
  def change
    add_index :categories, :internal_name, unique: true
  end
end
