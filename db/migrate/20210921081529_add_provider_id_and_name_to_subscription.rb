class AddProviderIdAndNameToSubscription < ActiveRecord::Migration[6.1]
  def change
    rename_column :subscriptions, :stripe_id, :provider_id
    add_column :subscriptions, :provider, :integer
  end
end
