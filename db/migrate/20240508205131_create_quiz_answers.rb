class CreateQuizAnswers < ActiveRecord::Migration[7.0]
  def change
    create_table :quiz_answers do |t|
      t.references :user
      t.references :question
      t.references :proposition

      t.timestamps
    end
  end
end
