class ChangeEnumsDefaultValues < ActiveRecord::Migration[6.1]
  def change
    change_column_default(:essays, :status, Essay.statuses[:draft])
    change_column_default(:users, :level, User.levels[:eleve])
  end
end
