require "rails_helper"

RSpec.describe AdminPagesController, type: :controller do
  describe "GET #essays" do
    subject { get(:essays) }

    context "the user is not signed in" do
      it "redirect to the login page" do
        expect(subject).to redirect_to(new_user_session_path)
      end

      it "displays an error message" do
        subject
        expect(flash[:alert]).to match("Vous devez vous connecter ou vous enregistrer pour continuer.")
      end
    end

    context "the user is signed in" do
      let(:user) { create(:user, level: user_level) }
      before { sign_in(user) }

      context "user is admin " do
        let(:user_level) { :admin }
        it "returns a ok http status" do
          is_expected.to have_http_status(:ok)
        end
      end

      context "user is not admin" do
        let(:user_level) { :eleve }
        it "redirects to root_path" do
          is_expected.to redirect_to(membres_accueil_path)
        end
      end
    end
  end
end
