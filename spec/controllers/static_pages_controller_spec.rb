# frozen_string_literal: true

require "rails_helper"

RSpec.describe StaticPagesController, type: :controller do
  describe "GET #home" do
    it "returns http success" do
      get :home
      expect(response).to have_http_status(:success)
    end

    it "renders home template" do
      get :home
      expect(response).to render_template(:home)
    end
  end

  describe "GET #about_whatwedo" do
    it "returns http success" do
      get :about_whatwedo
      expect(response).to have_http_status(:success)
    end

    it "renders about_whatwedo template" do
      get :about_whatwedo
      expect(response).to render_template(:about_whatwedo)
    end
  end

  describe "GET #about_berewarded" do
    it "returns http success" do
      get :about_berewarded
      expect(response).to have_http_status(:success)
    end

    it "renders about_berewarded template" do
      get :about_berewarded
      expect(response).to render_template(:about_berewarded)
    end
  end

  describe "GET #about_premiumaccess" do
    it "returns http success" do
      get :about_premiumaccess
      expect(response).to have_http_status(:success)
    end

    it "renders about_premiumaccess template" do
      get :about_premiumaccess
      expect(response).to render_template(:about_premiumaccess)
    end
  end

  describe "GET #about_legalstatement" do
    it "returns http success" do
      get :about_legalstatement
      expect(response).to have_http_status(:success)
    end

    it "renders about_legalstatement template" do
      get :about_legalstatement
      expect(response).to render_template(:about_legalstatement)
    end
  end
end
