# frozen_string_literal: true

require "rails_helper"

RSpec.describe EssayContents::InternalsController, type: :controller do
  describe "GET #new" do
    subject { get(:new, params: {topic_id: topic_id}) }
    let(:topic_id) { nil }

    context "the user is not signed in" do
      it "redirect to the login page" do
        expect(subject).to redirect_to(new_user_session_path)
      end

      it "displays an error message" do
        subject
        expect(flash[:alert]).to match("Vous devez vous connecter ou vous enregistrer pour continuer.")
      end
    end

    context "the user is signed in" do
      let(:user) { create(:user) }
      before { sign_in(user) }

      context "topic_id is given" do
        context "topic exists" do
          let(:topic) { create(:topic, :philosophie_dissertation) }
          let(:topic_id) { topic.id }

          it "assigns the given topic to @topic" do
            subject
            expect(assigns(:topic)).to eq(topic)
          end

          it "assigns a new EssayContents to @essay_content" do
            subject
            expect(assigns(:essay_content)).to be_a(EssayContents::Internal)
              .and be_new_record
          end

          it "associates the topic and essay_content" do
            subject
            expect(assigns(:essay_content).topic).to be(assigns(:topic))
          end
        end

        context "topic doesn't exist" do
          let(:topic_id) { -1 }

          it "assigns a new Topic to @topic" do
            subject
            expect(assigns(:topic)).to be_a(Topic)
              .and be_new_record
          end

          it "assigns a new EssayContents to @essay_content" do
            subject
            expect(assigns(:essay_content)).to be_a(EssayContents::Internal)
              .and be_new_record
          end

          it "associates the topic and essay_content" do
            subject
            expect(assigns(:essay_content).topic).to be(assigns(:topic))
          end
        end
      end

      context "topic_id is not given" do
        it "assigns a new topic to @topic" do
          subject
          expect(assigns(:topic)).to be_a(Topic)
            .and be_new_record
        end

        it "assigns a new EssayContents to @essay_content" do
          subject
          expect(assigns(:essay_content)).to be_a(EssayContents::Internal)
            .and be_new_record
        end

        it "associates the topic and essay_content" do
          subject
          expect(assigns(:essay_content).topic).to be(assigns(:topic))
        end
      end
    end
  end

  describe "GET #show" do
    subject { get(:show, params: {id: essay_content_id}) }

    context "the essay exists" do
      let!(:essay) { create(:essay, :internal, :philosophie_dissertation, status: status, essay_content: essay_content) }
      let(:essay_content) { create(:essay_contents_internal, user: author) }
      let(:author) { create(:user) }
      let(:essay_content_id) { essay_content.id }

      context "the essay is published" do
        let(:status) { :published }

        context "the user is not signed in" do
          it "increments the hits_nb by 1" do
            expect { subject }.to change { essay.reload.hits_nb }.by(1)
              .and change { essay.reload.hits_nb_last30d }.by(1)
          end

          it "returns a success status response" do
            subject
            expect(response).to have_http_status(:success)
          end
        end

        context "the user is signed in" do
          let(:user) { create(:user, level: user_level) }
          let(:user_level) { :eleve }

          before { sign_in(user) }

          context "the user is the author of the essay" do
            let(:author) { user }

            it "doesn't increment the hits_nb" do
              expect { subject }.not_to change { essay.reload.hits_nb }
              expect { subject }.not_to change { essay.reload.hits_nb_last30d }
            end

            it "returns a success status response" do
              subject
              expect(response).to have_http_status(:success)
            end
          end

          context "the user is an admin" do
            let(:user_level) { :admin }

            it "doesn't increment the hits_nb" do
              expect { subject }.not_to change { essay.reload.hits_nb }
              expect { subject }.not_to change { essay.reload.hits_nb_last30d }
            end

            it "returns a success status response" do
              subject
              expect(response).to have_http_status(:success)
            end
          end

          context "the user is neither the author of the essay, nor an admin" do
            it "increments the hits_nb by 1" do
              expect { subject }.to change { essay.reload.hits_nb }.by(1)
                .and change { essay.reload.hits_nb_last30d }.by(1)
            end

            it "doesn't update the updated_at field" do
              expect { subject }.not_to change { essay.reload.updated_at }
            end

            it "returns a success status response" do
              subject
              expect(response).to have_http_status(:success)
            end
          end
        end
      end

      context "the essay is not published" do
        let(:status) { :draft }
        it "doesn't increment the hits_nb" do
          expect { subject }.not_to change { essay.reload.hits_nb }
          expect { subject }.not_to change { essay.reload.hits_nb_last30d }
        end

        context "the user is signed in" do
          let(:user) { create(:user, level: user_level) }
          let(:user_level) { :eleve }

          before { sign_in(user) }

          context "the user is the author of the essay" do
            let(:author) { user }

            it "returns a ok status response" do
              subject
              expect(response).to have_http_status(:ok)
            end

            it "doesn't increment the number of hits" do
              expect { subject }.not_to change { essay.reload.hits_nb }
              expect { subject }.not_to change { essay.reload.hits_nb_last30d }
            end
          end
          context "the user is an admin" do
            let(:user_level) { :admin }

            it "returns a ok status response" do
              subject
              expect(response).to have_http_status(:ok)
            end

            it "doesn't increment the number of hits" do
              expect { subject }.not_to change { essay.reload.hits_nb }
              expect { subject }.not_to change { essay.reload.hits_nb_last30d }
            end
          end
          context "the user is neither the author, of the essay nor an admin" do
            it "redirects to the list of topics in the essay category" do
              subject
              expect(response).to redirect_to(category_topics_path(essay.topic.category))
            end

            it "doesn't increment the number of hits" do
              expect { subject }.not_to change { essay.reload.hits_nb }
              expect { subject }.not_to change { essay.reload.hits_nb_last30d }
            end
          end
        end

        context "the user is not signed in" do
          it "redirects to the list of topics in the essay category" do
            subject
            expect(response).to redirect_to(category_topics_path(essay.topic.category))
          end
        end
      end
    end

    context "the essay does not exist" do
      context "an external essay with this id exists" do
        let!(:essay_external) { create(:essay, :external, :philosophie_dissertation) }
        let(:essay_content_id) { essay_external.essay_content.id }

        it "redirects to the external essay" do
          subject
          expect(response).to redirect_to(essay_external.essay_content)
        end
      end

      context "no external essay with this id exists" do
        let(:essay_content_id) { -1 }

        it "redirects to the list of categories" do
          subject
          expect(response).to redirect_to(categories_path)
        end
      end
    end
  end

  describe "POST #create" do
    subject do
      post(
        :create,
        params: params
      )
    end

    let(:category) { create(:category, :philosophie_dissertation) }
    let(:params) { {essay: {content: ""}} }

    context "the user is not signed in" do
      it "redirect to the login page" do
        expect(subject).to redirect_to(new_user_session_path)
      end

      it "displays an error message" do
        subject
        expect(flash[:alert]).to match("Vous devez vous connecter ou vous enregistrer pour continuer.")
      end
    end

    context "the user is signed in" do
      let(:user) { create(:user, level: level) }
      before { sign_in(user) }

      context "the user is not an admin" do
        let(:level) { :eleve }

        context "the topic doesn't exist" do
          context "we don't submit a new author" do
            let(:topic_title) { "le titre" }
            let(:past_exam) { "Annale 2008" }
            let(:params) do
              {
                essay_contents_internal: {
                  content: "le contenu",
                  description: "la description",
                  topic_attributes: {
                    title: topic_title,
                    past_exam: past_exam,
                    category_id: category.id
                  }
                }
              }
            end
            it "creates a topic" do
              expect { subject }.to change(Topic, :count).by(1)
            end

            it "doesn't update the topic past_exam attribute" do
              subject

              topic = Topic.find_by(title: topic_title)
              expect(topic.past_exam).to eq(nil)
            end

            it "creates an essay" do
              expect { subject }.to change(EssayContents::Internal, :count).by(1)
                .and change(Essay, :count).by(1)
            end

            it "sets the newly created essay as a draft" do
              subject
              essay = Topic.find_by(title: "le titre").essays.last
              expect(essay.draft?).to be(true)
            end

            it "redirects to the view of the new created essay" do
              subject
              essay = Topic.find_by(title: "le titre").essays.last
              expect(response).to redirect_to(essay.essay_content)
            end
          end

          context "a new author is submitted" do
            let(:matiere) { "philosophie" }
            let(:type) { "commentaire" }
            let!(:category) { create(:category, :philosophie_commentaire) }
            let(:author_other) { "Nouvel auteur" }
            let(:params) do
              {
                matiere: matiere,
                type: type,
                author: {
                  other: author_other
                },
                essay_contents_internal: {
                  content: "le contenu",
                  description: "la description",
                  topic_attributes: {
                    studied_text: "Texte étudié",
                    category_id: category.id
                  }
                }
              }
            end

            it "creates a category" do
              expect { subject }.to change(Category, :count).by(1)
            end

            it 'assigns the categorie to "Non assigné" named group' do
              subject

              new_category = Category.find_by(name: author_other)
              expect(new_category.group.name).to eq(Group::NON_ASSIGNED)
            end

            it "gives the new category the right subject" do
              subject

              new_category = Category.find_by(name: author_other)
              expect(new_category.group.subject).to eq(matiere)
            end

            it "gives the new category the right exercice_type" do
              subject

              new_category = Category.find_by(name: author_other)
              expect(new_category.group.exercice_type).to eq(type)
            end

            it "creates a topic" do
              expect { subject }.to change(Topic, :count).by(1)
            end

            it "associates this category with the new topic" do
              subject
              topic = Topic.find_by(studied_text: "Texte étudié")
              category = Category.find_by(name: author_other)
              expect(topic.category).to eq(category)
            end

            it "creates an essay" do
              expect { subject }.to change(EssayContents::Internal, :count).by(1)
                .and change(Essay, :count).by(1)
            end

            it "sets the newly created essay as a draft" do
              subject
              essay = Topic.find_by(studied_text: "Texte étudié").essays.last
              expect(essay.draft?).to be(true)
            end

            it "redirects to the view of the new created essay" do
              subject
              essay = Topic.find_by(studied_text: "Texte étudié").essays.last
              expect(response).to redirect_to(essay.essay_content)
            end
          end
        end

        context "the topic exists" do
          context "content and description are given" do
            let(:topic) { create(:topic, :philosophie_dissertation, title: "Titre original") }
            let!(:existing_essay) { create(:essay, :internal, topic: topic) }
            let(:params) do
              {
                essay_contents_internal: {
                  content: "le contenu",
                  description: "la description",
                  topic_attributes: {
                    title: "nouveau titre",
                    id: topic.id
                  }
                }
              }
            end
            it "creates an essay" do
              expect { subject }.to change(EssayContents::Internal, :count).by(1)
                .and change(Essay, :count).by(1)
            end

            it "doesn't create a topic" do
              expect { subject }.not_to change(Topic, :count)
            end

            it "doesn't update the topic information" do
              expect { subject }.not_to change { topic.reload.title }
            end

            it "sets the newly created essay as a draft" do
              subject
              essay = topic.essays.last
              expect(essay.draft?).to be(true)
            end

            it "redirects to the view of the new created essay" do
              subject
              essay_content = topic.essays.last.essay_content
              expect(response).to redirect_to(essay_content)
            end
          end
        end
      end

      context "the user is an admin" do
        let(:level) { :admin }

        let(:topic_title) { "le titre" }
        let(:past_exam) { "Annale 2008" }
        let(:params) do
          {
            essay_contents_internal: {
              content: "le contenu",
              description: "la description",
              topic_attributes: {
                title: topic_title,
                past_exam: past_exam,
                category_id: category.id
              }
            }
          }
        end
        it "creates a topic" do
          expect { subject }.to change(Topic, :count).by(1)
        end

        it "updates the topic past_exam attribute" do
          subject

          topic = Topic.find_by(title: topic_title)
          expect(topic.past_exam).to eq(past_exam)
        end
      end
    end
  end

  describe "GET #edit" do
    subject { get(:edit, params: {id: id}) }
    let(:user) { create(:user, level: level) }
    let(:level) { :eleve }

    context "the essay doesn't exist" do
      let(:id) { -1 }

      context "the user is signed in" do
        before { sign_in(user) }

        it "redirect to error 404 page" do
          is_expected.to redirect_to("/404")
        end
      end

      context "the user is not signed in" do
        it "redirect to the login page" do
          expect(subject).to redirect_to(new_user_session_path)
        end
      end
    end

    context "the essay exists" do
      let!(:essay) { create(:essay, :internal, :philosophie_dissertation, essay_content: essay_content, status: status) }
      let(:status) { :draft }
      let(:essay_content) { create(:essay_contents_internal, user: author) }
      let(:author) { user }
      let(:id) { essay_content.id }

      context "the user is not signed in" do
        it "redirect to the login page" do
          expect(subject).to redirect_to(new_user_session_path)
        end

        it "displays an error message" do
          subject
          expect(flash[:alert]).to match("Vous devez vous connecter ou vous enregistrer pour continuer.")
        end
      end

      context "the user is signed in" do
        before { sign_in(user) }

        context "the user is an admin" do
          let(:level) { :admin }

          it "returns a ok http status" do
            is_expected.to have_http_status(:ok)
          end
        end

        context "the user is a professor" do
          let(:level) { :professeur }

          context "the user is not the author of the essay" do
            let(:author) { create(:user) }

            it "redirects to root_path" do
              is_expected.to redirect_to(root_path)
            end
          end

          context "the user is the author of the essay" do
            it "returns a ok http status" do
              is_expected.to have_http_status(:ok)
            end

            it "assigns the essay to @essay" do
              subject
              expect(assigns(:essay)).to eq(essay)
            end

            it "assigns the essay's topic to @topic" do
              subject
              expect(assigns(:topic)).to eq(essay.topic)
            end

            it "assigns the right subject, exercice type, groups" do
              subject
              group = essay.topic.category.group
              groups = Group.where(exercice_type: group.exercice_type.to_sym, subject: group.subject.to_sym)
              expect(assigns(:matiere)).to eq(group.subject.to_sym)
              expect(assigns(:type)).to eq(group.exercice_type.to_sym)
              expect(assigns(:groups)).to eq(groups)
            end
          end
        end

        context "the user is a student" do
          let(:level) { :eleve }

          context "the user is not the author of the essay" do
            let(:author) { create(:user) }

            it "redirects to root_path" do
              is_expected.to redirect_to(root_path)
            end
          end

          context "the user is the author of the essay" do
            context "the essay is draft" do
              it "returns a ok http status" do
                is_expected.to have_http_status(:ok)
              end

              it "assigns the essay to @essay" do
                subject
                expect(assigns(:essay)).to eq(essay)
              end

              it "assigns the essay's topic to @topic" do
                subject
                expect(assigns(:topic)).to eq(essay.topic)
              end

              it "assigns the right subject, exercice type, groups" do
                subject
                group = essay.topic.category.group
                groups = Group.where(exercice_type: group.exercice_type.to_sym, subject: group.subject.to_sym)
                expect(assigns(:matiere)).to eq(group.subject.to_sym)
                expect(assigns(:type)).to eq(group.exercice_type.to_sym)
                expect(assigns(:groups)).to eq(groups)
              end
            end

            context "the essay is pending_validation" do
              let(:status) { :validation_pending }

              it "sets an appropriate alert message" do
                subject
                expect(flash[:alert]).to eq("Afin d'éviter les abus, vous ne pouvez pas effectuer cette action. Merci de nous contacter.")
              end

              it "redirects to the essay view" do
                expect(subject).to redirect_to(essay_contents_internal_path(essay_content))
              end
            end

            context "the essay is published" do
              let(:status) { :published }

              it "sets an appropriate alert message" do
                subject
                expect(flash[:alert]).to eq("Afin d'éviter les abus, vous ne pouvez pas effectuer cette action. Merci de nous contacter.")
              end

              it "redirects to the essay view" do
                expect(subject).to redirect_to(essay_contents_internal_path(essay_content))
              end
            end
          end
        end
      end
    end
  end

  describe "PUT #update" do
    let(:category) { create(:category, :philosophie_commentaire) }
    let(:new_category) { create(:category, :philosophie_commentaire) }
    let(:topic) { create(:topic, :philosophie_commentaire, category: category) }
    let(:user) { create(:user, level: level) }
    let(:level) { :eleve }
    let!(:essay) { create(:essay, :internal, essay_content: essay_content, topic: topic, status: status) }
    let(:essay_content) { create(:essay_contents_internal, user: author) }
    let(:author) { user }
    let(:status) { :draft }
    let(:author_other) { "" }

    subject { put(:update, params: params) }

    let(:params) do
      {
        id: essay_content.id,
        essay_contents_internal: {
          content: "le nouveau contenu",
          description: "la nouvelle description",
          keywords: "nouveaux-mots-cles",
          topic_attributes: {
            id: topic.id,
            category_id: new_category.id
          }
        },
        author: {
          other: author_other
        }
      }
    end

    context "the user is not signed in" do
      it "redirect to the login page" do
        expect(subject).to redirect_to(new_user_session_path)
      end
    end

    context "the user is signed in" do
      before { sign_in(user) }

      context "the user is an admin" do
        let(:level) { :admin }

        it "returns a found http status" do
          is_expected.to have_http_status(:found)
        end

        context "the essay is published" do
          let(:status) { :published }

          it "updates the essay content keywords" do
            expect { subject }.to change { essay_content.reload.keywords }.from(nil).to("nouveaux-mots-cles")
          end
        end
      end

      context "the user is a professor" do
        let(:level) { :professeur }

        context "the user is not the author of the essay" do
          let(:author) { create(:user) }

          it "redirects to root_path" do
            is_expected.to redirect_to(root_path)
          end
        end

        context "the user is the author of the essay" do
          it "returns a ok http status" do
            is_expected.to have_http_status(:found)
          end

          context "the essay is not published" do
            context "this is the first essay for this topic" do
              it "updates the topic info" do
                expect { subject }.to change { topic.reload.title }.to(new_category.name)
                  .and change { topic.reload.category }.to(new_category)
              end

              it "updates the essay content info" do
                expect { subject }.to change { essay_content.reload.content }.to("le nouveau contenu")
                  .and change { essay_content.reload.description }.to("la nouvelle description")
              end

              it "doesn't update the essay content keywords" do
                expect { subject }.not_to change { essay_content.reload.keywords }
              end

              context "an other author is submitted" do
                let(:author_other) { "nouvel auteur" }

                it "takes precedence over the new (and old categories)" do
                  subject

                  unassigned_category = Category.find_by(name: author_other)
                  expect(topic.reload.category).to eq(unassigned_category)
                  expect(topic.reload.title).to eq(author_other)
                end
              end
            end

            context "this is not the first essay for this topic" do
              # forcing the id here as let! creates the essay even before this before hook (as placed above)
              before { create(:essay, :internal, :philosophie_dissertation, topic: topic, id: essay.id - 1) }

              it "doesn't update the topic info" do
                expect { subject }.not_to change { topic.reload.title }
                expect { subject }.not_to change { topic.reload.category }
              end

              it "updates the essay content info" do
                expect { subject }.to change { essay_content.reload.content }.to("le nouveau contenu")
                  .and change { essay_content.reload.description }.to("la nouvelle description")
              end
            end
          end

          context "the essay is published" do
            let(:status) { :published }

            it "doesn't update the topic info" do
              expect { subject }.not_to change { topic.reload.title }
              expect { subject }.not_to change { topic.reload.category }
            end

            it "updates the essay content info" do
              expect { subject }.to change { essay_content.reload.content }.to("le nouveau contenu")
                .and change { essay_content.reload.description }.to("la nouvelle description")
            end

            it "doesn't update the essay content keywords" do
              expect { subject }.not_to change { essay_content.reload.keywords }
            end
          end
        end
      end

      context "the user is a student" do
        let(:level) { :eleve }

        context "the user is not the author of the essay" do
          let(:author) { create(:user) }

          it "redirects to root_path" do
            is_expected.to redirect_to(root_path)
          end
        end

        context "the user is the author of the essay" do
          it "returns a ok http status" do
            is_expected.to have_http_status(:found)
          end

          context "the essay is not published" do
            it "updates the topic info" do
              expect { subject }.to change { topic.reload.title }.to(new_category.name)
                .and change { topic.reload.category }.to(new_category)
            end

            it "updates the essay content info" do
              expect { subject }.to change { essay_content.reload.content }.to("le nouveau contenu")
                .and change { essay_content.reload.description }.to("la nouvelle description")
            end

            it "doesn't update the essay content keywords" do
              expect { subject }.not_to change { essay_content.reload.keywords }
            end
          end

          context "the essay is published" do
            let(:status) { :published }

            it "doesn't update the topic info" do
              expect { subject }.not_to change { topic.reload.title }
              expect { subject }.not_to change { topic.reload.category }
            end

            it "doesn't update the essay info" do
              expect { subject }.not_to change { essay_content.reload.content }
              expect { subject }.not_to change { essay_content.reload.description }
            end

            it "doesn't update the essay content keywords" do
              expect { subject }.not_to change { essay_content.reload.keywords }
            end

            it "set an alert message" do
              subject
              expect(flash[:alert]).to be_present
            end

            it "redirects to the essay view" do
              expect(subject).to redirect_to(essay_contents_internal_path(essay_content))
            end
          end
        end
      end
    end
  end

  describe "DELETE #destroy" do
    let(:user) { create(:user, level: level) }
    let(:level) { :eleve }
    subject { delete(:destroy, params: {id: id}) }

    context "the id doesn't exist" do
      let(:id) { -1 }

      context "the user is signed in" do
        before { sign_in(user) }

        it "redirects to 404 error page" do
          is_expected.to redirect_to("/404")
        end
      end

      context "the user is not signed in" do
        it "redirect to the login page" do
          expect(subject).to redirect_to(new_user_session_path)
        end
      end
    end

    context "the id exists" do
      let!(:essay) { create(:essay, :internal, :philosophie_dissertation, essay_content: essay_content, status: status) }
      let(:essay_content) { create(:essay_contents_internal, user: author) }
      let(:status) { :published }
      let(:author) { user }
      let(:id) { essay_content.id }
      let(:essay_id) { essay.id }

      context "the user is not signed in" do
        it "redirect to the login page" do
          expect(subject).to redirect_to(new_user_session_path)
        end
      end

      context "the user is signed in" do
        before { sign_in(user) }

        context "the user is an admin" do
          let(:level) { :admin }

          it "returns a found http status" do
            is_expected.to have_http_status(:found)
          end

          it "deletes the essay" do
            subject
            expect(EssayContents::Internal.find_by(id: id)).to be_nil
            expect(Essay.find_by(id: essay_id)).to be_nil
          end
        end

        context "the user is a professor" do
          let(:level) { :professeur }
          context "the user is not the author of the essay" do
            let(:author) { create(:user) }

            it "redirects to root_path" do
              is_expected.to redirect_to(root_path)
            end

            it "doesn't delete the essay" do
              subject
              expect(EssayContents::Internal.find_by(id: id)).not_to be_nil
              expect(Essay.find_by(id: essay_id)).not_to be_nil
            end
          end

          context "the user is the author of the essay" do
            it "returns a found http status" do
              is_expected.to have_http_status(:found)
            end

            context "the topic doesn't have other essays" do
              it "deletes the essay" do
                subject
                expect(EssayContents::Internal.find_by(id: id)).to be_nil
                expect(Essay.find_by(id: essay_id)).to be_nil
              end

              it "deletes the topic" do
                subject
                expect(Topic.find_by(id: essay.topic.id)).to be_nil
              end
            end

            context "the topic has other essays" do
              let!(:other_essay) { create(:essay, :internal, :philosophie_dissertation, topic: essay.topic) }
              it "deletes the essay" do
                subject
                expect(EssayContents::Internal.find_by(id: id)).to be_nil
              end

              it "doesn't delete the topic" do
                subject
                expect(Topic.find_by(id: essay.topic.id)).not_to be_nil
              end

              it "doesn't delete the other essay" do
                subject
                expect(EssayContents::Internal.find_by(id: other_essay.essay_content.id)).not_to be_nil
                expect(Essay.find_by(id: other_essay.id)).not_to be_nil
              end
            end
          end
        end

        context "the user is a student" do
          let(:level) { :eleve }

          context "the user is not the author of the essay" do
            let(:author) { create(:user) }

            it "redirects to root_path" do
              is_expected.to redirect_to(root_path)
            end

            it "doesn't delete the essay" do
              subject
              expect(EssayContents::Internal.find_by(id: id)).not_to be_nil
              expect(Essay.find_by(id: essay_id)).not_to be_nil
            end
          end

          context "the user is the author of the essay" do
            it "returns a found http status" do
              is_expected.to have_http_status(:found)
            end

            context "the essay isn't published" do
              let(:status) { :draft }

              it "deletes the essay" do
                subject
                expect(EssayContents::Internal.find_by(id: id)).to be_nil
                expect(Essay.find_by(id: essay_id)).to be_nil
              end

              it "deletes the topic" do
                subject
                expect(Topic.find_by(id: essay.topic.id)).to be_nil
              end
            end

            context "the essay is published" do
              let(:status) { :published }

              it "doesn't delete the essay" do
                subject
                expect(EssayContents::Internal.find_by(id: id)).not_to be_nil
                expect(Essay.find_by(id: essay_id)).not_to be_nil
              end

              it "doesn't delete the topic" do
                subject
                expect(Topic.find_by(id: essay.topic.id)).not_to be_nil
              end

              it "set an alert message" do
                subject
                expect(flash[:alert]).to be_present
              end

              it "redirects to the essay view" do
                expect(subject).to redirect_to(essay_contents_internal_path(essay_content))
              end
            end
          end
        end
      end
    end
  end

  describe "PUT #publish" do
    let(:category) { create(:category, :philosophie_dissertation) }
    let(:topic) { create(:topic, :philosophie_dissertation, category: category) }
    let(:user) { create(:user, level: level) }
    let(:level) { :eleve }
    let!(:essay) { create(:essay, :internal, :philosophie_dissertation, essay_content: essay_content, topic: topic, status: status) }
    let(:essay_content) { create(:essay_contents_internal, user: author) }
    let(:id) { essay_content.id }
    let(:author) { user }
    let(:status) { :draft }

    subject { put(:publish, params: {id: id}) }

    context "the user is not signed in" do
      it "redirect to the login page" do
        expect(subject).to redirect_to(new_user_session_path)
      end
    end

    context "the user is signed in" do
      before { sign_in(user) }

      context "the user is an admin" do
        let(:level) { :admin }

        it "returns a found http status" do
          is_expected.to have_http_status(:found)
        end

        context "ESSAY_VALIDATION_DISABLED has been set to true" do
          before do
            allow(ENV).to receive(:[]).with("ESSAY_VALIDATION_DISABLED").and_return("true")
          end

          it "updates (regardless) the status to validation_pending" do
            expect { subject }.to change { essay.reload.status }.to("validation_pending")
          end
        end
      end

      context "the user is not an admin" do
        context "the user is not the author of the essay" do
          let(:author) { create(:user) }

          it "redirects to root_path" do
            is_expected.to redirect_to(root_path)
          end
        end

        context "the user is the author of the essay" do
          it "returns a ok http status" do
            is_expected.to have_http_status(:found)
          end

          context "the essay is in draft status" do
            context "ESSAY_VALIDATION_DISABLED has been set to true" do
              before do
                allow(ENV).to receive(:[]).with("ESSAY_VALIDATION_DISABLED").and_return("true")
              end

              it "does NOT update the status to validation_pending" do
                expect { subject }.not_to change { essay.reload.status }
              end

              it "generates an appropriate error flash message" do
                subject
                expect(flash[:alert]).to match(/Il n'est pas possible de soumettre à validation de nouveaux corrigés pour le moment/)
              end

              it "is redirected to the essay content" do
                subject
                expect(response).to redirect_to(essay_content)
              end
            end

            context "ESSAY_VALIDATION_DISABLED has NOT been set to true" do
              it "updates the status to validation_pending" do
                expect { subject }.to change { essay.reload.status }.to("validation_pending")
              end

              it "generates a success flash message" do
                subject
                expect(flash[:success]).to match(/Votre corrigé est désormais en attente de validation/)
              end

              it "is redirected to the essay content" do
                subject
                expect(response).to redirect_to(essay_content)
              end
            end
          end
        end

        context "the essay is pending validation" do
          let(:status) { :validation_pending }

          it "doesn't change the status of the essay" do
            expect { subject }.not_to change { essay.reload.status }
          end

          it "is redirected to the essay content" do
            subject
            expect(response).to redirect_to(essay_content)
          end
        end

        context "the essay is published" do
          let(:status) { :published }

          context "the user isn't an student" do
            let(:level) { :professeur }

            it "updates the status to draft" do
              expect { subject }.to change { essay.reload.status }.to("draft")
            end

            it "generates a success flash message" do
              subject
              expect(flash[:success]).to match(/Votre corrigé est désormais un brouillon/)
            end

            it "is redirected to the essay content" do
              subject
              expect(response).to redirect_to(essay_content)
            end
          end

          context "the user is an student" do
            let(:level) { :eleve }

            it "updates the status to draft" do
              expect { subject }.not_to change { essay.reload.status }
            end

            it "generates an alert flash message" do
              subject
              expect(flash[:alert]).to be_present
            end

            it "is redirected to the essay content" do
              subject
              expect(response).to redirect_to(essay_content)
            end
          end
        end
      end
    end
  end

  describe "PUT #validate" do
    let(:user) { create(:user, level: level) }
    let(:level) { :eleve }
    let!(:essay) { create(:essay, :internal, topic: topic, essay_content: essay_content, status: status, validated_at: validated_at) }
    let(:validated_at) { nil }
    let(:topic) { create(:topic, :philosophie_dissertation, title: "le titre") }
    let(:essay_content) { create(:essay_contents_internal, user: author) }
    let(:id) { essay_content.id }
    let(:author) { create(:user) }
    let(:status) { :draft }
    let(:validated) { nil }

    subject { put(:validate, params: {id: id, validated: validated}) }

    context "the user is not signed in" do
      it "redirect to the login page" do
        expect(subject).to redirect_to(new_user_session_path)
      end
    end

    context "the user is signed in" do
      before { sign_in(user) }

      context "the user is not an admin" do
        it "redirects to the user home page" do
          is_expected.to redirect_to(membres_accueil_path)
        end
      end

      context "the user is an admin" do
        let(:level) { :admin }

        it "returns a found http status" do
          is_expected.to have_http_status(:found)
        end

        context "the essay is in draft" do
          let(:status) { :draft }

          it "doesn't update the status" do
            expect { subject }.not_to change { essay.reload.status }
          end
        end

        context "the essay is pending validation" do
          let(:status) { :validation_pending }

          context "the essay is validated" do
            let(:validated) { "true" }

            it "assigns to essay_content keywords from topic" do
              expect { subject }.to change { essay.reload.essay_content.keywords }.from(nil).to("titre")
            end

            it "changes the status of the essay to published" do
              expect { subject }.to change { essay.reload.status }.to("published")
            end

            it "rewards the user upon validation" do
              expect { subject }.to change { author.premium_accesses.count }.from(0).to(1)
            end

            it "gives a validation date to the essay" do
              expect { subject }.to change { essay.reload.validated_at }.from(validated_at).to(an_instance_of(ActiveSupport::TimeWithZone))
            end

            it "sends an email to the author" do
              expect(UserMailer).to receive(:essay_validated).and_call_original
              subject
            end

            it "is redirected to the essay content" do
              subject
              expect(response).to redirect_to(essay_content.reload)
            end

            context "the essay has already been validated in the past" do
              let(:validated_at) { 3.days.ago }

              it "doesn't update the validation date again" do
                expect { subject }.not_to change { essay.validated_at }
              end

              it "gives no additional premium accesses to the user" do
                expect { subject }.not_to change { user.premium_accesses.count }
              end
            end
          end

          context "the essay is not validated" do
            it "doesn't assign to essay_content keywords from topic" do
              expect { subject }.not_to change { essay.reload.essay_content.keywords }
            end

            it "changes the status of the essay to draft" do
              expect { subject }.to change { essay.reload.status }.to("draft")
            end

            it "sends an email to the author" do
              expect(UserMailer).to receive(:essay_rejected).and_call_original
              subject
            end

            it "is redirected to the essay content" do
              subject
              expect(response).to redirect_to(essay_content)
            end
          end
        end
      end
    end
  end

  describe "PUT #premiumize" do
    let(:user) { create(:user, level: level) }
    let(:level) { :eleve }
    let!(:essay) { create(:essay, :philosophie_dissertation, essay_content: essay_content) }
    let(:essay_content) { create(:essay_contents_internal, premium: premium) }
    let(:premium) { false }
    let(:id) { essay_content.id }

    subject { put(:premiumize, params: {id: id}) }

    context "the user is not signed in" do
      it "redirect to the login page" do
        expect(subject).to redirect_to(new_user_session_path)
      end
    end

    context "the user is signed in" do
      before { sign_in(user) }

      context "the user is not an admin" do
        it "redirects to the user home page" do
          is_expected.to redirect_to(membres_accueil_path)
        end
      end

      context "the user is an admin" do
        let(:level) { :admin }

        it "returns a found http status" do
          is_expected.to have_http_status(:found)
        end

        context "the essay_content is not premium" do
          it "makes the essay_content premium" do
            expect { subject }.to change { essay_content.reload.premium }.from(false).to(true)
          end

          it "is redirected to the essay content" do
            subject
            expect(response).to redirect_to(essay_content)
          end
        end

        context "the essay_content is premium" do
          let(:premium) { true }
          it "makes the essay_content not premium" do
            expect { subject }.to change { essay_content.reload.premium }.from(true).to(false)
          end

          it "is redirected to the essay content" do
            subject
            expect(response).to redirect_to(essay_content)
          end
        end
      end
    end
  end

  describe "PUT #update_topic" do
    let(:user) { create(:user, level: level) }
    let(:level) { :eleve }
    let(:essay) { create(:essay, :internal, :philosophie_dissertation) }
    let(:essay_content) { essay.essay_content }
    let(:id) { essay_content.id }
    let(:old_topic) { essay.topic }
    let(:new_topic) { create(:topic, :francais) }
    let(:new_topic_id) { new_topic.id }

    subject { put(:update_topic, params: {id: id, new_topic_id: new_topic_id}) }

    context "the user is not signed in" do
      it "redirect to the login page" do
        expect(subject).to redirect_to(new_user_session_path)
      end
    end

    context "the user is signed in" do
      before { sign_in(user) }

      context "the user is not an admin" do
        it "redirects to the user home page" do
          is_expected.to redirect_to(membres_accueil_path)
        end
      end

      context "the user is an admin" do
        let(:level) { :admin }

        it "returns a found http status" do
          is_expected.to have_http_status(:found)
        end

        context "given topic id does NOT exist" do
          let(:new_topic_id) { -1 }

          it "does NOT update the topic of the essay" do
            expect { subject }.not_to change { essay_content.reload.topic }
          end

          it "does NOT destroy the old topic" do
            expect { subject }.not_to change { Topic.find_by(id: old_topic.id) }
          end

          it "sets the appropriate flash message" do
            subject
            expect(flash[:alert]).to match("L'id du topic indiqué n'est pas valide")
          end
        end

        context "given topic id exists" do
          context "the old topic has NO other essay associated to it" do
            it "updates the topic of the essay" do
              expect { subject }.to change { essay_content.reload.topic }.from(old_topic).to(new_topic)
            end

            it "destroys the old topic" do
              expect { subject }.to change { Topic.find_by(id: old_topic.id) }.from(old_topic).to(nil)
            end

            it "is redirected to the essay content" do
              subject
              expect(response).to redirect_to(essay_content)
            end

            it "sets the appropriate flash message" do
              subject
              expect(flash[:success]).to match("Le corrigé a désormais un nouveau topic")
            end
          end

          context "the old topic has an other essay associated to it" do
            let!(:other_topic_essay) { create(:essay, :external, topic: old_topic) }

            it "updates the topic of the essay" do
              expect { subject }.to change { essay_content.reload.topic }.from(old_topic).to(new_topic)
            end

            it "does NOT destroy the old topic" do
              expect { subject }.not_to change { Topic.find_by(id: old_topic.id) }
            end

            it "is redirected to the essay content" do
              subject
              expect(response).to redirect_to(essay_content)
            end

            it "sets the appropriate flash message" do
              subject
              expect(flash[:success]).to match("Le corrigé a désormais un nouveau topic")
            end
          end
        end
      end
    end
  end
end
