# frozen_string_literal: true

require "rails_helper"

RSpec.describe EssayContents::ExternalsController, type: :controller do
  describe "GET #show" do
    subject { get(:show, params: {id: essay_content_id}) }

    context "the external essay exists" do
      let(:essay_external) { create(:essay, :external, :philosophie_dissertation, status: status) }
      let(:essay_content_id) { essay_external.essay_content.id }

      context "the external essay is published" do
        let(:status) { :published }

        context "the user is not signed in" do
          it "increments the hits_nb by 1" do
            expect { subject }.to change { essay_external.reload.hits_nb }.by(1)
              .and change { essay_external.reload.hits_nb_last30d }.by(1)
          end

          it "returns a success status response" do
            subject
            expect(response).to have_http_status(:success)
          end
        end

        context "the user is signed in" do
          let(:user) { create(:user, level: user_level) }
          let(:user_level) { :eleve }

          before { sign_in(user) }

          context "the user is an admin" do
            let(:user_level) { :admin }

            it "doesn't increment the hits_nb" do
              expect { subject }.not_to change { essay_external.reload.hits_nb }
              expect { subject }.not_to change { essay_external.reload.hits_nb_last30d }
            end

            it "returns a success status response" do
              subject
              expect(response).to have_http_status(:success)
            end
          end

          context "the user is not an admin" do
            it "increments the hits_nb by 1" do
              expect { subject }.to change { essay_external.reload.hits_nb }.by(1)
                .and change { essay_external.reload.hits_nb_last30d }.by(1)
            end

            it "doesn't update the updated_at field" do
              expect { subject }.not_to change { essay_external.reload.updated_at }
            end

            it "returns a success status response" do
              subject
              expect(response).to have_http_status(:success)
            end
          end
        end
      end

      context "the external essay is in draft" do
        let(:status) { :draft }
        it "doesn't increment the hits_nb" do
          expect { subject }.not_to change { essay_external.reload.hits_nb }
          expect { subject }.not_to change { essay_external.reload.hits_nb_last30d }
        end

        context "the user is signed in" do
          let(:user) { create(:user, level: user_level) }
          let(:user_level) { :eleve }

          before { sign_in(user) }

          context "the user is an admin" do
            let(:user_level) { :admin }

            it "returns a ok status response" do
              subject
              expect(response).to have_http_status(:ok)
            end

            it "doesn't increment the number of hits" do
              expect { subject }.not_to change { essay_external.reload.hits_nb }
              expect { subject }.not_to change { essay_external.reload.hits_nb_last30d }
            end
          end
          context "the user is not an admin" do
            it "redirects to the list of topics in the essay category" do
              subject
              expect(response).to redirect_to(category_topics_path(essay_external.topic.category))
            end

            it "doesn't increment the number of hits" do
              expect { subject }.not_to change { essay_external.reload.hits_nb }
              expect { subject }.not_to change { essay_external.reload.hits_nb_last30d }
            end
          end
        end

        context "the user is not signed in" do
          it "redirects to the list of topics in the essay category" do
            subject
            expect(response).to redirect_to(category_topics_path(essay_external.topic.category))
          end
        end
      end

      context "the external essay is pending validation" do
        let(:status) { :validation_pending }
        it "doesn't increment the hits_nb" do
          expect { subject }.not_to change { essay_external.reload.hits_nb }
          expect { subject }.not_to change { essay_external.reload.hits_nb_last30d }
        end

        context "the user is signed in" do
          let(:user) { create(:user, level: user_level) }
          let(:user_level) { :eleve }

          before { sign_in(user) }

          context "the user is an admin" do
            let(:user_level) { :admin }

            it "returns a ok status response" do
              subject
              expect(response).to have_http_status(:ok)
            end

            it "doesn't increment the number of hits" do
              expect { subject }.not_to change { essay_external.reload.hits_nb }
              expect { subject }.not_to change { essay_external.reload.hits_nb_last30d }
            end
          end
          context "the user is not an admin" do
            it "redirects to the list of topics in the essay category" do
              subject
              expect(response).to redirect_to(category_topics_path(essay_external.topic.category))
            end

            it "doesn't increment the number of hits" do
              expect { subject }.not_to change { essay_external.reload.hits_nb }
              expect { subject }.not_to change { essay_external.reload.hits_nb_last30d }
            end
          end
        end

        context "the user is not signed in" do
          it "redirects to the list of topics in the essay category" do
            subject
            expect(response).to redirect_to(category_topics_path(essay_external.topic.category))
          end
        end
      end
    end

    context "the external essay does not exist" do
      let(:essay_content_id) { -1 }

      it "redirects to the not found page" do
        subject
        expect(response).to redirect_to(not_found_path)
      end
    end
  end

  describe "GET #archive_html" do
    subject { get(:archive_html, params: {id: essay_content_id}) }

    let(:essay_content) { create(:essay_contents_external, url_unavailable: url_unavailable) }
    let(:url_unavailable) { false }
    let!(:essay) { create(:essay, :philosophie_dissertation, essay_content: essay_content) }
    let(:essay_content_id) { essay_content.id }
    let(:user) { create(:user, level: user_level) }
    let(:user_level) { :eleve }

    before { sign_in(user) }

    context "the user is admin" do
      let(:user_level) { :admin }

      it "doesn't update the archives_access_count" do
        expect { subject }.not_to change { essay_content.reload.archives_access_count }
      end
    end

    context "the user is not admin" do
      context "the url is available" do
        it "updates the archives_access_count" do
          expect { subject }.to change { essay_content.reload.archives_access_count }.from(0).to(1)
        end
      end

      context "the url is not available" do
        let(:url_unavailable) { true }

        it "doesn't update the archives_access_count" do
          expect { subject }.not_to change { essay_content.reload.archives_access_count }
        end
      end
    end
  end

  describe "GET #archive_pdf" do
    subject { get(:archive_pdf, params: {id: essay_content_id}) }

    let(:essay_content) { create(:essay_contents_external, url_unavailable: url_unavailable) }
    let(:url_unavailable) { false }
    let!(:essay) { create(:essay, :philosophie_dissertation, essay_content: essay_content) }
    let(:essay_content_id) { essay_content.id }
    let(:user) { create(:user, level: user_level) }

    context "the user is not signed in" do
      it "doesn't update the archives_access_count" do
        expect { subject }.not_to change { essay_content.reload.archives_access_count }
      end

      it "redirects to the login page" do
        expect(subject).to redirect_to(new_user_session_path)
      end
    end

    context "the user is signed in" do
      before(:each) { sign_in(user) }

      context "the user is admin" do
        let(:user_level) { :admin }

        it "doesn't update the archives_access_count" do
          expect { subject }.not_to change { essay_content.reload.archives_access_count }
        end

        it "redirects to the pdf archive" do
          expect(subject).to redirect_to(essay_content.pdf)
        end
      end

      context "the user is not admin" do
        context "the user is a professeur" do
          let(:user_level) { :professeur }
          it "updates the archives_access_count" do
            expect { subject }.to change { essay_content.reload.archives_access_count }.from(0).to(1)
          end

          it "redirects to the pdf archive" do
            expect(subject).to redirect_to(essay_content.pdf)
          end
        end

        context "the user is an eleve" do
          let(:user_level) { :eleve }

          context "the user doesn't have a premium access" do
            it "redirects to the membres_premiumabonnement_path" do
              expect(subject).to redirect_to(membres_premiumabonnement_path)
            end

            it "doesn't update the archives_access_count" do
              expect { subject }.not_to change { essay_content.reload.archives_access_count }
            end
          end

          context "the user has a premium access" do
            let!(:premium_access) { create(:premium_access, user: user) }

            it "redirects to the pdf archive" do
              expect(subject).to redirect_to(essay_content.pdf)
            end

            context "the url is available" do
              it "updates the archives_access_count" do
                expect { subject }.to change { essay_content.reload.archives_access_count }.from(0).to(1)
              end
            end

            context "the url is not available" do
              let(:url_unavailable) { true }

              it "doesn't update the archives_access_count" do
                expect { subject }.not_to change { essay_content.reload.archives_access_count }
              end
            end
          end
        end
      end
    end
  end

  describe "PUT #archive" do
    subject { put(:archive, params: {id: id}) }
    let(:user) { create(:user, level: level) }
    let(:level) { :eleve }
    let(:id) { -1 }

    context "the user is not signed in" do
      it "redirects to the login page" do
        is_expected.to redirect_to(new_user_session_path)
      end
    end

    context "the user is not admin" do
      before { sign_in(user) }

      it "redirects to the user home page" do
        expect(subject).to redirect_to(membres_accueil_path)
      end
    end

    context "the user is signed in as admin" do
      let(:level) { :admin }
      before { sign_in(user) }

      context "the id doesn't exist" do
        it "redirects to the not found page" do
          is_expected.to redirect_to(not_found_path)
        end
      end

      context "the id exists" do
        let(:archives_access_count) { 5 }
        let(:essay_content) { create(:essay_contents_external, archives_access_count: archives_access_count) }
        let!(:essay) { create(:essay, :philosophie_dissertation, essay_content: essay_content, status: status) }
        let(:id) { essay_content.id }
        let(:status) { :draft }

        it "updates url_unavailable to true" do
          expect { subject }.to change { essay_content.reload.url_unavailable }.from(false).to(true)
        end

        it "updates the archives_access_count to 0" do
          expect { subject }.to change { essay_content.reload.archives_access_count }.from(archives_access_count).to(0)
        end

        it "redirects to the essay content" do
          expect(subject).to redirect_to(essay_content)
        end
      end
    end
  end

  describe "DELETE #destroy" do
    subject { delete(:destroy, params: {id: id}) }
    let(:user) { create(:user, level: level) }
    let(:level) { :eleve }
    let(:id) { -1 }

    context "the user is not signed in" do
      it "redirects to the login page" do
        is_expected.to redirect_to(new_user_session_path)
      end
    end

    context "the user is not admin" do
      before { sign_in(user) }

      it "redirects to the user home page" do
        expect(subject).to redirect_to(membres_accueil_path)
      end
    end

    context "the user is signed in as admin" do
      let(:level) { :admin }
      before { sign_in(user) }

      context "the id doesn't exist" do
        it "redirects to the not found page" do
          is_expected.to redirect_to(not_found_path)
        end
      end

      context "the id exists" do
        let!(:essay) { create(:essay, :external, :philosophie_dissertation, status: status) }
        let(:id) { essay.essay_content.id }
        let(:status) { :draft }

        context "the topic doesn't have other essays" do
          it "deletes the essay" do
            subject
            expect(EssayContents::External.find_by(id: id)).to be_nil
            expect(Essay.find_by(id: essay.id)).to be_nil
          end

          it "deletes the topic" do
            subject
            expect(Topic.find_by(id: essay.topic.id)).to be_nil
          end
        end

        context "the topic has other essays" do
          let!(:other_essay) { create(:essay, :internal, :philosophie_dissertation, topic: essay.topic) }
          it "deletes the essay" do
            subject
            expect(EssayContents::Internal.find_by(id: id)).to be_nil
          end

          it "doesn't delete the topic" do
            subject
            expect(Topic.find_by(id: essay.topic.id)).not_to be_nil
          end

          it "doesn't delete the other essay" do
            subject
            expect(EssayContents::Internal.find_by(id: other_essay.essay_content.id)).not_to be_nil
            expect(Essay.find_by(id: other_essay.id)).not_to be_nil
          end
        end
      end
    end
  end
end
