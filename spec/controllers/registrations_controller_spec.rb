# frozen_string_literal: true

require "rails_helper"

RSpec.describe RegistrationsController, type: :controller do
  describe "PUT #create" do
    before(:each) do
      # we need to indicate devise which mapping to use as tests are not going through the router
      @request.env["devise.mapping"] = Devise.mappings[:user]
    end

    subject { put(:create, params: params) }

    context "params correct" do
      let(:username) { "username" }
      let(:email) { "test@test.com" }
      let(:password) { "password" }
      let(:password_confirmation) { "password" }
      let(:params) do
        {
          user: {
            username: username,
            email: email,
            password: password,
            password_confirmation: password
          }
        }
      end

      it "creates a user with given params" do
        expect { subject }
          .to change { User.find_by(username: username, email: email) }
          .from(nil)
          .to(an_instance_of(User))
      end
    end
  end

  describe "PATCH #update" do
    let(:user) { create(:user, level: :eleve) }
    subject { patch(:update, params: params) }

    before(:each) do
      # we need to indicate devise which mapping to use as tests are not going through the router
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in(user)
    end

    context "new username provided" do
      let(:new_username) { "new-username" }
      let(:params) do
        {user: {username: new_username, current_password: user.password}}
      end

      it "updates the username" do
        expect { subject }
          .to change { user.reload.username }
          .to(new_username)
      end
    end
  end

  describe "DELETE #destroy" do
    let(:user) { create(:user, level: level) }
    let(:user_id) { user.id }
    let!(:essay) { create(:essay, :philosophie_dissertation, essay_content: essay_content, status: status) }
    let(:essay_content) { create(:essay_contents_internal, user: user) }
    let(:essay_id) { essay.id }
    let(:essay_content_id) { essay_content.id }
    subject { delete(:destroy) }

    before(:each) do
      # we need to indicate devise which mapping to use as tests are not going through the router
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in(user)
    end

    context "the user is a professor" do
      let(:level) { :professeur }

      context "the user has no published essays" do
        let(:status) { :draft }

        it "deletes the user" do
          subject
          expect(User.find_by(id: user_id)).to be_nil
        end

        it "deletes the essay" do
          subject
          expect(EssayContents::Internal.find_by(id: essay_content_id)).to be_nil
          expect(Essay.find_by(id: essay_id)).to be_nil
        end
      end

      context "the user has a published essay" do
        let(:status) { :published }

        it "deletes the user" do
          subject
          expect(User.find_by(id: user_id)).to be_nil
        end

        it "deletes the essay" do
          subject
          expect(EssayContents::Internal.find_by(id: essay_content_id)).to be_nil
          expect(Essay.find_by(id: essay_id)).to be_nil
        end
      end
    end

    context "the user is an admin" do
      let(:level) { :admin }

      context "the user has no published essays" do
        let(:status) { :draft }

        it "deletes the user" do
          subject
          expect(User.find_by(id: user_id)).to be_nil
        end

        it "deletes the essay" do
          subject
          expect(EssayContents::Internal.find_by(id: essay_content_id)).to be_nil
          expect(Essay.find_by(id: essay_id)).to be_nil
        end
      end

      context "the user has a published essay" do
        let(:status) { :published }

        it "deletes the user" do
          subject
          expect(User.find_by(id: user_id)).to be_nil
        end

        it "deletes the essay" do
          subject
          expect(EssayContents::Internal.find_by(id: essay_content_id)).to be_nil
          expect(Essay.find_by(id: essay_id)).to be_nil
        end
      end
    end

    context "the user is a student" do
      let(:level) { :eleve }

      context "the user has no published essays" do
        let(:status) { :draft }

        context "the user has an active recurring premium access" do
          let(:provider_id) { "sub_123" }
          let!(:premium_access) { create(:premium_access, user: user, provider: :stripe, provider_id: provider_id) }

          before do
            allow(Stripe::Subscription).to receive(:retrieve).with(provider_id).and_return(JSON.parse({status: stripe_status}.to_json, object_class: OpenStruct))
          end

          context "the recurring access hasn't been cancelled" do
            let(:stripe_status) { "active" }

            it "doesn't delete the user" do
              subject
              expect(User.find_by(id: user_id)).not_to be_nil
            end

            it "doesn't delete his essays" do
              subject
              expect(EssayContents::Internal.find_by(id: essay_content_id)).not_to be_nil
              expect(Essay.find_by(id: essay_id)).not_to be_nil
            end

            it "set an alert message" do
              subject
              expect(flash[:alert]).to be_present
            end

            it "redirects to the essay view" do
              expect(subject).to redirect_to(membres_accueil_path)
            end
          end

          context "the recurring access has been cancelled" do
            let(:stripe_status) { "inactive" }

            it "deletes the user" do
              subject
              expect(User.find_by(id: user_id)).to be_nil
            end

            it "deletes the essay" do
              subject
              expect(EssayContents::Internal.find_by(id: essay_content_id)).to be_nil
              expect(Essay.find_by(id: essay_id)).to be_nil
            end
          end
        end

        context "the user hasn't an active recurring premium access" do
          it "deletes the user" do
            subject
            expect(User.find_by(id: user_id)).to be_nil
          end

          it "deletes the essay" do
            subject
            expect(EssayContents::Internal.find_by(id: essay_content_id)).to be_nil
            expect(Essay.find_by(id: essay_id)).to be_nil
          end
        end
      end

      context "the user has a published essay" do
        let(:status) { :published }

        it "doesn't delete the user" do
          subject
          expect(User.find_by(id: user_id)).not_to be_nil
        end

        it "doesn't delete his essays" do
          subject
          expect(EssayContents::Internal.find_by(id: essay_content_id)).not_to be_nil
          expect(Essay.find_by(id: essay_id)).not_to be_nil
        end

        it "set an alert message" do
          subject
          expect(flash[:alert]).to be_present
        end

        it "redirects to the essay view" do
          expect(subject).to redirect_to(membres_accueil_path)
        end
      end
    end
  end
end
