# frozen_string_literal: true

require "rails_helper"

RSpec.describe CategoriesController, type: :controller do
  describe "GET #index" do
    subject { get(:index, params: {type: type, matiere: matiere}) }
    let(:type) { nil }
    let(:matiere) { nil }

    describe "params_type" do
      context "it is given" do
        context "it is a valid value" do
          let(:type) { "commentaire" }
          it "assigns @type this valid symbolized value" do
            subject
            expect(assigns(:type)).to eq(type.to_sym)
          end
        end

        context "it is not a valid value" do
          let(:type) { "toto" }
          it "assigns :dissertation to @type" do
            subject
            expect(assigns(:type)).to eq(:dissertation)
          end
        end
      end

      context "it is not given" do
        it "assigns :dissertation to @type" do
          subject
          expect(assigns(:type)).to eq(:dissertation)
        end
      end
    end

    describe "params_matiere" do
      context "it is given" do
        context "it is a valid value" do
          let(:matiere) { "francais" }
          it "assigns @matiere this valid symbolized value" do
            subject
            expect(assigns(:matiere)).to eq(matiere.to_sym)
          end
        end

        context "it is not a valid value" do
          let(:matiere) { "toto" }
          it "assigns :philosophie to @type" do
            subject
            expect(assigns(:matiere)).to eq(:philosophie)
          end
        end
      end

      context "it is not given" do
        it "assigns :philosophie to @matiere" do
          subject
          expect(assigns(:matiere)).to eq(:philosophie)
        end
      end
    end

    it "returns http success" do
      subject
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #edit" do
    subject { get(:edit, params: {id: slug}) }
    let(:user) { create(:user, level: level) }
    let(:level) { :admin }

    context "the category doesn't exist" do
      let(:slug) { "seljavallalaug" }

      context "the user is signed in" do
        before { sign_in(user) }

        it "redirect to error 404 page" do
          is_expected.to redirect_to("/404")
        end
      end

      context "the user is not signed in" do
        it "redirect to the login page" do
          expect(subject).to redirect_to(new_user_session_path)
        end
      end
    end

    context "the category exists" do
      let!(:category) { create(:category, :philosophie_dissertation) }
      let(:slug) { category.slug }

      context "the user is not signed in" do
        it "redirect to the login page" do
          expect(subject).to redirect_to(new_user_session_path)
        end

        it "displays an error message" do
          subject
          expect(flash[:alert]).to match("Vous devez vous connecter ou vous enregistrer pour continuer.")
        end
      end

      context "the user is signed in" do
        before { sign_in(user) }

        context "the user is not an admin" do
          let(:level) { :eleve }

          it "redirects to user home page" do
            is_expected.to redirect_to(membres_accueil_path)
          end
        end

        context "the user is an admin" do
          it "returns a ok http status" do
            is_expected.to have_http_status(:ok)
          end

          it "assigns the category to @category" do
            subject
            expect(assigns(:category)).to eq(category)
          end
        end
      end
    end
  end

  describe "PUT #update" do
    let(:user) { create(:user, level: level) }
    let(:level) { :eleve }
    let(:category) { create(:category, :philosophie_commentaire) }

    subject { put(:update, params: params) }

    let(:params) do
      {
        id: category.slug,
        category: {
          name: name,
          slug: slug,
          sorting_index: sorting_index,
          avatar: avatar
        }
      }
    end
    let(:name) { "Nouveau nom" }
    let(:slug) { "nouveau-slug" }
    let(:sorting_index) { 1000 }
    let(:avatar) { fixture_file_upload "#{Rails.root}/spec/factories/image/avatar.jpg", "image/jpeg" }

    context "the user is not signed in" do
      it "redirect to the login page" do
        expect(subject).to redirect_to(new_user_session_path)
      end
    end

    context "the user is signed in" do
      before { sign_in(user) }

      context "the user is not an admin" do
        it "redirects to user home page" do
          is_expected.to redirect_to(membres_accueil_path)
        end
      end

      context "the user is an admin" do
        let(:level) { :admin }

        it "returns an found http status" do
          is_expected.to have_http_status(:found)
        end

        it "updates the info" do
          expect { subject }.to change { category.reload.name }.to(name)
            .and change { category.reload.slug }.to(slug)
            .and change { category.reload.sorting_index }.to(sorting_index)
        end

        it "attaches the avatar to the category" do
          subject
          expect(category.avatar).to be_attached
        end
      end
    end
  end

  describe "DELETE #destroy" do
    let(:user) { create(:user, level: level) }
    let(:level) { :admin }
    subject { delete(:destroy, params: {id: slug}) }

    context "the id doesn't exist" do
      let(:slug) { "sdkqldkqjd" }

      context "the user is signed in" do
        before { sign_in(user) }

        it "redirects to 404 error page" do
          is_expected.to redirect_to("/404")
        end
      end

      context "the user is not signed in" do
        it "redirect to the login page" do
          expect(subject).to redirect_to(new_user_session_path)
        end
      end
    end

    context "the id exists" do
      let!(:category) { create(:category, :philosophie_dissertation) }
      let(:slug) { category.slug }

      context "the user is not signed in" do
        it "redirect to the login page" do
          expect(subject).to redirect_to(new_user_session_path)
        end
      end

      context "the user is signed in" do
        before { sign_in(user) }

        context "the user is an admin" do
          let(:level) { :admin }

          it "returns a found http status" do
            is_expected.to have_http_status(:found)
          end

          context "the category has no topics" do
            it "deletes the category" do
              subject
              expect(Category.find_by(slug: slug)).to be_nil
            end
          end

          context "the category has topics" do
            let!(:topic) { create(:topic, :philosophie_dissertation, category: category) }

            it "doesn't delete the category" do
              subject
              expect(Category.find_by(slug: category.slug)).not_to be_nil
            end

            it "doesn't delete the topic" do
              subject
              expect(Topic.find_by(id: topic.id)).not_to be_nil
            end
          end
        end

        context "the user is a professor" do
          let(:level) { :professeur }

          it "redirects to users home" do
            is_expected.to redirect_to(membres_accueil_path)
          end

          it "doesn't delete the category" do
            subject
            expect(Category.find_by(slug: slug)).not_to be_nil
          end
        end

        context "the user is a student" do
          let(:level) { :eleve }

          it "redirects to users home" do
            is_expected.to redirect_to(membres_accueil_path)
          end

          it "doesn't delete the category" do
            subject
            expect(Category.find_by(slug: slug)).not_to be_nil
          end
        end
      end
    end
  end
end
