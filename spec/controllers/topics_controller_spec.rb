# frozen_string_literal: true

require "rails_helper"

RSpec.describe TopicsController, type: :controller do
  describe "GET #index" do
    let(:category) { create(:category, :francais) }
    subject { get(:index, params: {category_id: category_slug}) }

    context "the category_slug exists" do
      let(:category_slug) { category.slug }

      it "returns http success" do
        subject
        expect(response).to have_http_status(:success)
      end
    end

    context "the category_name does not exist" do
      let(:category_slug) { -1 }

      it "redirects_to categories_path" do
        expect(subject).to redirect_to(categories_path)
      end
    end
  end

  describe "GET #new" do
    subject { get(:new) }

    context "the user is not signed in" do
      it "redirect to the login page" do
        expect(subject).to redirect_to(new_user_session_path)
      end

      it "displays an error message" do
        subject
        expect(flash[:alert]).to match("Vous devez vous connecter ou vous enregistrer pour continuer.")
      end
    end

    context "the user is signed in" do
      before { sign_in(user) }
      let(:user) { create(:user, level: level) }

      context "the user is not admin" do
        let(:level) { :eleve }

        it "redirect to the users home" do
          expect(subject).to redirect_to(membres_accueil_path)
        end

        it "displays an error message" do
          subject
          expect(flash[:alert]).to match("Vous devez être administrateur pour effectuer cette action.")
        end
      end

      context "the user is admin" do
        let(:level) { :admin }

        it { is_expected.to have_http_status(:ok) }
      end
    end
  end

  describe "POST #create" do
    subject do
      post(
        :create,
        params: params
      )
    end

    let(:category) { create(:category, :philosophie_dissertation) }
    let(:params) { {essay: {content: ""}} }

    context "the user is not signed in" do
      it "redirect to the login page" do
        expect(subject).to redirect_to(new_user_session_path)
      end

      it "displays an error message" do
        subject
        expect(flash[:alert]).to match("Vous devez vous connecter ou vous enregistrer pour continuer.")
      end
    end

    context "the user is signed in" do
      let(:user) { create(:user, level: level) }
      before { sign_in(user) }

      context "the user is not an admin" do
        let(:level) { :eleve }

        it "redirect to the users home" do
          expect(subject).to redirect_to(membres_accueil_path)
        end

        it "displays an error message" do
          subject
          expect(flash[:alert]).to match("Vous devez être administrateur pour effectuer cette action.")
        end
      end

      context "the user is an admin" do
        let(:level) { :admin }

        let(:topic_title) { "le titre" }
        let(:past_exam) { "Annale 2008" }
        let(:params) do
          {
            topic: {
              title: topic_title,
              past_exam: past_exam,
              category_id: category.id
            }
          }
        end
        it "creates a topic" do
          expect { subject }.to change(Topic, :count).by(1)
        end

        it "updates the topic past_exam attribute" do
          subject

          topic = Topic.find_by(title: topic_title)
          expect(topic.past_exam).to eq(past_exam)
        end
      end
    end
  end

  describe "GET #edit" do
    subject { get(:edit, params: {id: id}) }
    let(:user) { create(:user, level: level) }
    let(:level) { :admin }

    context "the topic doesn't exist" do
      let(:id) { -1 }

      context "the user is signed in" do
        before { sign_in(user) }

        it "redirect to error 404 page" do
          is_expected.to redirect_to("/404")
        end
      end

      context "the user is not signed in" do
        it "redirect to the login page" do
          expect(subject).to redirect_to(new_user_session_path)
        end
      end
    end

    context "the topic exists" do
      let!(:topic) { create(:topic, :philosophie_dissertation) }
      let(:essay) { create(:essay, :internal, topic: topic) }
      let(:id) { topic.id }

      context "the user is not signed in" do
        it "redirect to the login page" do
          expect(subject).to redirect_to(new_user_session_path)
        end

        it "displays an error message" do
          subject
          expect(flash[:alert]).to match("Vous devez vous connecter ou vous enregistrer pour continuer.")
        end
      end

      context "the user is signed in" do
        before { sign_in(user) }

        context "the user is not an admin" do
          let(:level) { :eleve }

          it "redirects to the user home page" do
            is_expected.to redirect_to(membres_accueil_path)
          end
        end

        context "the user is an admin" do
          it "returns a ok http status" do
            is_expected.to have_http_status(:ok)
          end

          it "assigns topic to @topic" do
            subject
            expect(assigns(:topic)).to eq(topic)
          end

          it "assigns the right subject, exercice type, groups" do
            subject
            group = topic.category.group
            groups = Group.where(exercice_type: group.exercice_type.to_sym, subject: group.subject.to_sym)
            expect(assigns(:matiere)).to eq(group.subject.to_sym)
            expect(assigns(:type)).to eq(group.exercice_type.to_sym)
            expect(assigns(:groups)).to eq(groups)
          end
        end
      end
    end
  end

  describe "PUT #update" do
    let(:user) { create(:user, level: level) }
    let(:level) { :eleve }
    let(:topic) { create(:topic, :philosophie_commentaire) }

    subject { put(:update, params: params) }

    let(:params) do
      {
        id: topic.id,
        topic: {
          studied_text: studied_text,
          category_id: category.id,
          book: book,
          part: part,
          themes: themes,
          edition: edition,
          past_exam: past_exam,
          title_short: title_short,
          studied_text_audio: studied_text_audio
        },
        author: {
          other: author_other
        }
      }
    end
    let(:category) { create(:category, :philosophie_commentaire) }
    let(:author_other) { "" }
    let(:studied_text) { "Nouveau texte" }
    let(:book) { "Nouveau livre" }
    let(:part) { "nouveau passage" }
    let(:themes) { "nouveaux thèmes" }
    let(:edition) { "nouvelle édition" }
    let(:past_exam) { "nouvelles annales" }
    let(:title_short) { "nouveau titre court" }
    let(:studied_text_audio) { fixture_file_upload "#{Rails.root}/spec/factories/mp3/sample.mp3", "audio/mpeg" }

    context "the user is not signed in" do
      it "redirect to the login page" do
        expect(subject).to redirect_to(new_user_session_path)
      end
    end

    context "the user is signed in" do
      before { sign_in(user) }

      context "the user is not an admin" do
        it "redirects to the user home page" do
          is_expected.to redirect_to(membres_accueil_path)
        end
      end

      context "the user is an admin" do
        let(:level) { :admin }

        it "does NOT redirect to the user home page" do
          is_expected.not_to redirect_to(membres_accueil_path)
        end

        it "updates the info" do
          expect { subject }.to change { topic.reload.category }.to(category)
            .and change { topic.reload.studied_text }.to(studied_text)
            .and change { topic.reload.book }.to(book)
            .and change { topic.reload.part }.to(part)
            .and change { topic.reload.themes }.to(themes)
            .and change { topic.reload.edition }.to(edition)
            .and change { topic.reload.past_exam }.to(past_exam)
            .and change { topic.reload.title_short }.to(title_short)
        end

        it "attaches the studied_text_audio to the topic" do
          subject
          expect(topic.studied_text_audio).to be_attached
        end

        context "an author_other is provided" do
          let(:author_other) { "autre auteur" }

          it "takes precedence over the category" do
            subject

            unassigned_category = Category.find_by(name: author_other)
            expect(topic.reload.category).to eq(unassigned_category)
          end
        end

        context 'the topic type is a "dissertation"' do
          let(:topic) { create(:topic, :philosophie_dissertation) }
          let(:params) do
            {
              id: topic.id,
              topic: {
                title: title,
                category_id: category.id,
                past_exam: past_exam,
                title_short: title_short
              }
            }
          end
          let(:category) { create(:category, :philosophie_dissertation) }
          let(:title) { "Nouveau sujet de dissertation" }
          let(:past_exam) { "nouvelles annales" }

          it "updates the title, category and past_exam fields" do
            expect { subject }.to change { topic.reload.category }.to(category)
              .and change { topic.reload.title }.to(title)
              .and change { topic.reload.past_exam }.to(past_exam)
              .and change { topic.reload.title_short }.to(title_short)
          end
        end
      end
    end
  end

  describe "DELETE #destroy" do
    let(:user) { create(:user, level: level) }
    let(:level) { :admin }
    subject { delete(:destroy, params: {id: id}) }

    context "the id doesn't exist" do
      let(:id) { -1 }

      context "the user is signed in" do
        before { sign_in(user) }

        it "redirects to 404 error page" do
          is_expected.to redirect_to("/404")
        end
      end

      context "the user is not signed in" do
        it "redirect to the login page" do
          expect(subject).to redirect_to(new_user_session_path)
        end
      end
    end

    context "the id exists" do
      let!(:topic) { create(:topic, :philosophie_dissertation) }
      let(:id) { topic.id }

      context "the user is not signed in" do
        it "redirect to the login page" do
          expect(subject).to redirect_to(new_user_session_path)
        end
      end

      context "the user is signed in" do
        before { sign_in(user) }

        context "the user is an admin" do
          let(:level) { :admin }

          it "returns a found http status" do
            is_expected.to have_http_status(:found)
          end

          context "the topic has no essays" do
            it "deletes the topic" do
              subject
              expect(Topic.find_by(id: id)).to be_nil
            end
          end

          context "the topic has essays" do
            let!(:essay) { create(:essay, :internal, :philosophie_dissertation, topic: topic) }

            it "doesn't delete the topic" do
              subject
              expect(Topic.find_by(id: essay.topic.id)).not_to be_nil
            end

            it "doesn't delete the essay" do
              subject
              expect(EssayContents::Internal.find_by(id: essay.essay_content.id)).not_to be_nil
              expect(Essay.find_by(id: essay.id)).not_to be_nil
            end
          end
        end

        context "the user is a professor" do
          let(:level) { :professeur }

          it "redirects to users home" do
            is_expected.to redirect_to(membres_accueil_path)
          end

          it "doesn't delete the topic" do
            subject
            expect(Topic.find_by(id: id)).not_to be_nil
          end
        end

        context "the user is a student" do
          let(:level) { :eleve }

          it "redirects to users home" do
            is_expected.to redirect_to(membres_accueil_path)
          end

          it "doesn't delete the topic" do
            subject
            expect(Topic.find_by(id: id)).not_to be_nil
          end
        end
      end
    end
  end
end
