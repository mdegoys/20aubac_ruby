require "rails_helper"

RSpec.describe "ErrorsPages", type: :request do
  describe "not_found" do
    context "we visit an unknown address" do
      subject { get "/unknown-folder/unknown-address" }

      it "returns a not found error" do
        # Can't test this as RoutingError occurs and it doesn't go further :(
        # subject
        # expect(response.layout).to eq('errors_pages/not_found')
      end
    end
  end

  describe "internal_server_error" do
    context "we have a standard error" do
      subject { raise StandardError }

      it "returns http success" do
        # same as above
        # subject
        # expect(response.layout).to eq('errors_pages/internal_error')
      end
    end
  end
end
