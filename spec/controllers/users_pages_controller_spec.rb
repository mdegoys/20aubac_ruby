require "rails_helper"

RSpec.describe UsersPagesController, type: :controller do
  describe "GET #home" do
    subject { get(:home) }

    context "the user is not signed in" do
      it "redirect to the login page" do
        expect(subject).to redirect_to(new_user_session_path)
      end

      it "displays an error message" do
        subject
        expect(flash[:alert]).to match("Vous devez vous connecter ou vous enregistrer pour continuer.")
      end
    end

    context "the user is signed in" do
      let(:user) { create(:user) }
      before { sign_in(user) }

      it "returns a not found http status" do
        is_expected.to have_http_status(:ok)
      end
    end
  end

  describe "GET #essays" do
    subject { get(:essays) }

    context "the user is not signed in" do
      it "redirect to the login page" do
        expect(subject).to redirect_to(new_user_session_path)
      end

      it "displays an error message" do
        subject
        expect(flash[:alert]).to match("Vous devez vous connecter ou vous enregistrer pour continuer.")
      end
    end

    context "the user is signed in" do
      let(:user) { create(:user) }
      before { sign_in(user) }

      it "returns a ok http status" do
        is_expected.to have_http_status(:ok)
      end
    end
  end

  describe "GET #premium_accesses" do
    let(:user) { create(:user) }
    let!(:active_premium_access) { create(:premium_access, user: user) }
    let!(:inactive_premium_access) { create(:premium_access, user: user, activate_at: 1.day.from_now) }
    let!(:inactive_premium_access2) { create(:premium_access, user: user, activate_at: 10.days.ago, expire_at: 1.day.ago) }

    subject { get(:premium_accesses) }

    context "the user is not signed in" do
      it "redirect to the login page" do
        expect(subject).to redirect_to(new_user_session_path)
      end

      it "displays an error message" do
        subject
        expect(flash[:alert]).to match("Vous devez vous connecter ou vous enregistrer pour continuer.")
      end
    end

    context "the user is signed in" do
      let(:user) { create(:user) }
      before { sign_in(user) }

      it "returns a ok http status" do
        is_expected.to have_http_status(:ok)
      end

      it "assigns the active premium access to @active_premium_acces" do
        subject
        expect(assigns(:active_premium_access)).to eq(active_premium_access)
      end

      it "assigns the other premium accesses to @other_premium_acceses" do
        subject
        expect(assigns(:other_premium_accesses)).to include(inactive_premium_access, inactive_premium_access2)
      end
    end
  end

  describe "GET #premium_recurring" do
    subject { get(:premium_recurring) }
    let(:user) { create(:user, stripe_id: stripe_id, level: level) }
    let(:stripe_id) { nil }

    context "the user is not signed in" do
      it "redirect to the login page" do
        expect(subject).to redirect_to(new_user_session_path)
      end
    end

    context "the user is signed in" do
      before(:each) do
        sign_in(user)
      end

      context "the user is a professeur" do
        let(:level) { :professeur }

        it "redirects to the user home page" do
          expect(subject).to redirect_to(membres_accueil_path)
        end
      end

      context "the user is an admin" do
        let(:level) { :admin }

        it "redirects to the user home page" do
          expect(subject).to redirect_to(membres_accueil_path)
        end
      end

      context "the user is an eleve" do
        let(:level) { :eleve }

        context "the user already has a premium access" do
          let!(:premium_access) { create(:premium_access, user: user) }

          it "sets an alert message" do
            subject
            expect(flash[:alert]).to be_present
          end

          it "redirects the user to membres_accespremium_path" do
            expect(subject).to redirect_to(membres_accespremium_path)
          end
        end

        context "the user doesn't already have a subscription" do
          context "the user doesn't have a stripe_id" do
            context "there is an invalid request error from Stripe" do
              before do
                allow(StripeHelper).to receive(:create_customer).with(user).and_raise(Stripe::InvalidRequestError.new("Invalid email address", "WrongEmail"))
              end

              it "doesn't give to the user a stripe_id" do
                expect { subject }.not_to change { user.reload.stripe_id }
              end

              it "logs an error" do
                expect(RorVsWild).to receive(:record_error)
                subject
              end

              it "sets a alert flash message" do
                subject
                expect(flash[:alert]).to be_present
              end

              it "redirects to user homepage" do
                expect(subject).to redirect_to(membres_accueil_path)
              end
            end

            context "there is no error with StripeHelper" do
              it "calls StripeHelper.create_customer" do
                generated_stripe_id = "cus_NEW123"
                expect(StripeHelper).to receive(:create_customer)
                  .with(user)
                  .and_return(JSON.parse({id: generated_stripe_id}.to_json, object_class: OpenStruct))
                subject
              end

              it "gives to the user a stripe_id" do
                generated_stripe_id = "cus_NEW123"
                expect(StripeHelper).to receive(:create_customer)
                  .with(user)
                  .and_return(JSON.parse({id: generated_stripe_id}.to_json, object_class: OpenStruct))
                expect { subject }.to change { user.reload.stripe_id }.from(nil).to(generated_stripe_id)
              end
            end
          end

          context "the user has a stripe_id" do
            let(:stripe_id) { "cus_123ABC" }

            it "doesn't call StripeHelper.create_customer" do
              expect(StripeHelper).not_to receive(:create_customer)
              subject
            end
          end
        end
      end
    end
  end
end
