# frozen_string_literal: true

require "rails_helper"

RSpec.describe PremiumAccessesController, type: :controller do
  describe "PUT #create_stripe_subscription" do
    subject { put(:create_stripe_subscription, params: {customerId: customer_id}) }

    context "params customer_id is given" do
      let(:customer_id) { "cus_123ABC" }
      it "calls StripeHelper.create_subscription" do
        price_id = Rails.application.credentials.dig(:stripe, :price_id)
        expect(StripeHelper).to receive(:create_subscription)
          .with(customer_id, price_id)
          .and_return(JSON.parse({id: "sub_123ABC",
                                  latest_invoice: {
                                    payment_intent: {
                                      clientSecret: "clientSecret"
                                    }
                                  }}.to_json, object_class: OpenStruct))
        subject
      end
    end

    context "params customer_id is not given" do
      let(:customer_id) { nil }

      it "doesn't call StripeHelper" do
        expect(StripeHelper).not_to receive(:create_subscription)
        subject
      end

      it "returns a json containing an error message" do
        subject
        parsed_body = JSON.parse(response.body)
        expect(parsed_body["error"]["message"]).to eq("Le paramètre customerId est manquant.")
      end
    end
  end

  describe "PUT #cancel" do
    let(:premium_access) { create(:premium_access, provider_id: provider_id, provider: :stripe, user: premium_access_user) }
    let(:premium_access_user) { create(:user) }
    let(:current_user) { premium_access_user }
    let(:premium_access_id) { premium_access.id }

    subject { put(:cancel, params: {id: premium_access_id}) }

    before(:each) do
      sign_in(current_user)
    end

    context "the premium accesss id doesn't exist" do
      let(:premium_access_id) { -1 }

      it "doesn't call #cancel on the premium accesss" do
        expect_any_instance_of(PremiumAccess).not_to receive(:cancel)
        subject
      end

      it "sets an alert message" do
        subject
        expect(flash[:alert]).to be_present
      end

      it "redirects to membres_accespremium_path" do
        expect(subject).to redirect_to(membres_accespremium_path)
      end
    end

    context "the premium accesss id exists" do
      context "the premium accesss is not cancelable" do
        let(:provider_id) { nil }

        it "doesn't call #cancel on the premium accesss" do
          expect_any_instance_of(PremiumAccess).not_to receive(:cancel)
          subject
        end

        it "sets an alert message" do
          subject
          expect(flash[:alert]).to be_present
        end

        it "redirects to membres_accespremium_path" do
          expect(subject).to redirect_to(membres_accespremium_path)
        end
      end

      context "the premium accesss is cancelable" do
        let(:provider_id) { "sub_123ABC" }
        before(:each) do
          allow(StripeHelper).to receive(:status_for).with(provider_id).and_return("active")
        end

        context "the premium accesss is not the user's" do
          let(:current_user) { create(:user) }

          it "doesn't call #cancel on the premium accesss" do
            expect_any_instance_of(PremiumAccess).not_to receive(:cancel)
            subject
          end

          it "sets an alert message" do
            subject
            expect(flash[:alert]).to be_present
          end

          it "redirects to membres_accespremium_path" do
            expect(subject).to redirect_to(membres_accespremium_path)
          end
        end

        context "the premium accesss is the user's" do
          before(:each) do
            allow(Stripe::Subscription).to receive(:delete).with(provider_id)
          end

          it "calls #cancel on the premium accesss" do
            expect_any_instance_of(PremiumAccess).to receive(:cancel)
            subject
          end

          it "sets a success message" do
            subject
            expect(flash[:success]).to be_present
          end

          it "redirects to membres_accespremium_path" do
            expect(subject).to redirect_to(membres_accespremium_path)
          end
        end
      end
    end
  end

  describe "PUT #activate" do
    let(:premium_access) { create(:premium_access, activate_at: activate_at, expire_at: expire_at, user: premium_access_user) }
    let(:activate_at) { nil }
    let(:expire_at) { nil }
    let(:premium_access_user) { create(:user) }
    let(:current_user) { premium_access_user }
    let(:premium_access_id) { premium_access.id }

    subject { put(:activate, params: {id: premium_access_id}) }

    before(:each) do
      sign_in(current_user)
    end

    context "the premium accesss id doesn't exist" do
      let(:premium_access_id) { -1 }

      it "doesn't call #activate on the premium accesss" do
        expect_any_instance_of(PremiumAccess).not_to receive(:activate)
        subject
      end

      it "sets an alert message" do
        subject
        expect(flash[:alert]).to be_present
      end

      it "redirects to membres_accespremium_path" do
        expect(subject).to redirect_to(membres_accespremium_path)
      end
    end

    context "the premium accesss id exists" do
      context "the premium accesss is not activable" do
        let(:expire_at) { 3.days.from_now }

        it "doesn't call #activate on the premium accesss" do
          expect_any_instance_of(PremiumAccess).not_to receive(:activate)
          subject
        end

        it "sets an alert message" do
          subject
          expect(flash[:alert]).to be_present
        end

        it "redirects to membres_accespremium_path" do
          expect(subject).to redirect_to(membres_accespremium_path)
        end
      end

      context "the premium accesss is activable" do
        context "the premium accesss is not the user's" do
          let(:current_user) { create(:user) }

          it "doesn't call #activate on the premium accesss" do
            expect_any_instance_of(PremiumAccess).not_to receive(:activate)
            subject
          end

          it "sets an alert message" do
            subject
            expect(flash[:alert]).to be_present
          end

          it "redirects to membres_accespremium_path" do
            expect(subject).to redirect_to(membres_accespremium_path)
          end
        end

        context "the premium accesss is the user's" do
          it "calls #activate on the premium accesss" do
            expect_any_instance_of(PremiumAccess).to receive(:activate)
            subject
          end

          it "sets a success message" do
            subject
            expect(flash[:success]).to be_present
          end

          it "redirects to membres_accespremium_path" do
            expect(subject).to redirect_to(membres_accespremium_path)
          end
        end
      end
    end
  end

  describe "POST #webhook_stripe" do
    subject { post(:webhook_stripe) }
    let(:customer) { create(:user, stripe_id: "cus_123ABC") }
    let(:event) do
      {
        type: "invoice.payment_succeeded",
        data: {
          object: {
            subscription: subscription_stripe_id,
            customer: customer.stripe_id,
            billing_reason: billing_reason
          }
        }
      }
    end

    before(:each) do
      allow(Stripe::Webhook).to receive(:construct_event)
        .and_return(JSON.parse(event.to_json, object_class: OpenStruct))
      allow(Stripe::Subscription).to receive(:retrieve)
        .with(subscription_stripe_id)
        .and_return(JSON.parse({current_period_end: 30.days.from_now.to_i}.to_json, object_class: OpenStruct))
    end

    context "a premium accesss is created" do
      let(:subscription_stripe_id) { "sub_123NEW" }
      let(:billing_reason) { "subscription_create" }

      before do
        allow(Stripe::PaymentIntent).to receive(:retrieve).and_return(JSON.parse({payment_method: "XYZ"}.to_json, object_class: OpenStruct))
        allow(Stripe::Subscription).to receive(:update)
      end

      it "create a new premium accesss locally" do
        expect { subject }.to change { PremiumAccess.count }.by(1)
      end

      it "gives the premium accesss to the user, with the right provider_id and provider" do
        subject
        premium_access = PremiumAccess.find_by(provider_id: subscription_stripe_id, provider: :stripe)
        expect(premium_access.user).to eq(customer)
      end

      it "gives the premium accesss a activation and a expiration date" do
        subject
        premium_access = PremiumAccess.find_by(provider_id: subscription_stripe_id, provider: :stripe)
        expect(premium_access.activate_at).not_to be_nil
        expect(premium_access.expire_at).not_to be_nil
      end

      it "returns a json saying success" do
        subject
        expect(JSON.parse(response.body)["status"]).to eq("success")
      end
    end

    context "the premium accesss is renewed" do
      let!(:premium_access) { create(:premium_access, provider_id: "sub_123ABC", user: customer) }
      let(:subscription_stripe_id) { premium_access.provider_id }
      let(:billing_reason) { nil }

      it "extends the existing local premium accesss" do
        expect { subject }.to change { premium_access.reload.expire_at }
      end

      it "doesn't create a new premium accesss" do
        expect { subject }.not_to change { PremiumAccess.count }
      end

      it "returns a json saying success" do
        subject
        expect(JSON.parse(response.body)["status"]).to eq("success")
      end
    end
  end
end
