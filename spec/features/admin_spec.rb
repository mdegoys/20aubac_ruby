# frozen_string_literal: true

require "rails_helper"

RSpec.describe "Admin", type: :feature do
  let(:admin_email) { "admin@admin.com" }
  let(:admin_password) { "123456" }
  let!(:admin) { create(:user, email: admin_email, password: admin_password, level: :admin) }

  describe "consulting an essay" do
    let(:essay) { create(:essay, :internal, :philosophie_dissertation) }
  end

  describe "essay validation" do
    let!(:essay_validation_pending) { create(:essay, :internal, :philosophie_dissertation, status: :validation_pending) }

    it "allows admin to validate an essay pending validation" do
      visit(root_path)
      find_button(text: /^Compte/).click
      page.fill_in("Adresse email", with: admin_email)
      page.fill_in("Mot de passe", with: admin_password)
      find_button(value: "Se connecter").click
      find_button(text: "Compte").click
      find_link(text: "Corrigés en attente").click
      expect(page).to have_current_path(admin_corriges_path)
      expect(page).to have_content(essay_validation_pending.topic.title)

      find_link(text: essay_validation_pending.topic.title).click
      expect(page).to have_current_path(essay_contents_internal_path(essay_validation_pending.essay_content))

      find_button(text: "Accepter").click
      expect(page).to have_current_path(essay_contents_internal_path(essay_validation_pending.reload.essay_content))
      expect(page).to have_content("Statut: En ligne")
    end
  end
end
