# frozen_string_literal: true

require "rails_helper"

RSpec.describe "User", type: :feature do
  let(:email) { "test@test.com" }
  let(:password) { "123456" }

  describe "account creation" do
    it "allows the guest to create an account" do
      visit(root_path)
      find_button(text: /^Compte/).click
      expect(page).to have_current_path(new_user_session_path)

      find_link(text: "Créer un compte").click
      expect(page).to have_current_path(new_user_registration_path)

      page.fill_in("Adresse email", with: email)
      page.fill_in("Nom d'utilisateur", with: "test")
      page.fill_in("Mot de passe", with: password)
      page.fill_in("Confirmation du mot de passe", with: password)
      find_button(value: "Créer le compte").click
      expect(page).to have_content("Bienvenue ! Vous vous êtes bien enregistré(e).")
      expect(page).to have_current_path(membres_accueil_path)
    end
  end

  describe "login" do
    let!(:user) { create(:user, email: email, password: password) }

    it "allows the guest to login" do
      visit(root_path)
      find_button(text: /^Compte/).click
      expect(page).to have_current_path(new_user_session_path)

      page.fill_in("Adresse email", with: email)
      page.fill_in("Mot de passe", with: password)
      find_button(value: "Se connecter").click
      expect(page).to have_content("Connecté(e).")
      expect(page).to have_current_path(membres_accueil_path)
    end
  end

  describe "logout" do
    let!(:user) { create(:user, email: email, password: password) }

    it "allows the logged user to logout" do
      visit(new_user_session_path)
      page.fill_in("Adresse email", with: email)
      page.fill_in("Mot de passe", with: password)
      find_button(value: "Se connecter").click
      find_button(text: "Compte").click
      find_button(text: "Se déconnecter").click
      expect(page).to have_content("Déconnecté(e).")
      expect(page).to have_current_path(root_path)

      find_button(text: /^Compte/).click
      expect(page).to have_current_path(new_user_session_path)

      visit(membres_accueil_path)
      expect(page).to have_content("Vous devez vous connecter ou vous enregistrer pour continuer.")
      expect(page).to have_current_path(new_user_session_path)
    end
  end

  describe "essay creation" do
    let!(:user) { create(:user, email: email, password: password) }
    let!(:category) { create(:category, :philosophie_dissertation) }
    let(:essay_title) { "Titre de la dissert" }
    let(:essay_content) { "Contenu de la dissert" }

    it "allows the logged user to add an essay and submit it for publication" do
      visit(new_user_session_path)
      page.fill_in("Adresse email", with: email)
      page.fill_in("Mot de passe", with: password)
      find_button(value: "Se connecter").click
      find_button(text: "Compte").click
      find_link(text: "Ajouter un corrigé").click
      expect(page).to have_current_path(new_essay_contents_internal_path)

      page.fill_in("Intitulé du sujet", with: essay_title)
      page.fill_in("Description du corrigé", with: "Description dissert")
      page.fill_in("Votre corrigé", with: essay_content)
      find_button(value: "Créer le corrigé").click
      essay_internal = EssayContents::Internal.find_by(content: essay_content)
      expect(page).to have_current_path(essay_contents_internal_path(essay_internal))
      expect(page).to have_content("Statut: Brouillon")

      find_button(text: "Soumettre à validation").click
      expect(page).to have_content("Statut: En cours de validation")

      visit(membres_corriges_path)
      expect(page).to have_content(essay_title)
    end
  end

  describe "consulting NON premium essay" do
    let(:essay_content) { create(:essay_contents_internal, user: author) }
    let!(:essay) { create(:essay, :philosophie_dissertation, essay_content: essay_content) }
    let(:author) { create(:user) }
    let!(:user) { create(:user, email: user_email, password: user_password, level: level) }
    let(:user_password) { "password" }
    let(:user_email) { generate(:email) }

    before do
      visit(root_path)
      find_button(text: /^Compte/).click
      page.fill_in("Adresse email", with: user_email)
      page.fill_in("Mot de passe", with: user_password)
      find_button(value: "Se connecter").click
      visit(essay_contents_internal_path(essay_content))
    end

    describe "professor" do
      let(:level) { :professeur }

      it "doesn't display the premium wall" do
        expect(page).to have_no_content("Accédez à la suite de ce contenu")
      end
    end

    describe "student" do
      let(:level) { :eleve }

      it "doesn't display the premium wall" do
        expect(page).to have_no_content("Accédez à la suite de ce contenu")
      end
    end
  end

  describe "consulting premium essay" do
    let(:essay_content) { create(:essay_contents_internal, premium: true) }
    let!(:essay) { create(:essay, :philosophie_dissertation, essay_content: essay_content) }
    let!(:user) { create(:user, email: user_email, password: user_password, level: level) }
    let!(:premium_access) { create(:premium_access, user: premium_user) }
    let(:premium_user) { create(:user) }
    let(:user_password) { "password" }
    let(:user_email) { generate(:email) }

    before do
      visit(root_path)
      find_button(text: /^Compte/).click
      page.fill_in("Adresse email", with: user_email)
      page.fill_in("Mot de passe", with: user_password)
      find_button(value: "Se connecter").click
      visit(essay_contents_internal_path(essay_content))
    end

    context "professor" do
      let(:level) { :professeur }
      it "doesn't display the premium wall" do
        expect(page).to have_current_path(essay_contents_internal_path(essay_content))
        expect(page).to have_no_content("Accédez à la suite de ce contenu")
      end
    end

    context "student" do
      let(:level) { :eleve }
      context "having an active premium access" do
        let(:premium_user) { user }

        it "doesn't display the premium wall" do
          expect(page).to have_no_content("Accédez à la suite de ce contenu")
        end
      end

      context "not having an active premium access" do
        it "displays the premium wall" do
          expect(page).to have_content("Accédez à la suite de ce contenu")
        end
      end
    end
  end

  describe "consulting external essay" do
    let(:essay_content) { create(:essay_contents_external) }
    let!(:essay) { create(:essay, :philosophie_dissertation, essay_content: essay_content) }
    let!(:user) { create(:user, email: user_email, password: user_password, level: level) }
    let!(:premium_acces) { create(:premium_access, user: premium_user) }
    let(:premium_user) { create(:user) }
    let(:user_password) { "password" }
    let(:user_email) { generate(:email) }

    before do
      visit(root_path)
      find_button(text: /^Compte/).click
      page.fill_in("Adresse email", with: user_email)
      page.fill_in("Mot de passe", with: user_password)
      find_button(value: "Se connecter").click
      visit(essay_contents_external_path(essay_content))
    end

    describe "the user is a professor" do
      let(:level) { :professeur }
      it "doesn't redirect to another page" do
        expect(page).to have_current_path(essay_contents_external_path(essay_content))
      end

      it "displays a link to the external essay" do
        expect(page).to have_link(href: essay_contents_external_url(essay_content))
      end

      it "displays a link to the pdf archive" do
        expect(page).to have_link(href: archive_pdf_essay_contents_external_path(essay_content))
      end

      describe "the users clicks the pdf archive button" do
        it "displays the pdf" do
          find_link(href: archive_pdf_essay_contents_external_path(essay_content)).click
          expect(response_headers["Content-Type"]).to eq("application/pdf")
          expect(response_headers["Content-Disposition"]).to eq("inline; filename=\"sample.pdf\"; filename*=UTF-8''sample.pdf")
        end
      end
    end

    describe "the user is an eleve" do
      let(:level) { :eleve }

      context "having an active premium access" do
        let(:premium_user) { user }
        it "doesn't redirect to another page" do
          expect(page).to have_current_path(essay_contents_external_path(essay_content))
        end

        it "displays a link to the external essay" do
          expect(page).to have_link(href: essay_contents_external_url(essay_content))
        end

        it "displays a link to the pdf archive" do
          expect(page).to have_link(href: archive_pdf_essay_contents_external_path(essay_content))
        end

        describe "the users clicks the pdf archive button" do
          it "displays the pdf" do
            find_link(href: archive_pdf_essay_contents_external_path(essay_content)).click
            expect(response_headers["Content-Type"]).to eq("application/pdf")
            expect(response_headers["Content-Disposition"]).to eq("inline; filename=\"sample.pdf\"; filename*=UTF-8''sample.pdf")
          end
        end
      end

      context "not having an active premium access" do
        it "doesn't redirect to another page" do
          expect(page).to have_current_path(essay_contents_external_path(essay_content))
        end

        it "displays a link to the external essay" do
          expect(page).to have_link(href: essay_contents_external_url(essay_content))
        end

        it "displays a link to the pdf archive" do
          expect(page).to have_link(href: archive_pdf_essay_contents_external_path(essay_content))
        end

        describe "the users clicks the pdf archive button" do
          it "displays the pdf" do
            find_link(href: archive_pdf_essay_contents_external_path(essay_content)).click
            expect(page).to have_current_path(membres_premiumabonnement_path)
          end
        end
      end
    end
  end
end
