# frozen_string_literal: true

require "rails_helper"

RSpec.describe "Guest", type: :feature do
  describe "homepage" do
    it "welcomes the visitor" do
      visit(root_path)
      expect(page).to have_content("Trouvez votre corrigé, en français ou en philo")
    end
  end

  describe "internal essay" do
    describe "dissertation" do
      let(:essay_title) { "titre de la dissertation" }
      let(:essay_description) { "La description de la dissertation" }
      let(:essay_content_content) { "Le contenu de la dissertation [tp]II. partie 2[/tp]" }
      let(:topic) { create(:topic, :philosophie_dissertation, title: essay_title) }
      let(:essay_content) { create(:essay_contents_internal, description: essay_description, content: essay_content_content) }
      let!(:essay) { create(:essay, :internal, topic: topic, essay_content: essay_content, status: status) }

      describe "published" do
        let(:status) { :published }

        it "displays a link to this essay we can access" do
          visit(root_path)
          expect(page).to have_selector("a", text: essay_title)
        end

        it "redirects to the essay when the link is clicked" do
          visit(root_path)
          first("a", text: essay_title).click

          expect(page).to have_current_path(essay_contents_internal_path(essay.essay_content))
          expect(page).to have_selector("h1", text: essay_title)
          expect(page).to have_content(essay_description)
          expect(page).to have_content("Le contenu de la dissertation")
            .and have_content("II. partie 2")
        end
      end

      describe "not published" do
        let(:status) { :validation_pending }

        it "doesn't display the content" do
          visit(essay_contents_internal_path(essay.essay_content))

          expect(page).to have_no_current_path(essay_contents_internal_path(essay.essay_content))
          expect(page).to have_no_selector("h1", text: essay_title)
          expect(page).to have_no_content(essay_description)
          expect(page).to have_no_content(essay_content_content)
        end
      end
    end

    describe "commentaire" do
      let(:studied_text) { "Texte de commentaire étudiée." }
      let(:topic) { create(:topic, :philosophie_commentaire, studied_text: studied_text) }
      let(:essay) { create(:essay, :internal, topic: topic) }

      it "displays the studied text" do
        visit(essay_contents_internal_path(essay.essay_content))
        expect(page).to have_content(studied_text)
      end
    end
  end

  describe "external essay" do
    let(:essay_title) { "titre de la dissertation" }
    let(:essay_url) { "https://url-de-lessai.fr/essai.html" }
    let(:topic) { create(:topic, :philosophie_dissertation, title: essay_title) }
    let(:essay_content) { create(:essay_contents_external, url: essay_url) }
    let!(:essay) { create(:essay, :internal, topic: topic, essay_content: essay_content, status: status) }

    describe "published" do
      let(:status) { :published }

      it "displays the content" do
        visit(essay_contents_external_path(essay.essay_content))
        expect(page).to have_selector("h1", text: essay_title)
        expect(page).to have_link(href: essay_url)
      end
    end

    describe "not published" do
      let(:status) { :draft }

      it "doesn't display the content" do
        visit(essay_contents_external_path(essay.essay_content))
        expect(page).to have_no_selector("h1", text: essay_title)
        expect(page).to have_no_link(href: essay_url)
      end
    end
  end
end
