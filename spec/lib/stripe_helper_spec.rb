# frozen_string_literal: true

require "rails_helper"

RSpec.describe StripeHelper do
  describe ".create_customer" do
    subject { described_class.create_customer(user) }
    let(:user) { create(:user, level: level, stripe_id: stripe_id) }
    let(:stripe_id) { nil }

    context "there is no user" do
      let(:user) { nil }

      it "doesn't call Stripe::Customer.create" do
        expect(Stripe::Customer).not_to receive(:create)
        subject
      end
    end
    context "there is an user" do
      context "the user is not an 'eleve'" do
        let(:level) { :professeur }

        it "doesn't call Stripe::Customer.create" do
          expect(Stripe::Customer).not_to receive(:create)
          subject
        end
      end

      context "the user is an 'eleve'" do
        let(:level) { :eleve }
        context "the user already has a stripe id" do
          let(:stripe_id) { "existing_stripe_id" }
          it "doesn't call Stripe::Customer.create" do
            expect(Stripe::Customer).not_to receive(:create)
            subject
          end
        end

        context "the user doesn't have a stripe id yet" do
          it "doesn't call Stripe::Customer.create" do
            expect(Stripe::Customer).to receive(:create).with({email: user.email})
            subject
          end
        end
      end
    end
  end

  describe ".status_for" do
    let(:premium_access) { create(:premium_access, provider: :stripe, provider_id: provider_id) }
    let(:provider_id) { "sub_123ABC" }
    subject { described_class.status_for(premium_access.provider_id) }

    it "calls Stripe::Subscription.retrieve" do
      expect(Stripe::Subscription).to receive(:retrieve)
        .with(premium_access.provider_id)
        .and_return(JSON.parse({status: "active"}.to_json, object_class: OpenStruct))
      subject
    end
  end

  describe ".create_subscription" do
    let(:customer_id) { "cus_123ABC" }
    let(:price_id) { "pri_123ABC" }
    subject { described_class.create_subscription(customer_id, price_id) }

    it "calls Stripe::Subscription.create" do
      expect(Stripe::Subscription).to receive(:create)
        .with(customer: customer_id,
          items: [{price: price_id}],
          payment_behavior: "default_incomplete",
          expand: ["latest_invoice.payment_intent"])
      subject
    end
  end

  describe ".cancel_subscription" do
    let(:premium_access) { create(:premium_access, provider: :stripe, provider_id: provider_id) }
    let(:provider_id) { "sub_123ABC" }
    subject { described_class.cancel_subscription(premium_access.provider_id) }

    it "calls Stripe::Subscription.delete" do
      expect(Stripe::Subscription).to receive(:delete).with(premium_access.provider_id)
      subject
    end
  end
end
