# frozen_string_literal: true

require "rails_helper"

RSpec.describe RomanFiguresHelper do
  describe "roman_to_arabic" do
    subject { described_class.roman_to_arabic(roman_number) }

    context "I" do
      let(:roman_number) { "I" }
      it { is_expected.to eq(1) }
    end

    context "III" do
      let(:roman_number) { "III" }
      it { is_expected.to eq(3) }
    end

    context "IV" do
      let(:roman_number) { "IV" }
      it { is_expected.to eq(4) }
    end

    context "XIII" do
      let(:roman_number) { "XIII" }
      it { is_expected.to eq(13) }
    end

    context "MCMXCIX" do
      let(:roman_number) { "MCMXCIX" }
      it { is_expected.to eq(1999) }
    end
  end
end
