# frozen_string_literal: true

require "rails_helper"

RSpec.describe "rake reset_stats_last30d", type: :task do
  let(:essay_internal) { create(:essay, :internal, :philosophie_dissertation, hits_nb_last30d: 1) }
  let(:essay_external_content) { create(:essay_contents_external, archives_access_count: 10) }
  let(:essay_external) { create(:essay, :philosophie_commentaire, essay_content: essay_external_content, hits_nb_last30d: 18) }

  subject { task.execute }

  it "sets all the hits_nb_last30d to 0" do
    expect { subject }.to change { essay_internal.reload.hits_nb_last30d }.to(0)
      .and change { essay_external.reload.hits_nb_last30d }.to(0)
  end

  it "sets all the archives_access_count to 0" do
    expect { subject }.to change { essay_external.essay_content.reload.archives_access_count }.to(0)
  end

  it "sends an email to admin" do
    subject
    expect { task.execute }.to have_enqueued_mail(AdminMailer, :hits_reset)
  end
end
