# frozen_string_literal: true

FactoryBot.define do
  sequence :email do |i|
    "user_#{i}@test.com"
  end

  sequence :dissertation_title do |i|
    "Titre de la dissertation #{i}"
  end

  sequence :name_group do |i|
    "Nom du groupe #{i}"
  end

  sequence :name_category do |i|
    "Nom de la catégorie #{i}"
  end

  sequence :slug_category do |i|
    "nom-categorie-#{i}"
  end

  sequence :url_external do |i|
    "https://www.exemple.fr/page-d-exemple-#{i}.html"
  end
end
