# frozen_string_literal: true

FactoryBot.define do
  factory :question, class: "Quiz::Question" do
    association :categorisable, factory: [:category, :francais]
    statement { "Une question" }
  end
end
