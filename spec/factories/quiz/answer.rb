# frozen_string_literal: true

FactoryBot.define do
  factory :answer, class: "Quiz::Answer" do
    question
    proposition { association :proposition, question: question }
    user
  end
end
