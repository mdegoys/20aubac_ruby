# frozen_string_literal: true

FactoryBot.define do
  factory :proposition, class: "Quiz::Proposition" do
    question
    sequence(:statement) { |n| "Proposition #{n}" }
    correct { false }

    trait :correct do
      correct { true }
    end
  end
end
