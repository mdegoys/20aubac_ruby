# frozen_string_literal: true

FactoryBot.define do
  factory :essay do
    status { :published }

    trait :internal do
      association :essay_content, factory: :essay_contents_internal
    end

    trait :external do
      association :essay_content, factory: :essay_contents_external
    end

    trait :francais do
      association :topic, factory: %i[topic francais]
    end

    trait :philosophie_dissertation do
      association :topic, factory: %i[topic philosophie_dissertation]
    end

    trait :philosophie_commentaire do
      association :topic, factory: %i[topic philosophie_commentaire]
    end
  end
end
