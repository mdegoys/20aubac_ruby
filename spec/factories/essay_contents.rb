FactoryBot.define do
  factory :essay_contents_internal, class: "EssayContents::Internal" do
    user
    description { "Description du corrigé." }
    content do
      'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est.

      [tp]I. partie 1[/tp]

      qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?

      [tp]II. partie 2[/tp]

      Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?'
    end
  end

  factory :essay_contents_external, class: "EssayContents::External" do
    url { generate :url_external }

    after(:build) do |essay_content|
      essay_content.pdf.attach(io: File.open(Rails.root.join("spec", "factories", "pdf", "sample.pdf")), filename: "sample.pdf", content_type: "application/pdf")
    end
  end
end
