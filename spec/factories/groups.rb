# frozen_string_literal: true

FactoryBot.define do
  factory :group do
    trait :francais do
      subject { :francais }
      exercice_type { :commentaire }
      name { generate :name_group }
    end

    trait :philosophie_dissertation do
      subject { :philosophie }
      exercice_type { :dissertation }
      name { generate :name_group }
    end

    trait :philosophie_commentaire do
      subject { :philosophie }
      exercice_type { :commentaire }
      name { generate :name_group }
    end
  end
end
