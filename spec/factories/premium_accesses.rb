FactoryBot.define do
  factory :premium_access do
    user
    expire_at { 30.days.from_now }
    created_at { 1.minute.ago }
    activate_at { 1.minute.ago }
  end
end
