# frozen_string_literal: true

FactoryBot.define do
  factory :category do
    trait :francais do
      name { generate :name_category }
      slug { generate :slug_category }
      sorting_index { 1694 }
      association :group, factory: %i[group francais]
    end

    trait :philosophie_dissertation do
      name { generate :name_category }
      slug { generate :slug_category }
      association :group, factory: %i[group philosophie_dissertation]
    end

    trait :philosophie_commentaire do
      name { generate :name_category }
      slug { generate :slug_category }
      association :group, factory: %i[group philosophie_commentaire]
    end
  end
end
