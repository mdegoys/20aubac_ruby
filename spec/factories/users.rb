FactoryBot.define do
  factory :user do
    email { generate :email }
    username { "username" }
    password { "password" }
  end
end
