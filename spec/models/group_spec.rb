require "rails_helper"

RSpec.describe Group, type: :model do
  it { should have_many(:categories) }
  it { should validate_presence_of(:name) }
  it { should validate_uniqueness_of(:name).scoped_to(:subject) }

  describe "group name is 'non assigné'" do
    let!(:existing_group) { create(:group, :francais, name: Group::NON_ASSIGNED) }

    it "shouldn't check for name uniqueness" do
      new_group = Group.new(name: Group::NON_ASSIGNED, subject: :philosophie, exercice_type: :commentaire)
      expect(new_group).to be_valid
    end
  end
end
