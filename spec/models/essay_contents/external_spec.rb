require "rails_helper"

RSpec.describe EssayContents::External, type: :model do
  describe "relations" do
    it { should have_one(:topic) }
  end

  describe "validations" do
    it { should validate_presence_of(:url) }
  end

  describe ".not_published" do
    subject { described_class.not_published }
    let!(:draft_essay) { create(:essay, :external, :philosophie_dissertation, status: :draft) }
    let!(:published_essay) { create(:essay, :external, :philosophie_dissertation) }

    it "contains drafts essays" do
      expect(subject).to include(draft_essay.essay_content)
    end

    it "doesn't contain published essays" do
      expect(subject).not_to include(published_essay.essay_content)
    end
  end
end
