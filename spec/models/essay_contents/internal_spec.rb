# frozen_string_literal: true

require "rails_helper"

RSpec.describe EssayContents::Internal, type: :model do
  describe "relations" do
    it { should belong_to(:user) }
    it { should have_one(:topic) }
    it { should accept_nested_attributes_for(:topic) }
    it { should accept_nested_attributes_for(:essay) }
  end

  describe "validations" do
    it { should validate_presence_of(:description) }
    it { should validate_presence_of(:content) }
  end

  describe ".not_published" do
    subject { described_class.not_published }
    let!(:draft_essay) { create(:essay, :internal, :philosophie_dissertation, status: :draft) }
    let!(:pending_essay) { create(:essay, :internal, :philosophie_dissertation, status: :validation_pending) }
    let!(:published_essay) { create(:essay, :internal, :philosophie_dissertation) }

    it "contains drafts essays" do
      expect(subject).to include(draft_essay.essay_content)
    end

    it "contains pending validation essays" do
      expect(subject).to include(pending_essay.essay_content)
    end

    it "doesn't contain published essays" do
      expect(subject).not_to include(published_essay.essay_content)
    end
  end

  describe ".to_param" do
    let(:essay) { create(:essay, :philosophie_dissertation, essay_content: essay_contents_internal) }
    let(:essay_contents_internal) { create(:essay_contents_internal, id: 666, keywords: keywords) }

    subject { essay_contents_internal.to_param }

    context "the essay content has keywords" do
      let(:keywords) { "mots-cles" }

      it "contains the keywords" do
        expect(subject).to eq("666-mots-cles")
      end
    end

    context "the essay content doesn't have keywords" do
      let(:keywords) { nil }

      it "contains only the id" do
        expect(subject).to eq("666")
      end
    end
  end
end
