# frozen_string_literal: true

require "rails_helper"

RSpec.describe Quiz::Answer, type: :model do
  describe "relations" do
    it { is_expected.to belong_to(:question).class_name("Quiz::Question") }
    it { is_expected.to belong_to(:proposition).class_name("Quiz::Proposition") }
    it { is_expected.to belong_to(:user) }
  end

  describe "validations" do
  end

  describe "scopes" do
  end
end
