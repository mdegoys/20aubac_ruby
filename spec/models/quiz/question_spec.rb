# frozen_string_literal: true

require "rails_helper"

RSpec.describe Quiz::Question, type: :model do
  describe "relations" do
    it { is_expected.to have_many(:answers).class_name("Quiz::Answer").dependent(:destroy) }
    it { is_expected.to have_many(:propositions).class_name("Quiz::Proposition").dependent(:destroy) }
    it { is_expected.to have_one(:correct_proposition).conditions(correct: true).class_name("Quiz::Proposition") }
    it { is_expected.to belong_to(:categorisable) }
  end

  describe "validations" do
    it { is_expected.to validate_presence_of(:statement) }

    describe "propositions_count" do
      subject { build(:question) }

      context "question does NOT have 4 propositions, including 1 valid" do
        it "is not valid" do
          is_expected.to be_invalid
          expect(subject.errors.full_messages)
            .to match_array(
              [
                "Le nombre de propositions uniques doit être de 4",
                "Le nombre de propositions correctes doit être de 1"
              ]
            )
        end
      end

      context "question has 4 propositions, 1 correct" do
        let(:propositions) { build_list(:proposition, 3) }
        let(:correct_proposition) { build(:proposition, :correct) }
        before { subject.propositions << [propositions, correct_proposition] }

        it { is_expected.to be_valid }

        context "One of the proposition is duplicated" do
          let!(:correct_proposition) { build(:proposition, :correct, statement: propositions.first.statement) }

          it "is not valid" do
            is_expected.to be_invalid
            expect(subject.errors.full_messages)
              .to match_array(
                [
                  "Le nombre de propositions uniques doit être de 4"
                ]
              )
          end
        end
      end
    end
  end

  describe "scopes" do
  end
end
