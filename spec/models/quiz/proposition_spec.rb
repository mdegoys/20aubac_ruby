# frozen_string_literal: true

require "rails_helper"

RSpec.describe Quiz::Proposition, type: :model do
  describe "relations" do
    it { is_expected.to belong_to(:question) }
  end

  describe "validations" do
    it { is_expected.to validate_presence_of(:statement) }
  end

  describe "scopes" do
  end
end
