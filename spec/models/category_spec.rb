# frozen_string_literal: true

require "rails_helper"

RSpec.describe Category, type: :model do
  it { is_expected.to have_many(:topics) }
  it { is_expected.to have_many(:quiz_questions) }
  it { is_expected.to have_many(:sub_categories) }
  it { is_expected.to belong_to(:group) }
  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_uniqueness_of(:name).scoped_to(:group_id) }

  describe "scopes" do
    describe ".without_attached_avatar" do
      subject { described_class.without_attached_avatar }

      let(:category_with_avatar) { create(:category, :philosophie_dissertation) }
      let(:category_without_avatar) { create(:category, :philosophie_dissertation) }

      before do
        io = File.open(Rails.root.join("spec", "factories", "image", "avatar.jpg"))
        category_with_avatar.avatar.attach(io: io, filename: "avatar.jpg", content_type: "image/jpeg")
      end

      it "contains only the categories without avatar" do
        expect(subject).to include(category_without_avatar)
        expect(subject).not_to include(category_with_avatar)
      end
    end
  end

  describe ".create_author_other" do
    subject do
      described_class.create_author_other(author_other, studied_subject)
    end
    let(:author_other) { "Jean-Charles Inconnu" }
    let(:studied_subject) { :francais }

    describe "group" do
      context "there is non NON_ASSIGNED group" do
        it "creates a group" do
          expect { subject }.to change { Group.count }.by(1)
        end
      end

      context "there is already a NON_ASSIGNED group for this subject" do
        let!(:group) do
          create(:group, name: Group::NON_ASSIGNED,
            subject: studied_subject,
            exercice_type: :commentaire)
        end
        it "doesn't create a group" do
          expect { subject }.not_to change { Group.count }
        end
      end
    end

    describe "category" do
      context "there is no category by that name and group" do
        it "creates a category" do
          expect { subject }.to change { Category.count }.by(1)
        end
      end

      context "there is a category by that name and group" do
        let(:group) do
          create(:group, name: Group::NON_ASSIGNED,
            subject: studied_subject,
            exercice_type: :commentaire)
        end
        let!(:category) do
          create(:category, name: author_other, group: group)
        end

        it "doesn't create a category" do
          expect { subject }.not_to change { Category.count }
        end
      end
    end
  end

  describe "#slugify" do
    let(:name) { "Jean-Charles Inconnu" }
    subject { create(:category, :francais, name: name, slug: nil) }

    context "there is no other category with this name" do
      it "creates a slug based on the name" do
        expect(subject.reload.slug).to eq("jean-charles-inconnu")
      end
    end

    context "there is another category with the expected slug" do
      let!(:existing_category) do
        create(:category, :francais, name: "Jean Charles Inconnu", slug: "jean-charles-inconnu")
      end

      it "creates a slug based on name and add suffix" do
        expect(subject.reload.slug).to eq("jean-charles-inconnu-bis")
      end
    end

    context "we update the slug of an existing category" do
      let!(:existing_category) do
        create(:category, :francais, name: "Jean Charles Inconnu")
      end
      subject { existing_category.update(slug: "new-slug") }

      it "doesn't apply the slugify method" do
        subject
        expect(existing_category.reload.slug).to eq("new-slug")
      end
    end
  end
end
