require "rails_helper"

RSpec.describe User, type: :model do
  describe "Associations" do
    it { is_expected.to have_many(:essay_contents_internals).dependent(:destroy) }
    it { is_expected.to have_many(:premium_accesses).dependent(:destroy) }
    it { is_expected.to have_many(:answers).class_name("Quiz::Answer").dependent(:destroy) }
  end

  describe "#active_premium?" do
    let(:user) { create(:user) }
    subject { user.active_premium? }

    context "the user has no premium access" do
      it "returns false" do
        expect(subject).to eq(false)
      end
    end

    context "the user has an expired premium access" do
      let!(:premium_access) { create(:premium_access, user: user, created_at: 3.days.ago, expire_at: 1.day.ago) }

      it "returns false" do
        expect(subject).to eq(false)
      end
    end

    context "the user has an active premium access" do
      let!(:premium_access) { create(:premium_access, user: user) }

      it "returns true" do
        expect(subject).to eq(true)
      end
    end
  end

  describe "#active_recurring_premium?" do
    let(:user) { create(:user) }
    subject { user.active_recurring_premium? }

    context "the user has no premium access" do
      it "returns false" do
        expect(subject).to eq(false)
      end
    end

    context "the user has an expired premium access" do
      let!(:premium_access) { create(:premium_access, user: user, created_at: 3.days.ago, expire_at: 1.day.ago) }

      it "returns false" do
        expect(subject).to eq(false)
      end
    end

    context "the user has an active premium access" do
      let!(:premium_access) { create(:premium_access, user: user, provider_id: provider_id, provider: provider) }
      let(:provider) { nil }

      context "the premium_access is not paid" do
        let(:provider_id) { nil }

        it "returns false" do
          expect(subject).to eq(false)
        end
      end

      context "the premium access is paid" do
        context "the premium access is not recurring" do
          let(:provider_id) { "123ABC" }
          let(:provider) { :mobiyo }

          it "returns true" do
            expect(subject).to eq(false)
          end
        end

        context "the premium access is recurring" do
          let(:provider_id) { "123ABC" }
          let(:provider) { :stripe }

          it "returns true" do
            expect(subject).to eq(true)
          end
        end
      end
    end
  end

  describe "#premium_rights?" do
    let(:user) { create(:user, level: level) }
    subject { user.premium_rights? }

    context "admin" do
      let(:level) { :admin }

      it "returns true" do
        expect(subject).to eq(true)
      end
    end

    context "the user is a professeur" do
      let(:level) { :professeur }

      it "returns true" do
        expect(subject).to eq(true)
      end
    end

    context "the user is an eleve" do
      let(:level) { :eleve }

      context "he has a premium access" do
        let!(:premium_access) { create(:premium_access, user: user) }

        it "returns true" do
          expect(subject).to eq(true)
        end
      end
      context "he has no premium access" do
        it "returns false" do
          expect(subject).to eq(false)
        end
      end
    end
  end

  describe "#active_premium_access" do
    let(:user) { create(:user) }
    subject { user.active_premium_access }

    context "the user has no premium access" do
      it "returns nil" do
        expect(subject).to eq(nil)
      end
    end

    context "the user has at least one premium access" do
      let!(:inactive_premium_access) { create(:premium_access, user: user, activate_at: 1.day.from_now) }

      context "none of them are active" do
        it "returns nil" do
          expect(subject).to eq(nil)
        end
      end

      context "at least one is active" do
        let!(:active_premium_access) { create(:premium_access, user: user, expire_at: 2.days.from_now) }
        let!(:last_active_premium_access) { create(:premium_access, user: user, expire_at: 3.days.from_now) }

        it "returns the last expiring one" do
          expect(subject).to eq(last_active_premium_access)
        end
      end
    end
  end

  describe "#add_premium_access" do
    let(:user) { create(:user, level: user_level) }
    let(:number_of_days) { PremiumAccess::DEFAULT_DURATION }
    let(:provider_id) { nil }
    let(:provider) { :no_provider }
    subject { user.add_premium_access(number_of_days, provider_id, provider) }

    context 'the user is an "eleve"' do
      let(:user_level) { :eleve }

      context "the user has no active premium access" do
        it "creates a new premium access for the user" do
          expect { subject }.to change { user.premium_accesses.reload.count }.from(0).to(1)
        end

        context "there is a number_of_days provided" do
          let(:number_of_days) { 10 }

          it "makes the new premium access expire in the number of days provided" do
            subject
            premium_access = user.premium_accesses.first
            expect(premium_access.expire_at - Time.zone.now).to be <= number_of_days.days
            expect(premium_access.expire_at - Time.zone.now).to be >= (number_of_days - 1).days
          end
        end

        context "there is no arguments given" do
          subject { user.add_premium_access }

          it "makes the new premium access expire in DEFAULT_DURATION days by default" do
            subject
            premium_access = user.premium_accesses.first
            expect(premium_access.expire_at - Time.zone.now).to be <= PremiumAccess::DEFAULT_DURATION.days
            expect(premium_access.expire_at - Time.zone.now).to be >= (PremiumAccess::DEFAULT_DURATION - 1).days
          end

          it "makes the new premium_access provider :no_provider by default" do
            subject
            premium_access = user.premium_accesses.first
            expect(premium_access.provider).to eq(:no_provider.to_s)
          end
        end
      end

      context "the user already has an active premium access" do
        let(:expire_at_existing) { DateTime.new(3020, 12, 30) }
        let!(:premium_access) do
          create(:premium_access, user: user, expire_at: expire_at_existing, provider_id: provider_id_existing, provider: provider_existing)
        end
        let(:provider_id_existing) { "123ABC" }
        let(:provider_existing) { "stripe" }

        subject { user.add_premium_access(PremiumAccess::DEFAULT_DURATION) }

        it "provider defaults to :no_provider" do
          subject
          new_premium_access = user.premium_accesses.last
          expect(new_premium_access.provider).to eq(:no_provider.to_s)
        end

        it "creates a new premium access for the user" do
          expect { subject }.to change { user.premium_accesses.reload.count }.from(1).to(2)
        end

        it "doesn't set an activation or expiration date for the premium access" do
          subject
          premium_access = user.premium_accesses.last
          expect(premium_access.activate_at).to be_nil
          expect(premium_access.expire_at).to be_nil
        end

        it "sets provider to :no_provider" do
          subject
          premium_access = user.premium_accesses.last
          expect(premium_access.provider).to eq(:no_provider.to_s)
        end
      end
    end

    context 'the user is a "professeur"' do
      let(:user_level) { :professeur }

      it "doesn't create a premium access" do
        expect { subject }.not_to change { user.premium_accesses.count }
      end
    end

    context 'the user is an "admin"' do
      let(:user_level) { :admin }

      it "doesn't create a premium access" do
        expect { subject }.not_to change { user.premium_accesses.count }
      end
    end
  end
end
