# frozen_string_literal: true

require "rails_helper"

RSpec.describe Topic, type: :model do
  describe "relations" do
    it { is_expected.to belong_to(:category) }
    it { is_expected.to belong_to(:sub_category).optional(true) }
    it { is_expected.to have_many(:essays).dependent(:destroy) }
    it { is_expected.to have_many(:quiz_questions) }
  end

  describe "validations" do
    let(:category) { create(:category, category_trait) }
    let(:topic) { Topic.create(category_id: category.id) }

    context "when exercice_type is dissertation" do
      let(:category_trait) { :philosophie_dissertation }

      it "validations" do
        expect(topic).to validate_presence_of(:title)
        expect(topic).to validate_uniqueness_of(:title)
      end
    end

    context "when exercice_type is commentaire" do
      let(:category_trait) { :philosophie_commentaire }

      it "validations" do
        expect(topic).to validate_presence_of(:studied_text)
      end
    end

    describe "book details" do
      let(:category_trait) { :francais }
      subject { Topic.create(category_id: category.id, book: book) }

      context "book present" do
        let(:book) { "Livre" }

        it { is_expected.not_to validate_absence_of(:part) }
        it { is_expected.not_to validate_absence_of(:edition) }
      end
      context "book absent" do
        let(:book) { nil }

        it { is_expected.to validate_absence_of(:part) }
        it { is_expected.to validate_absence_of(:edition) }
      end
    end

    context "group philosophie" do
      let(:category_trait) { :philosophie_commentaire }

      it { is_expected.to validate_absence_of(:part) }
    end
  end

  describe "scopes" do
    describe "displayable" do
      let!(:topic1) { create(:topic, :philosophie_dissertation) }
      let!(:essay1_topic1) do
        create(:essay, :internal, topic: topic1, status: :draft)
      end
      let!(:essay2_topic1) do
        create(:essay, :internal, topic: topic1, status: :published)
      end
      let!(:topic2) { create(:topic, :philosophie_dissertation) }
      let!(:essay1_topic2) do
        create(:essay, :internal, topic: topic2, status: :draft)
      end

      it "returns topics for which there is at least one published essay" do
        expect(Topic.displayable.to_a)
          .to match_array(topic1)
      end

      context "there are two published essays for one topic" do
        before { create(:essay, :internal, topic: topic1, status: :published) }
        it "returns the topic only once" do
          expect(Topic.displayable.to_a)
            .to match_array(topic1)
        end
      end
    end

    describe ".without_attached_audio" do
      subject { described_class.without_attached_audio }

      let(:topic_with_audio) { create(:topic, :philosophie_dissertation) }
      let(:topic_without_audio) { create(:topic, :philosophie_dissertation) }

      before do
        io = File.open(Rails.root.join("spec", "factories", "mp3", "sample.mp3"))
        topic_with_audio.studied_text_audio.attach(io: io, filename: "sample.mp3", content_type: "audio/mpeg")
      end

      it "contains only the topics without audio" do
        expect(subject).to include(topic_without_audio)
        expect(subject).not_to include(topic_with_audio)
      end
    end
  end

  describe "#destroy_orphan_category" do
    let(:essay) { create(:essay, :internal, :philosophie_dissertation) }
    let(:topic) { essay.topic }
    let(:category) { topic.category }
    let(:category_id) { category.id }
    subject { topic.destroy }

    context "there is no more topics for the category" do
      it "destroys the category" do
        expect { subject }.to change { Category.find_by(id: category_id) }.to(nil)
      end
    end

    context "there is still topics for the category" do
      let!(:essay2) { create(:essay, :internal, topic: topic2) }
      let(:topic2) { create(:topic, :philosophie_dissertation, category: category) }

      it "doesn't destroy the category" do
        expect { subject }.not_to change { Category.find_by(id: category_id) }
      end
    end
  end

  describe "#best_displayable_essay" do
    let(:topic) { create(:topic, :philosophie_dissertation) }
    subject { topic.best_displayable_essay }

    context "there is only one essay" do
      let!(:essay) { create(:essay, :internal, topic: topic) }

      it "returns it" do
        expect(subject).to eq(essay)
      end
    end

    context "there is two external essays, one internal essay, having both the same number of hits" do
      let!(:external_essay_one) { create(:essay, :external, topic: topic, hits_nb: hits_nb) }
      let!(:essay) { create(:essay, :internal, topic: topic, hits_nb: hits_nb) }
      let!(:external_essay_two) { create(:essay, :external, topic: topic, hits_nb: hits_nb) }
      let(:hits_nb) { 3 }

      it "returns the internal essay" do
        expect(subject).to eq(essay)
      end
    end

    context "there are several essays of each kind, with different grades" do
      let!(:essay_many_hits) { create(:essay, :internal, topic: topic, hits_nb: 4) }
      let!(:essay_few_hits) { create(:essay, :internal, topic: topic, hits_nb: 2) }
      let!(:external_essay_many_hits) { create(:essay, :external, topic: topic, hits_nb: 5) }
      let!(:external_essay_few_hits) { create(:essay, :external, topic: topic, hits_nb: 1) }

      it "returns the internal essay with the best grade" do
        expect(subject).to eq(essay_many_hits)
      end
    end
  end

  describe "#generate_title_from_details" do
    let(:book) { nil }
    let(:part) { nil }
    let(:themes) { nil }
    let(:category) do
      create(:category,
        category_trait,
        {
          name: "La categorie"
        })
    end
    subject do
      create(:topic, {
        category: category,
        title: title,
        studied_text: studied_text,
        book: book,
        part: part,
        themes: themes
      })
    end

    context "exercice type is commentaire" do
      let(:category_trait) { :francais }
      let(:title) { nil }
      let(:studied_text) { "Texte étudié" }
      let(:book) { "" }

      context "We only have the category/author" do
        it "generate the title from the category/author only" do
          expect(subject.reload.title).to eq(category.name)
        end
      end

      context "We have the category and the book" do
        let(:book) { "le livre" }
        let(:expected_title) do
          "#{category.name}, #{book}"
        end

        it "adds the book to the title" do
          expect(subject.reload.title).to eq(expected_title)
        end

        context "We have the category, the book and also the part" do
          let(:part) { "chapitre 2" }

          context "the subject is francais" do
            let(:expected_title) do
              "#{category.name}, #{book} - #{part}"
            end

            it "adds the part to the title" do
              expect(subject.reload.title).to eq(expected_title)
            end
          end

          context "the subject is philosophie" do
            let(:category_trait) { :philosophie_commentaire }
            let(:expected_title) do
              "#{category.name}, #{book}"
            end

            it "doesn't add the part to the title" do
              expect(subject.reload.title).to eq(expected_title)
            end
          end
        end
      end

      context "We have the category and the themes" do
        let(:themes) { "thème 1 et thème 2" }
        let(:expected_title) do
          "#{category.name} : #{themes}"
        end

        it "adds the themes" do
          expect(subject.reload.title).to eq(expected_title)
        end
      end

      context "We have the category, the book, the part and the themes" do
        let(:book) { "le livre" }
        let(:part) { "chapitre 2" }
        let(:themes) { "thème 1 et thème 2" }
        let(:expected_title) do
          "#{category.name}, #{book} - #{part} : #{themes}"
        end

        it "adds up everything" do
          expect(subject.reload.title).to eq(expected_title)
        end
      end
    end

    context "exercice type is dissertation" do
      let(:category_trait) { :philosophie_dissertation }
      let(:title) { "Titre de la dissert" }
      let(:studied_text) { nil }

      it "doesn't change the title from its original topic title value" do
        expect(subject.reload.title).to eq(title)
      end
    end
  end

  describe "#generate_title_arabic_numbers" do
    let(:category) do
      create(:category, :philosophie_dissertation, {name: "Catégorie"})
    end
    let(:topic) do
      create(:topic, {
        category: category,
        title: title
      })
    end
    subject { topic.reload.title_arabic_numbers }

    context "Title containing no roman figures" do
      let(:title) { "No roman figures, only 5 digits" }

      it "generate the title prepending arabic numbers with leading 0s" do
        is_expected.to eq("No roman figures, only 00005 digits")
      end
    end

    context "Title containing roman figures" do
      let(:title) { "Roman figures, V, II only 5 digits" }

      it "converts them all with leading 0s" do
        is_expected.to eq("Roman figures, 00005, 00002 only 00005 digits")
      end
    end

    context "Title containing roman figures, including as words" do
      let(:title) { "Le Roman figures, Le matin, L'aprem V, II only 5 digits" }

      it "converts them all with leading 0s, expect for words" do
        is_expected.to eq("Le Roman figures, Le matin, L'aprem 00005, 00002 only 00005 digits")
      end
    end

    context "Title containing Prologue or Préambule or Préface" do
      let(:title) { "Le Roman - Préface" }

      it "replaces it with 0 with leading 0s" do
        is_expected.to eq("Le Roman - 00000")
      end
    end
  end

  describe "#reference_from_details" do
    let(:book) { nil }
    let(:part) { nil }
    let(:themes) { "les thèmes" }
    let(:edition) { nil }
    let(:category) do
      create(:category,
        category_trait,
        {
          name: "La categorie"
        })
    end
    let(:topic) do
      create(:topic, {
        category: category,
        title: title,
        studied_text: studied_text,
        book: book,
        part: part,
        themes: themes,
        edition: edition
      })
    end
    subject { topic.reference_from_details }

    context "exercice_type is commentaire" do
      let(:category_trait) { :philosophie_commentaire }
      let(:title) { nil }
      let(:studied_text) { "Texte étudié" }

      context "We only have the category/author" do
        it "generate the title from the category/author only" do
          expect(subject).to eq(category.name)
        end
      end

      context "We have the category and the book" do
        let(:book) { "le livre" }
        let(:expected_title) do
          "#{category.name}, #{book}"
        end

        it "adds the book to the title" do
          expect(subject).to eq(expected_title)
        end

        context "We have the category, the book and also the part" do
          let(:part) { "chapitre 2" }
          let(:expected_title) do
            "#{category.name}, #{book} - #{part}"
          end

          it "adds the part to the title" do
            expect(subject).to eq(expected_title)
          end
        end
      end

      context "We have the category, the book, the part and the edition" do
        let(:book) { "le livre" }
        let(:part) { "chapitre 2" }
        let(:edition) { "Edition larousse" }
        let(:expected_title) do
          "#{category.name}, #{book} - #{part} (#{edition})"
        end

        it "adds up everything" do
          expect(subject).to eq(expected_title)
        end
      end
    end

    context "exercice type is dissertation" do
      let(:category_trait) { :philosophie_dissertation }
      let(:title) { "Titre de la dissert" }
      let(:studied_text) { nil }

      it "returns nil" do
        expect(subject).to be_nil
      end
    end
  end

  describe "#keywords" do
    let(:category) { create(:category, category_trait, name: category_name) }
    subject {
      create(:topic,
        category: category,
        title: title,
        studied_text: studied_text,
        book: book,
        part: part,
        themes: themes)
    }
    let(:category_name) { "Nom categorie" }
    let(:studied_text) { nil }
    let(:book) { nil }
    let(:part) { nil }
    let(:themes) { nil }
    let(:edition) { nil }

    context "category is philosophie dissertation" do
      let(:category_trait) { :philosophie_dissertation }

      context "title is composed of long and lowercase words only" do
        let(:title) { "football pizza biere" }

        it "returns them with dashes replacing the spaces" do
          expect(subject.reload.keywords).to eq("football-pizza-biere")
        end
      end

      context "title is composed lowercase and uppercase letters" do
        let(:title) { "FootBall PizZa bierE" }

        it "returns them all lowercase" do
          expect(subject.reload.keywords).to eq("football-pizza-biere")
        end
      end

      context "title contains punctuation marks" do
        let(:title) { "FootBall?- PizZa, bierE...!" }

        it "removes them" do
          expect(subject.reload.keywords).to eq("football-pizza-biere")
        end
      end

      context "title contains words with accents" do
        let(:title) { "bellâtre âme étudier deçà être" }

        it "replaces them with the letter without accent" do
          expect(subject.reload.keywords).to eq("bellatre-ame-etudier-deca-etre")
        end
      end

      context "title context contains small words, of one or two letters" do
        let(:title) { "FootBall a le et PizZa, tu je 1d bierE... à de!" }

        it "removes them" do
          expect(subject.reload.keywords).to eq("football-pizza-biere")
        end
      end

      context 'title contains hypenated word "Peut-on" "soi-même"' do
        let(:title) { "Peut-on ne pas être soi-même ?" }

        it "keeps them" do
          expect(subject.reload.keywords).to eq("peut-on-pas-etre-soi-meme")
        end
      end

      context "title contains abbrevated pronouns with quotes or other equivalents symbols" do
        let(:title) { "c'est d'être l'arbre L'homme l’arbitre lʼidiot qu'elle qu'est-ce" }

        it "removes the pronoun and quote" do
          expect(subject.reload.keywords).to eq("est-etre-arbre-homme-arbitre-idiot-elle-est-ce")
        end
      end

      context "title contains a œ ligature" do
        let(:title) { "un œuf un bœuf" }

        it "replaces it with oe" do
          expect(subject.reload.keywords).to eq("oeuf-boeuf")
        end
      end

      context "title is a duplicate" do
        let(:title) { "Titre de la dissertation 10" }

        it "keeps the figure" do
          expect(subject.reload.keywords).to eq("titre-dissertation-10")
        end
      end
    end

    context "the essay is a commentaire" do
      let(:category_trait) { :francais }
      context "title contains digits, like chapters numbers" do
        let(:title) { nil }
        let(:category_name) { "Descartes" }
        let(:studied_text) { "blah" }
        let(:book) { "Les Passions de l'âme" }
        let(:part) { "Partie IV, chapitre 2" }
        let(:themes) { "Maitrise des passions " }

        it "keeps the figures in roman letters or digits" do
          expect(subject.reload.keywords)
            .to eq("descartes-les-passions-ame-partie-iv-chapitre-2-maitrise-des-passions")
        end
      end
    end
  end
end
