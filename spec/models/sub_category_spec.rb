# frozen_string_literal: true

require "rails_helper"

RSpec.describe SubCategory, type: :model do
  it { is_expected.to have_many(:quiz_questions) }
  it { is_expected.not_to allow_value(nil).for(:studied_texts_autonomous) }
end
