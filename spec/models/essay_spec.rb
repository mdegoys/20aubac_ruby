# frozen_string_literal: true

require "rails_helper"

RSpec.describe Essay, type: :model do
  describe "relations" do
    it { should belong_to(:topic) }
    it { should belong_to(:essay_content) }
  end

  describe ".top_by" do
    subject { described_class.top_by(:hits_nb) }
    before do
      create_list(:essay, number_of_essays, :internal, :philosophie_dissertation, hits_nb: 5)
    end

    context "there are 4 essays" do
      let(:number_of_essays) { 4 }

      it "returns 4 essays" do
        expect(subject.count).to eq 4
      end
    end

    context "there are more than 5 essays" do
      let(:number_of_essays) { 5 }
      let!(:least_viewed_essay) { create(:essay, :internal, :philosophie_dissertation, hits_nb: 1) }
      let!(:most_viewed_essay) { create(:essay, :internal, :philosophie_dissertation, hits_nb: 10) }
      let!(:unpublished_essay) { create(:essay, :internal, :philosophie_dissertation, hits_nb: 15, status: :draft) }

      it "returns 5 essays" do
        expect(subject.count).to eq 5
      end

      it "returns the essays with the most hits" do
        expect(subject).not_to include(least_viewed_essay)
        expect(subject).to include(most_viewed_essay)
      end

      it "does not return the not published essays" do
        expect(subject).not_to include(unpublished_essay)
      end
    end
  end

  describe "#destroy_orphan_topic" do
    let(:essay) { create(:essay, :internal, :philosophie_dissertation) }
    let(:topic) { essay.topic }
    subject { essay.destroy }

    context "there is no more essays for the topic" do
      it "destroys the topic" do
        expect { subject }.to change { Topic.find_by(id: topic.id) }.to(nil)
      end
    end

    context "there is still essays for the topic" do
      let!(:essay2) { create(:essay, :internal, :philosophie_dissertation, topic: topic) }
      it "doesn't destroy the topic" do
        expect { subject }.not_to change { Topic.find_by(id: topic.id) }
      end
    end

    context "there is still external essays for the topic" do
      let!(:essay_external2) { create(:essay, :external, :philosophie_dissertation, topic: topic) }
      it "doesn't destroy the topic" do
        expect { subject }.not_to change { Topic.find_by(id: topic.id) }
      end
    end
  end

  describe "#update_topic_best_essay" do
    let(:topic) { create(:topic, :philosophie_dissertation) }
    let!(:essay) { create(:essay, :internal, topic: topic, hits_nb: 4) }
    let!(:draft_essay) { create(:essay, :internal, status: :draft, topic: topic) }

    context "the best essay is destroyed" do
      subject { essay.destroy }

      context "there is no other published essay" do
        it "sets the best essay to nil" do
          expect { subject }.to change { topic.best_essay }.from(essay).to(nil)
        end
      end

      context "there are others published essays" do
        let!(:other_published_essay) { create(:essay, :internal, topic: topic, hits_nb: 2) }

        it "sets the best essay to the best amongst published essays" do
          expect { subject }.to change { topic.best_essay }.from(essay).to(other_published_essay)
        end
      end
    end

    context "the best essay is updated" do
      let!(:other_published_essay) { create(:essay, :internal, topic: topic, hits_nb: 2) }
      subject { essay.update(hits_nb: 1) }

      it "updates the best essay" do
        expect { subject }.to change { topic.best_essay }.from(essay).to(other_published_essay)
      end
    end

    context "a new and better essay is added" do
      let(:new_essay) { build(:essay, :internal, topic: topic, hits_nb: 5) }
      subject { new_essay.save }

      it "updates the best essay" do
        expect { subject }.to change { topic.best_essay }.from(essay).to(new_essay)
      end
    end
  end
end
