# frozen_string_literal: true

require "rails_helper"

RSpec.describe PremiumAccess, type: :model do
  describe "relations" do
    it { should belong_to(:user) }
  end

  describe "scopes" do
    describe ".active" do
      subject { described_class.active }

      let!(:active_premium_access) do
        create(:premium_access,
          activate_at: 10.minutes.ago)
      end
      let!(:past_premium_access) do
        create(:premium_access,
          activate_at: 3.days.ago,
          expire_at: 1.day.ago)
      end
      let!(:future_premium_access) do
        create(:premium_access,
          activate_at: 3.days.from_now,
          expire_at: 1.day.from_now)
      end

      let!(:not_activated_yet_premium_access) do
        create(:premium_access,
          activate_at: 3.days.from_now,
          expire_at: "NULL")
      end

      it "includes only the active premium access" do
        expect(subject).to eq([active_premium_access])
      end
    end

    describe ".inactive" do
      subject { described_class.inactive }

      let!(:active_premium_access) do
        create(:premium_access,
          activate_at: 10.minutes.ago)
      end
      let!(:past_premium_access) do
        create(:premium_access,
          activate_at: 3.days.ago,
          expire_at: 1.day.ago)
      end
      let!(:future_premium_access) do
        create(:premium_access,
          activate_at: 3.days.from_now,
          expire_at: 1.day.from_now)
      end

      let!(:not_activated_yet_premium_access) do
        create(:premium_access,
          activate_at: 3.days.from_now,
          expire_at: "NULL")
      end

      it "includes all but the active premium access" do
        expect(subject).to include(past_premium_access, future_premium_access, not_activated_yet_premium_access)
        expect(subject).not_to include(active_premium_access)
      end
    end

    describe ".recurring_paid" do
      subject { described_class.recurring_paid }

      let!(:unpaid_premium_access) do
        create(:premium_access)
      end

      let!(:unpaid_premium_access_2) do
        create(:premium_access, provider_id: "")
      end

      let!(:recurring_paid_premium_access) do
        create(:premium_access, provider_id: "123ABC", provider: :stripe)
      end

      it "includes only the paid premium access" do
        expect(subject).to eq([recurring_paid_premium_access])
      end
    end
  end

  describe "#activable?" do
    let(:premium_access) do
      create(:premium_access,
        activate_at: activate_at,
        expire_at: expire_at,
        provider_id: provider_id,
        user: user)
    end
    let(:activate_at) { 10.minutes.ago }
    let(:expire_at) { 30.days.from_now }
    let(:user) { create(:user) }

    subject { premium_access.activable? }

    context "there is a provider_id" do
      let(:provider_id) { "123ABC" }
      it "returns false" do
        expect(subject).to eq(false)
      end
    end

    context "there is no provider_id" do
      let(:provider_id) { nil }

      context "there is an activation date" do
        let(:expire_at) { nil }

        it "returns false" do
          expect(subject).to eq(false)
        end
      end

      context "there is an expiration date" do
        let(:active_at) { nil }

        it "returns false" do
          expect(subject).to eq(false)
        end
      end

      context "there is no activation date or expiration date" do
        let(:expire_at) { nil }
        let(:activate_at) { nil }

        context "the user has at least one active premium access" do
          let!(:active_premium_access) { create(:premium_access, user: user) }

          it "returns false" do
            expect(subject).to eq(false)
          end
        end

        context "the user has no active premium access" do
          it "returns true" do
            expect(subject).to eq(true)
          end
        end
      end
    end
  end

  describe "#active?" do
    let(:premium_access) do
      create(:premium_access,
        activate_at: activate_at,
        expire_at: expire_at)
    end
    let(:activate_at) { 2.days.ago }
    let(:expire_at) { 30.days.from_now }

    subject { premium_access.active? }

    context "there is no expiration date" do
      let(:expire_at) { nil }

      it "returns false" do
        expect(subject).to eq(false)
      end
    end

    context "there is an expiration date" do
      context "the acivation date is in the future" do
        let(:activate_at) { 1.day.from_now }
        it "returns false" do
          expect(subject).to eq(false)
        end
      end

      context "the expire_at is in the past" do
        let(:expire_at) { 1.day.ago }

        it "returns false" do
          expect(subject).to eq(false)
        end
      end

      context "we are between the activate_at and the expire_at" do
        it "returns true" do
          expect(subject).to eq(true)
        end
      end
    end
  end

  describe "#cancelable?" do
    let(:premium_access) { create(:premium_access, provider_id: provider_id, provider: provider) }

    subject { premium_access.cancelable? }

    context "there is no provider_id" do
      let(:provider_id) { nil }
      let(:provider) { nil }

      it "returns false" do
        expect(subject).to eq(false)
      end
    end

    context "there is a provider_id" do
      let(:provider_id) { "sub_123ABC" }

      context "the provider is stripe" do
        let(:provider) { :stripe }

        context "StripeHelper.status_for returns something else than active" do
          before { allow(StripeHelper).to receive(:status_for).and_return("something_else_than_active") }

          it "returns false" do
            expect(subject).to eq(false)
          end
        end

        context "StripeHelper.status_for returns active" do
          before { allow(StripeHelper).to receive(:status_for).and_return("active") }

          it "returns true" do
            expect(subject).to eq(true)
          end
        end
      end
    end
  end

  describe "#cancel" do
    let(:premium_access) { create(:premium_access, provider_id: provider_id, provider: provider) }
    subject { premium_access.cancel }

    context "there is no provider_id" do
      let(:provider_id) { nil }
      let(:provider) { nil }

      it "doesn't call StripeHelper.cancel_subscription method" do
        expect(StripeHelper).not_to receive(:cancel_subscription)
        subject
      end
    end

    context "there is a provider_id" do
      let(:provider_id) { "sub_123ABC" }
      let(:provider) { :stripe }

      it "calls StripeHelper.cancel_subscription method" do
        expect(StripeHelper).to receive(:cancel_subscription).with(provider_id)
        subject
      end
    end
  end

  describe "#activate" do
    let(:premium_access) { create(:premium_access) }

    subject { premium_access.activate }

    it "updates the expire_at" do
      expect { subject }.to change { premium_access.reload.activate_at }
    end

    it "updates the expire_at" do
      expect { subject }.to change { premium_access.reload.expire_at }
    end

    it "updates the activate_at to be around now" do
      subject
      expect(premium_access.reload.activate_at - Time.zone.now).to be <= 0.day
      expect(premium_access.reload.activate_at - Time.zone.now).to be > -1.day
    end

    it "updates the expire_at to be DEFAULT_DURATION*days from now" do
      default_duration = PremiumAccess::DEFAULT_DURATION
      subject
      expect(premium_access.reload.expire_at - Time.zone.now).to be <= default_duration.days
      expect(premium_access.reload.expire_at - Time.zone.now).to be > (default_duration - 1).days
    end
  end
end
