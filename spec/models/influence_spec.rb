# frozen_string_literal: true

require "rails_helper"

RSpec.describe Influence, type: :model do
  it { should belong_to(:influencer) }
  it { should belong_to(:influencee) }
  it { should validate_uniqueness_of(:influencer_id).scoped_to(:influencee_id) }
end
