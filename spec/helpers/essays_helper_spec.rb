# frozen_string_literal: true

require "rails_helper"

# Specs in this file have access to a helper object that includes
# the EssaysHelper. For example:
#
# describe EssaysHelper do
#   describe "string concat" do
#     it "concats two strings with spaces" do
#       expect(helper.concat_strings("this","that")).to eq("this that")
#     end
#   end
# end
RSpec.describe EssaysHelper, type: :helper do
  describe ".bbcode_to_html" do
    it "decodes [b][/b] to <strong></strong>" do
      expect(helper.bbcode_to_html("super [b]test[/b]")).to eq('super <span class="font-bold">test</span>')
    end

    it "decodes [i][/i] to <em></em>" do
      expect(helper.bbcode_to_html("super [i]test[/i]")).to eq('super <span class="italic">test</span>')
    end

    it 'decodes [u][/u] to <span class="italic bg-gray-200"></span>' do
      expect(helper.bbcode_to_html("super [u]test[/u]")).to eq('super <span class="italic bg-gray-200">test</span>')
    end

    it "decodes [tp][/tp] to <h2></h2> and add an id equals to the first word" do
      expect(helper.bbcode_to_html("super [tp]test[/tp]")).to eq('super <h2 id="test">test</h2>')
      expect(helper.bbcode_to_html("[tp]Un super test[/tp]")).to eq('<h2 id="Un">Un super test</h2>')
      expect(helper.bbcode_to_html("[tp] Un super test[/tp]")).to eq('<h2 id="Un"> Un super test</h2>')
      expect(helper.bbcode_to_html("[tp]Étude linéaire[/tp]")).to eq('<h2 id="Étude">Étude linéaire</h2>')
    end
  end

  describe ".extract_plan" do
    it "extracts the plan from the essay" do
      essay_text =
        'blah

          [tp]I. test[/tp]

          blouh

          [tp]II. test[/tp]

          Blih
        '
      expected_result = ["<a href='#I'>I. test</a>", "<a href='#II'>II. test</a>"]
      expect(helper.extract_plan(essay_text)).to eq(expected_result)
    end
  end

  describe ".remove_superflous_p_tags" do
    let(:subject) { helper.remove_superflous_p_tags(string) }

    context "p tags are surrounding h2 tags" do
      let(:string) { "<p><h2>Mon super titre</h2></p>" }
      let(:expected_result) { '<h2 class="pt-6 pb-4 scroll-mt-20">Mon super titre</h2>' }
      it "removes the p tags around h2 tags" do
        expect(subject).to eq(expected_result)
      end
    end

    context "there are spaces between the h2 and p tags" do
      let(:string) { "<p> <h2>Mon super titre</h2> </p>" }
      let(:expected_result) { '<h2 class="pt-6 pb-4 scroll-mt-20">Mon super titre</h2>' }
      it "removes the p tags" do
        expect(subject).to eq(expected_result)
      end
    end
  end
end
